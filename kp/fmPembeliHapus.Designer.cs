﻿namespace kp
{
    partial class fmPembeliHapus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPilihData = new System.Windows.Forms.Button();
            this.txtKodeUbah = new System.Windows.Forms.TextBox();
            this.btnLihatData = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPilihData);
            this.groupBox1.Controls.Add(this.txtKodeUbah);
            this.groupBox1.Controls.Add(this.btnLihatData);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(167, 124);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(301, 228);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hapus";
            // 
            // btnPilihData
            // 
            this.btnPilihData.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPilihData.Location = new System.Drawing.Point(188, 171);
            this.btnPilihData.Name = "btnPilihData";
            this.btnPilihData.Size = new System.Drawing.Size(67, 29);
            this.btnPilihData.TabIndex = 10;
            this.btnPilihData.Text = "Pilih";
            this.btnPilihData.UseVisualStyleBackColor = true;
            this.btnPilihData.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtKodeUbah
            // 
            this.txtKodeUbah.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKodeUbah.Location = new System.Drawing.Point(108, 115);
            this.txtKodeUbah.Name = "txtKodeUbah";
            this.txtKodeUbah.Size = new System.Drawing.Size(147, 29);
            this.txtKodeUbah.TabIndex = 9;
            // 
            // btnLihatData
            // 
            this.btnLihatData.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLihatData.Location = new System.Drawing.Point(82, 29);
            this.btnLihatData.Name = "btnLihatData";
            this.btnLihatData.Size = new System.Drawing.Size(133, 29);
            this.btnLihatData.TabIndex = 1;
            this.btnLihatData.Text = "Data Pembeli";
            this.btnLihatData.UseVisualStyleBackColor = true;
            this.btnLihatData.Click += new System.EventHandler(this.btnLihatData_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(16, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 26);
            this.label9.TabIndex = 11;
            this.label9.Text = "kode :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(251, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 30);
            this.label1.TabIndex = 2;
            this.label1.Text = "HAPUS PEMBELI";
            // 
            // fmPembeliHapus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 485);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "fmPembeliHapus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form hapus Pembeli";
            this.Load += new System.EventHandler(this.fmPembeliHapus_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnPilihData;
        private System.Windows.Forms.TextBox txtKodeUbah;
        private System.Windows.Forms.Button btnLihatData;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
    }
}