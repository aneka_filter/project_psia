﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmMasuk : Form
    {
        MySqlConnection con = new MySqlConnection();

        public fmMasuk()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";

        }
        String nama = "";
        private void lblNamaMasuk_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

        }

        private void lblNamaMasuk_EnabledChanged(object sender, EventArgs e)
        {

        }

        private void lblNamaMasuk_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void lblNamaMasuk_Click(object sender, EventArgs e)
        {

        }

        private void lblNamaMasuk_TabIndexChanged(object sender, EventArgs e)
        {

        }

        private void fmMasuk_Load(object sender, EventArgs e)
        {

        }

        private void btnMasuk_Click(object sender, EventArgs e)
        {
            Masuk();

            
        }

        #region Ambil Nama Login
        public void Masuk()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "Select nama From karyawan WHERE nama='" + txtNamaMasuk.Text + "'AND kata_sandi='" + txtKataSandi.Text + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            
            while (objReader.Read() == true)
            {
                nama = objReader.GetValue(0).ToString();

                Form masuk = new fmTampung(nama);
                masuk.Show();
                this.Hide();
            }
            if (nama == "")
            {
                MessageBox.Show("Nama atau Kata Sandi Anda Salah!!!");
                txtNamaMasuk.Text = "";
                txtKataSandi.Text = "";
            }
            objReader.Close();
            con.Close();
        }
        
        #endregion

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }
    }
}
