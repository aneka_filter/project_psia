﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmsupplierData : Form
    {
        MySqlConnection con = new MySqlConnection();
        int i = 0;
        public fmsupplierData()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =aneka_filter; uid=root;";
        }

        private void fmsupplierData_Load(object sender, EventArgs e)
        {
            DataSupplier();
            lblTotalData.Text += i + " Item";
        }
        #region Tampilkan Data Supplier
        public void DataSupplier()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT nama, alamat, no_telp FROM supplier WHERE status='0'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            dgvData.Rows.Clear();
            while (objReader.Read() == true)
            {
                i++;
                string nama = objReader.GetValue(0).ToString();
                string alamat = objReader.GetValue(1).ToString();
                string telp = objReader.GetValue(2).ToString();                
                dgvData.Rows.Add(nama, alamat, telp);

            }
            objReader.Close();
            con.Close();
        }

        #endregion

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }
    }
}
