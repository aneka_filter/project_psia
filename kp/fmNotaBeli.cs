﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmNotaBeli : Form
    {
        MySqlConnection con = new MySqlConnection();
        int i = 0;
        string ubahFormatBulan = "";
        string[] splitTampungDate;
        long totalHarga = 9000;
        long terimaUang = 0;
        long tampungUbah = 0;
        public fmNotaBeli()
        {
            InitializeComponent();

            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";

            #region Cara Bayar Default
            cboCaraBayar.Text = "Pilih Cara Bayar ...";
            cboCaraBayar.Items.Add("Tunai");
            cboCaraBayar.Items.Add("Transfer/Debit");
            cboCaraBayar.Items.Add("Credit Card");
            #endregion

            #region Daftar Supplier Default
            //cboNamaSupplier.Items.Add("Non-Member");
            DaftarNamaSupplier();
            #endregion

            #region Nama Bank
            cboNamaBank.Text = "Pilih Bank ...";
            cboNamaBank.Items.Add("BCA");
            cboNamaBank.Items.Add("Mandiri");
            #endregion

            //MessageBox.Show(getOldQuantity("Test").ToString());
            //MessageBox.Show(getKuantitiHargaLama("Test").ToString());
        }

        private void fmNotaBeli_Load(object sender, EventArgs e)
        {
            //--Tampilan Default--
            cboNamaSupplier.Text = "Nama Supplier";
            //--End Tampilan Default--

            string tampungDate = DateTime.Now.ToShortDateString();

            splitTampungDate = tampungDate.Split('/');

            //UbahFormatDate(splitTampungDate[2], splitTampungDate[0], splitTampungDate[1]);

            if (splitTampungDate[0] == "1")
                ubahFormatBulan = "I";
            else if (splitTampungDate[0] == "2")
                ubahFormatBulan = "II";
            else if (splitTampungDate[0] == "3")
                ubahFormatBulan = "III";
            else if (splitTampungDate[0] == "4")
                ubahFormatBulan = "IV";
            else if (splitTampungDate[0] == "5")
                ubahFormatBulan = "V";
            else if (splitTampungDate[0] == "6")
                ubahFormatBulan = "VI";
            else if (splitTampungDate[0] == "7")
                ubahFormatBulan = "VII";
            else if (splitTampungDate[0] == "8")
                ubahFormatBulan = "VIII";
            else if (splitTampungDate[0] == "9")
                ubahFormatBulan = "IX";
            else if (splitTampungDate[0] == "10")
                ubahFormatBulan = "X";
            else if (splitTampungDate[0] == "11")
                ubahFormatBulan = "XI";
            else if (splitTampungDate[0] == "12")
                ubahFormatBulan = "XII";


            NomorNota(ubahFormatBulan, splitTampungDate[2]);

            //MessageBox.Show(tampungDate);
            //MessageBox.Show(splitTampungDate[0]);
            //MessageBox.Show(splitTampungDate[2]);
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }

        private void txtTerimaUang_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvBarangNota_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            #region Isi Data
            int rowIndex = e.RowIndex;
            dgvBarangNota.Rows[rowIndex].Cells[0].Value = "";
            dgvBarangNota.Rows[rowIndex].Cells[1].Value = "";
            dgvBarangNota.Rows[rowIndex].Cells[2].Value = "";
            dgvBarangNota.Rows[rowIndex].Cells[3].Value = "";
            //MessageBox.Show(dgvBarangNota.);
            #endregion
        }

        private void dgvBarangNota_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            #region Isi Data
            int rowIndex = e.RowIndex;
            tampungUbah = Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[4].Value);
            
            #endregion
        }

        private void dgvBarangNota_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            #region Isi Data
            try
            {
                int rowIndex = e.RowIndex;
                int columnIndex = e.ColumnIndex;

                string cek = dgvBarangNota.Rows[rowIndex].Cells[0].Value.ToString();
                CekData(cek, rowIndex);
                if (dgvBarangNota.Rows[rowIndex].Cells[3].Value != null)
                {
                    totalHarga -= tampungUbah;
                    try
                    {
                        long hitungSub = Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[2].Value) * Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[3].Value);
                        dgvBarangNota.Rows[rowIndex].Cells[4].Value = hitungSub;

                    }
                    catch
                    {
                        dgvBarangNota.Rows[rowIndex].Cells[3].Value = 0;
                        dgvBarangNota.Rows[rowIndex].Cells[4].Value = 0;
                    }
                    totalHarga += Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[4].Value);
                    txtTotal.Text = totalHarga.ToString();
                }
                else
                {
                    long hitungSub = Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[2].Value) * Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[3].Value);
                    dgvBarangNota.Rows[rowIndex].Cells[4].Value = hitungSub;
                    totalHarga += Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[4].Value);
                    txtTotal.Text = totalHarga.ToString();
                }
            }
            catch
            {
                MessageBox.Show("Isi Kode Barang !!!");
            }

            #endregion
        }

        private void dgvBarangNota_KeyPress(object sender, KeyPressEventArgs e)
        {
            #region Isi Data
            try
            {
                if (e.KeyChar == (char)Keys.Back)
                {
                    //int column = dgvBarangNota.CurrentCellAddress.X;
                    int rowIndex = dgvBarangNota.CurrentCellAddress.Y;

                    if (dgvBarangNota.Rows[rowIndex].Cells[0].Value != null)
                    {
                        totalHarga -= Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[4].Value);
                        txtTotal.Text = totalHarga.ToString();
                        //for (int i = rowIndex; i < dgvBarangNota.RowCount; i++ )
                        //{
                        //    dgvBarangNota.Rows[i].Cells[0].Value = dgvBarangNota.Rows[i+1].Cells[0].Value;
                        //    dgvBarangNota.Rows[i].Cells[1].Value = dgvBarangNota.Rows[i + 1].Cells[1].Value;
                        //    dgvBarangNota.Rows[i].Cells[2].Value = dgvBarangNota.Rows[i + 1].Cells[2].Value;
                        //    dgvBarangNota.Rows[i].Cells[3].Value = dgvBarangNota.Rows[i + 1].Cells[3].Value;
                        //    dgvBarangNota.Rows[i].Cells[4].Value = dgvBarangNota.Rows[i + 1].Cells[4].Value;
                        //}
                        dgvBarangNota.Rows.RemoveAt(rowIndex);
                    }
                    else
                    {
                        MessageBox.Show("Tidak Ada Data Yang dihapus!!!");
                    }
                }
            }
            catch
            {
                MessageBox.Show(e.ToString());
            }
            dgvBarangNota.NotifyCurrentCellDirty(true);
            #endregion
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            #region Isi Data
            if (cboNamaSupplier.Text == "Pilih Supplier ..." || cboCaraBayar.Text == "Pilih Cara Bayar ..." || txtTotal.Text == "" || txtTotal.Text == "9000" || txtKodeNota.Text == "")
            {
                MessageBox.Show("Data Belum Lengkap!!!");
            }
            else
            {
                if (txtTerimaUang.Text == "")
                {
                    MessageBox.Show("Masukkan Jumlah Uang yang Diterima!!!");
                }
                else
                {
                    if (Convert.ToInt64(txtTerimaUang.Text) < Convert.ToInt64(txtTotal.Text))
                    {
                        MessageBox.Show("Jumlah Uang yang Diterima Kurang!!!");
                    }
                    else
                    {
                        SimpanNota();
                        updateHargaBaru(dgvBarangNota);
                        int barisDgv = dgvBarangNota.RowCount;
                        SimpanBarang(barisDgv);
                        SimpanJurnal();
                        SimpanAkunJurnal1();
                        SimpanAkunJurnal2();
                        SimpanAkunJurnal3();
                        SimpanAkunJurnal4();
                        cekdefault();


                    }
                }
            }
            tampungUbah = 0;
            #endregion 
        }

        #region Method
        public int AmbilIdJurnal()
        {
            int id = 0;
            con.Open();
            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id_jurnal FROM jurnal WHERE no_bukti='" + txtKodeNota.Text + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            //test
            while (objReader.Read() == true)
            {
                id = Convert.ToInt16(objReader.GetValue(0));
            }
            objReader.Close();
            con.Close();
            return id;
        }
        public void SimpanJurnal()
        {
            String a = "";

                if (cboCaraBayar.Text.Equals("Tunai"))
                {
                    a = " Pembelian  barang - Shipping point - cara bayar lunas tunai";
                }
                else if (cboCaraBayar.Text.Equals("Transfer/Debit"))
                {
                    if (cboNamaBank.Text.Equals("BCA"))
                    {
                        a = "Pembelian barang -Shipping point - cara bayar lunas transfer / debit bca";
                    }
                    else
                    {
                        a = "Pembelian barang -Shipping point - cara bayar lunas transfer / debit Mandiri";
                    }

                }
                else if (cboCaraBayar.Text.Equals("Credit Card"))
                {
                    a = "Pembelian Barang shipping point - cara bayar kredit bayar mundur";
                }
    
            String kt = a + "dengan jumlah biaya " + txtTotal.Text;
            con.Open();
            try
            {
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into jurnal Values (@id, @jenis, @no_bukti, @keterangan, @tgl_penyusutan)";
                objKomen.Parameters.AddWithValue("@id", "");
                objKomen.Parameters.AddWithValue("@jenis", "umum");
                objKomen.Parameters.AddWithValue("@no_bukti", txtKodeNota.Text);
                objKomen.Parameters.AddWithValue("@keterangan", kt);
                objKomen.Parameters.AddWithValue("@tgl_penyusutan", UbahFormatDate(splitTampungDate[2], splitTampungDate[0], splitTampungDate[1]));
                int hasil = objKomen.ExecuteNonQuery();
                if (hasil > 0)
                {
                    MessageBox.Show("jurnal masuk");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            con.Close();
        }
        public void SimpanAkunJurnal1()
        {
            
            int id = AmbilIdJurnal();
            con.Open();
            try
            {
                int hasil = 0;
                int totall = int.Parse(txtTotal.Text) - 9000;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun1, @id_jurnal1, @urutan1, @debet1, @kredit1)";

                objKomen.Parameters.AddWithValue("@no_akun1", 10060);
                objKomen.Parameters.AddWithValue("@id_jurnal1", id);
                objKomen.Parameters.AddWithValue("@urutan1", 1);
                objKomen.Parameters.AddWithValue("@debet1", totall);
                objKomen.Parameters.AddWithValue("@kredit1", 0);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            con.Close();
        }
        public void SimpanAkunJurnal2()
        {
            
            int id = AmbilIdJurnal();
            con.Open();
            try
            {
                int hasil = 0;
                int totall = int.Parse(txtTotal.Text) - 9000;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun2, @id_jurnal2, @urutan2, @debet2, @kredit2)";

                if (cboCaraBayar.Text.Equals("Tunai"))
                {
                    objKomen.Parameters.AddWithValue("@no_akun2", 10010);
                }
                else if (cboCaraBayar.Text.Equals("Transfer/Debit"))
                {
                    if (cboNamaBank.Text.Equals("BCA"))
                    {
                        objKomen.Parameters.AddWithValue("@no_akun2", 10020);
                    }
                    else
                    {
                        objKomen.Parameters.AddWithValue("@no_akun2", 10030);
                    }

                }
                else if (cboCaraBayar.Text.Equals("Credit Card"))
                {
                    objKomen.Parameters.AddWithValue("@no_akun2", 20010);
                }
                objKomen.Parameters.AddWithValue("@id_jurnal2", id);
                objKomen.Parameters.AddWithValue("@urutan2", 2);
                objKomen.Parameters.AddWithValue("@debet2", 0);
                objKomen.Parameters.AddWithValue("@kredit2", totall);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            con.Close();
        }
        public void SimpanAkunJurnal3()
        {
            int id = AmbilIdJurnal();
            con.Open();
            try
            {
                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun3, @id_jurnal3, @urutan3, @debet3, @kredit3)";

                objKomen.Parameters.AddWithValue("@no_akun3", 10060);
                objKomen.Parameters.AddWithValue("@id_jurnal3", id);
                objKomen.Parameters.AddWithValue("@urutan3", 3);
                objKomen.Parameters.AddWithValue("@debet3", 9000);
                objKomen.Parameters.AddWithValue("@kredit3", 0);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            con.Close();
        }
        public void SimpanAkunJurnal4()
        {
            
            int id = AmbilIdJurnal();
            con.Open();
            try
            {
                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun4, @id_jurnal4, @urutan4, @debet4, @kredit4)";

                if (cboCaraBayar.Text.Equals("Tunai"))
                {
                    objKomen.Parameters.AddWithValue("@no_akun4", 10010);
                }
                else if (cboCaraBayar.Text.Equals("Transfer/Debit"))
                {
                    if (cboNamaBank.Text.Equals("BCA"))
                    {
                        objKomen.Parameters.AddWithValue("@no_akun4", 10020);
                    }
                    else
                    {
                        objKomen.Parameters.AddWithValue("@no_akun4", 10030);
                    }

                }
                else if (cboCaraBayar.Text.Equals("Credit Card"))
                {
                    objKomen.Parameters.AddWithValue("@no_akun4", 20010);
                }
                objKomen.Parameters.AddWithValue("@id_jurnal4", id);
                objKomen.Parameters.AddWithValue("@urutan4", 4);
                objKomen.Parameters.AddWithValue("@debet4", 0);
                objKomen.Parameters.AddWithValue("@kredit4", 9000);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            con.Close();
        }

        public void updateHargaBaru(DataGridView data) {
            //con.Open();
            for (int i = 0; i < data.RowCount - 1; i++)
            {
                string kodeBarang = data[0, i].Value.ToString();
                int kuantitiBaru = int.Parse(data[3, i].Value.ToString());
                double hargaBaru = double.Parse(data[2, i].Value.ToString());
                int totalKuantiti = getKuantitiLama(kodeBarang) + kuantitiBaru;
                long hargaAverageBaru = (long)Math.Round(hitungAverage(kodeBarang, kuantitiBaru, hargaBaru), 2);
                long hargaJual = (long)Math.Round((hargaAverageBaru * 0.2) + hargaAverageBaru, 2);
                con.Open();
                string cmd = "UPDATE `barang` SET `kuantiti`="+totalKuantiti+",`harga_satuan`="+hargaAverageBaru+",`harga_jual`="+hargaJual+" WHERE id = '"+kodeBarang+"'";
                MessageBox.Show("harga jual baru: "+hargaJual.ToString());
                MySqlCommand queryCommand = new MySqlCommand(cmd,con);
                queryCommand.ExecuteNonQuery();
                con.Close();
            }
            //con.Close();
            MessageBox.Show("Update Harga Barang Sukses");
            data.DataSource = null;
        }

        public void DaftarNamaSupplier()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT nama FROM supplier Where status=0";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                string nama = objReader.GetValue(0).ToString();
                cboNamaSupplier.Items.Add(nama);
            }

            objReader.Close();
            con.Close();
        }

        public void NomorNota(string bulan, String Tahun)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT Count(*) FROM nota_beli Where status=0";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                int ubahDataInt = Convert.ToInt16(objReader.GetValue(0)) + 1;
                string data = ubahDataInt.ToString();
                string kodeNota = "B " + data + "/" + bulan + "/" + Tahun;
                txtKodeNota.Text = kodeNota;
                //MessageBox.Show(data+"/"+bulan+"/"+Tahun);
            }
            objReader.Close();
            con.Close();
        }

        //public void CekData(string kode, int indexBaris)
        //{
        //    con.Open();
        //    MySqlCommand objData = new MySqlCommand();
        //    objData.Connection = con;
        //    objData.CommandType = CommandType.Text;
        //    objData.CommandText = "SELECT id, nama, kuantiti, harga_satuan FROM barang Where hapus=0 and id='" + kode + "'";
        //    MySqlDataReader objReader;
        //    objReader = objData.ExecuteReader();

        //    if (objReader.Read() == true)
        //    {
        //        i++;
        //        string kodeTampung = objReader.GetValue(0).ToString();
        //        string nama = objReader.GetValue(1).ToString();
        //        string kuantiti = objReader.GetValue(2).ToString();
        //        //string harga_satuan = objReader.GetValue(3).ToString();
        //        dgvBarangNota.Rows[indexBaris].Cells[0].Value = kodeTampung;
        //        dgvBarangNota.Rows[indexBaris].Cells[1].Value = nama;
        //        //dgvBarangNota.Rows[indexBaris].Cells[2].Value = harga_satuan;
        //        //dgvBarangNota.Rows.Add(kodeTampung, nama, harga_satuan);
        //    }
        //    else
        //    {
        //        MessageBox.Show("Kode Barang Tidak Ada!!!");
        //        dgvBarangNota.Rows.RemoveAt(indexBaris);
        //    }

        //    objReader.Close();
        //    con.Close();
        //}
        public void CekData(string kode, int indexBaris)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id, nama, kuantiti, harga_satuan FROM barang Where hapus=0 and id='" + kode + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            
            if (objReader.Read() == true)
            {
                i++;
                string kodeTampung = objReader.GetValue(0).ToString();
                string nama = objReader.GetValue(1).ToString();
                string kuantiti = objReader.GetValue(2).ToString();
                //string harga_satuan = objReader.GetValue(3).ToString();
                dgvBarangNota.Rows[indexBaris].Cells[0].Value = kodeTampung;
                dgvBarangNota.Rows[indexBaris].Cells[1].Value = nama;
               // dgvBarangNota.Rows[indexBaris].Cells[2].Value = harga_satuan;
                //dgvBarangNota.Rows.Add(kodeTampung, nama, harga_satuan);

            }
            else
            {
                MessageBox.Show("Kode Barang Tidak Ada!!!");
                dgvBarangNota.Rows.RemoveAt(indexBaris);
            }

            objReader.Close();
            con.Close();
        }
        public string UbahFormatDate(string tahun, string bulan, string tanggal)
        {
            string a = tahun + "-" + bulan + "-" + tanggal;
            return a;
        }

        //public int getOldQuantity(int kodeBarang)
        //{
        //    int quantity = 0;
        //    con.Open();
        //    MySqlCommand objData = new MySqlCommand();
        //    objData.Connection = con;
        //    objData.CommandType = CommandType.Text;
        //    objData.CommandText = "SELECT kuantiti FROM barang Where hapus = 0 and id='" + kodeBarang + "'";
        //    MySqlDataReader objReader;
        //    objReader = objData.ExecuteReader();
        //    con.Close();
        //    while (objReader.Read() == true)
        //    {
        //        return Convert.ToInt16(objReader.GetValue(0));
        //    }
        //}

        public int getKuantitiLama(string kodeBarang)
        {
            con.Open();
            int kuantiti;
            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT kuantiti FROM barang Where hapus = 0 and id='" + kodeBarang + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            objReader.Read();
            kuantiti = int.Parse(objReader.GetValue(0).ToString());
            con.Close();
            return kuantiti;
        }

        public double getKuantitiHargaLama(string kodeBarang) {
            int kuantitiLama;
            double hargaLama;
            con.Open();
            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT kuantiti, harga_satuan FROM barang Where hapus = 0 and id='" + kodeBarang + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            objReader.Read();
            kuantitiLama = int.Parse(objReader.GetValue(0).ToString());
            hargaLama = double.Parse(objReader.GetValue(1).ToString());
            double quantityPrice = kuantitiLama * hargaLama;
            con.Close();
            return quantityPrice;
        }

        public double hitungAverage(string kodeBarang, int kuantitiBaru, double HargaBaru)
        {
            int kuantitiLama = getKuantitiLama(kodeBarang);
            double kuantitiHargaLama = getKuantitiHargaLama(kodeBarang);
            double hargaRata = (kuantitiHargaLama + (kuantitiBaru * HargaBaru)) / (kuantitiLama + kuantitiBaru);
            MessageBox.Show("kuantiti harga lama "+ hargaRata.ToString());
            return hargaRata;
        }

        public void SimpanNota()
        {
            con.Open();
            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            objKomen.CommandText = "Insert Into nota_beli Values (@kode, @totalHarga, @tipeBayar, @biayaKirim, @waktuBeli, @id_supplier, @id_karyawan, @namaBank, @noKartu,    @status)";
            objKomen.Parameters.AddWithValue("@kode", txtKodeNota.Text);
            objKomen.Parameters.AddWithValue("@totalHarga", txtTotal.Text);
            objKomen.Parameters.AddWithValue("@namaBank", cboNamaBank.Text);
            objKomen.Parameters.AddWithValue("@noKartu", txtNomorKartu.Text);
            objKomen.Parameters.AddWithValue("@tipeBayar", cboCaraBayar.Text);
            objKomen.Parameters.AddWithValue("@biayaKirim", 9000);
            objKomen.Parameters.AddWithValue("@waktuBeli", UbahFormatDate(splitTampungDate[2], splitTampungDate[0], splitTampungDate[1]));
            objKomen.Parameters.AddWithValue("@id_supplier", 1);
            objKomen.Parameters.AddWithValue("@id_karyawan", 1);
            objKomen.Parameters.AddWithValue("@status", 0);

            int hasil = objKomen.ExecuteNonQuery();

            con.Close();

            
        }

        public void SimpanBarang(int baris)
        {
            con.Open();
            try
            {
                int hasil = 0;
                for (int a = 0; a < baris - 1; a++)
                {
                    MySqlCommand objKomen = new MySqlCommand();
                    objKomen.Connection = con;
                    objKomen.CommandType = CommandType.Text;
                    objKomen.CommandText = "Insert Into nota_beli_has_barang Values (@kode_nota, @id_barang, @kuantiti, @sub_harga)";
                    objKomen.Parameters.AddWithValue("@kode_nota", txtKodeNota.Text);
                    objKomen.Parameters.AddWithValue("@id_barang", dgvBarangNota.Rows[a].Cells[0].Value.ToString());
                    objKomen.Parameters.AddWithValue("@kuantiti", Convert.ToInt64(dgvBarangNota.Rows[a].Cells[3].Value));
                    objKomen.Parameters.AddWithValue("@sub_harga", Convert.ToInt64(dgvBarangNota.Rows[a].Cells[4].Value));
                    hasil = objKomen.ExecuteNonQuery();
                }

                if (hasil > 0)
                {
                    
                }
                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            con.Close();
        }
        public void cekdefault()
        {
            MessageBox.Show("Penambahan data yang Anda tambahkan berhasil!");
            //con.Close();
            NomorNota(ubahFormatBulan, splitTampungDate[2]);
            //con.Open();
            cboNamaSupplier.Text = "Pilih Supplier ...";
            cboCaraBayar.Text = "Pilih Cara Bayar ...";
            txtTotal.Clear();
            totalHarga = 9000;
            txtUangKembali.Clear();
            txtTerimaUang.Clear();
            dgvBarangNota.Rows.Clear();
        }
        #endregion



        private void cboCaraBayar_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCaraBayar.Text == "Tunai")
            {
                cboNamaBank.Enabled = false;
                cboNamaBank.Text = "";
                cboCaraBayar.Text = "Tunai";
                txtNomorKartu.Enabled = false;
                dtpDate.Enabled = false;
            }
            else if (cboCaraBayar.Text == "Transfer/Debit")
            {
                cboNamaBank.Enabled = true;
                txtNomorKartu.Enabled = true;
                dtpDate.Enabled = false;
            }
            else if (cboCaraBayar.Text == "Credit Card")
            {
                cboNamaBank.Enabled = true;
                txtNomorKartu.Enabled = true;
                dtpDate.Enabled = true;
            }
        }

        private void cboNamaBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cboNamaSupplier_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtTerimaUang_TextChanged_1(object sender, EventArgs e)
        {
            #region Isi Data
            try
            {
                if (txtTerimaUang.Text == "")
                {
                    txtTerimaUang.Text = "0";
                    //terimaUang = Convert.ToInt64(txtTerimaUang.Text);
                    txtUangKembali.Text = "0";
                }
                else
                {
                    terimaUang = Convert.ToInt64(txtTerimaUang.Text);
                    txtUangKembali.Text = Convert.ToString(terimaUang - totalHarga);
                }

            }
            catch
            {
                MessageBox.Show("Isi Terima Uang dengan Benar !!!");
                txtUangKembali.Text = "0";
            }
            #endregion
        }
    }
}
