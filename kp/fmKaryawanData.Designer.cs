﻿namespace kp
{
    partial class fmKaryawanData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.txtNama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtAlamat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtTelepon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtAkses = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtGaji = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTotalData = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtNama,
            this.txtAlamat,
            this.txtTelepon,
            this.txtAkses,
            this.txtGaji});
            this.dgvData.Location = new System.Drawing.Point(40, 124);
            this.dgvData.Name = "dgvData";
            this.dgvData.Size = new System.Drawing.Size(544, 219);
            this.dgvData.TabIndex = 1;
            // 
            // txtNama
            // 
            this.txtNama.HeaderText = "Nama";
            this.txtNama.Name = "txtNama";
            this.txtNama.ReadOnly = true;
            this.txtNama.Width = 80;
            // 
            // txtAlamat
            // 
            this.txtAlamat.HeaderText = "Alamat";
            this.txtAlamat.Name = "txtAlamat";
            this.txtAlamat.ReadOnly = true;
            this.txtAlamat.Width = 150;
            // 
            // txtTelepon
            // 
            this.txtTelepon.HeaderText = "No. Telepon";
            this.txtTelepon.Name = "txtTelepon";
            this.txtTelepon.ReadOnly = true;
            // 
            // txtAkses
            // 
            this.txtAkses.HeaderText = "Jabatan";
            this.txtAkses.Name = "txtAkses";
            this.txtAkses.ReadOnly = true;
            this.txtAkses.Width = 70;
            // 
            // txtGaji
            // 
            this.txtGaji.HeaderText = "Gaji";
            this.txtGaji.Name = "txtGaji";
            this.txtGaji.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(251, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "DATA KARYAWAN";
            // 
            // lblTotalData
            // 
            this.lblTotalData.AutoSize = true;
            this.lblTotalData.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalData.Location = new System.Drawing.Point(329, 87);
            this.lblTotalData.Name = "lblTotalData";
            this.lblTotalData.Size = new System.Drawing.Size(116, 23);
            this.lblTotalData.TabIndex = 6;
            this.lblTotalData.Text = "Total Data : ";
            // 
            // fmKaryawanData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 485);
            this.ControlBox = false;
            this.Controls.Add(this.lblTotalData);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.label1);
            this.Name = "fmKaryawanData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Data Karyawan";
            this.Load += new System.EventHandler(this.fmDataKaryawan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTotalData;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNama;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtAlamat;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtTelepon;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtAkses;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtGaji;
    }
}