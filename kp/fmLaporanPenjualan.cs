﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmLaporanPenjualan : Form
    {
        MySqlConnection con = new MySqlConnection();
        int i = 0;
        string[] splitTampungDate;
        string[] splitTampungDtp;
        public fmLaporanPenjualan()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =aneka_filter; uid=root;";
        }

        private void fmLaporanPenjualan_Load(object sender, EventArgs e)
        {
            string tampungDate = DateTime.Now.ToShortDateString();
            splitTampungDate = tampungDate.Split('/');
            
            string tampungDtp = dtpDate.Value.ToShortDateString();
            splitTampungDtp = tampungDtp.Split('/');

            cboPeriode.Text = "Pilih Periode ...";
            cboPeriode.Items.Add("Hari ini");
            cboPeriode.Items.Add("Pilih tanggal");
            cboPeriode.Items.Add("Bulan ini");
            cboPeriode.Items.Add("Bulan Lalu");

            dtpDate.Enabled = false;

        }

        private void cboPeriode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboPeriode.Text == "Hari ini")
            {
                dtpDate.Enabled = false;
                TampilkanDataHariIni();
            }
            if (cboPeriode.Text == "Pilih tanggal")
            {
                dtpDate.Enabled = true;
                TampilkanDataPilihTanggal();
            }
            else if (cboPeriode.Text == "Bulan ini")
            {
                dtpDate.Enabled = false;
            }
            else if (cboPeriode.Text == "Bulan Lalu")
            {
                dtpDate.Enabled = false;
            }
        }


        public string UbahFormatDate(string tahun, string bulan, string tanggal)
        {
            string a = tahun + "-" + bulan + "-" + tanggal;
            return a;
        }
        public void TampilkanDataHariIni()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT j.kode , p.nama, j.tipe_bayar , j.total_harga FROM nota_jual j INNER JOIN pembeli p ON j.id_pembeli=p.id Where j.status=0 and j.waktu_jual='"+UbahFormatDate(splitTampungDate[2],splitTampungDate[0],splitTampungDate[1])+"'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            dgvData.Rows.Clear();
            while (objReader.Read() == true)
            {
                i++;
                string kode = objReader.GetValue(0).ToString();
                string nama = objReader.GetValue(1).ToString();
                string tipe = objReader.GetValue(2).ToString();
                string harga = objReader.GetValue(3).ToString();
                if (tipe == "Tunai")
                {
                    dgvData.Rows.Add(kode, nama, harga, 0);
                }
                else if (tipe == "Kredit")
                {
                    dgvData.Rows.Add(kode, nama, 0, harga);
                }
                
            }
            objReader.Close();
            con.Close();
        }
        public void TampilkanDataPilihTanggal()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT j.kode , p.nama, j.tipe_bayar , j.total_harga FROM nota_jual j INNER JOIN pembeli p ON j.id_pembeli=p.id Where j.status=0 and j.waktu_jual='" + UbahFormatDate(splitTampungDtp[2], splitTampungDtp[0], splitTampungDtp[1]) + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            dgvData.Rows.Clear();
            while (objReader.Read() == true)
            {
                i++;
                string kode = objReader.GetValue(0).ToString();
                string nama = objReader.GetValue(1).ToString();
                string tipe = objReader.GetValue(2).ToString();
                string harga = objReader.GetValue(3).ToString();
                if (tipe == "Tunai")
                {
                    dgvData.Rows.Add(kode, nama, harga, 0);
                }
                else if (tipe == "Kredit")
                {
                    dgvData.Rows.Add(kode, nama, 0, harga);
                }

            }
            objReader.Close();
            con.Close();
        }
    }
}
