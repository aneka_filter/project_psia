﻿namespace kp
{
    partial class fmMenuUtama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menuNota = new System.Windows.Forms.ToolStripMenuItem();
            this.filterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notaSementaraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notaAsliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notaPenyusutanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBarang = new System.Windows.Forms.ToolStripMenuItem();
            this.dataBarangToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.filterToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.sparePartToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stokBarangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perPartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.globalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tambahBarangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ubahBarangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hapusBarangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuKaryawan = new System.Windows.Forms.ToolStripMenuItem();
            this.dataKaryawanToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tambahKaryawanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ubahKaryawanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hapusKaryawanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pembeliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataPembeliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tambahPembeliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ubahPembeliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hapusPembeliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tambahSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ubahSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hapusSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLaporan = new System.Windows.Forms.ToolStripMenuItem();
            this.penjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ppnPenjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neracaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mutasiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penjualanToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pembelianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblNama = new System.Windows.Forms.Label();
            this.lblWaktu = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblTanggal = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNota,
            this.menuBarang,
            this.menuKaryawan,
            this.pembeliToolStripMenuItem,
            this.supplierToolStripMenuItem,
            this.menuLaporan,
            this.mutasiToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(1354, 31);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            // 
            // menuNota
            // 
            this.menuNota.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filterToolStripMenuItem,
            this.spareToolStripMenuItem,
            this.notaSementaraToolStripMenuItem,
            this.notaAsliToolStripMenuItem,
            this.notaPenyusutanToolStripMenuItem});
            this.menuNota.Name = "menuNota";
            this.menuNota.Size = new System.Drawing.Size(58, 27);
            this.menuNota.Text = "Nota";
            // 
            // filterToolStripMenuItem
            // 
            this.filterToolStripMenuItem.Name = "filterToolStripMenuItem";
            this.filterToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.filterToolStripMenuItem.Text = "Nota Jual";
            this.filterToolStripMenuItem.Click += new System.EventHandler(this.filterToolStripMenuItem_Click);
            // 
            // spareToolStripMenuItem
            // 
            this.spareToolStripMenuItem.Name = "spareToolStripMenuItem";
            this.spareToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.spareToolStripMenuItem.Text = "Nota Beli";
            this.spareToolStripMenuItem.Click += new System.EventHandler(this.spareToolStripMenuItem_Click);
            // 
            // notaSementaraToolStripMenuItem
            // 
            this.notaSementaraToolStripMenuItem.Name = "notaSementaraToolStripMenuItem";
            this.notaSementaraToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.notaSementaraToolStripMenuItem.Text = "Nota Bayar";
            this.notaSementaraToolStripMenuItem.Click += new System.EventHandler(this.notaSementaraToolStripMenuItem_Click);
            // 
            // notaAsliToolStripMenuItem
            // 
            this.notaAsliToolStripMenuItem.Name = "notaAsliToolStripMenuItem";
            this.notaAsliToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.notaAsliToolStripMenuItem.Text = "Nota Biaya";
            this.notaAsliToolStripMenuItem.Click += new System.EventHandler(this.notaAsliToolStripMenuItem_Click);
            // 
            // notaPenyusutanToolStripMenuItem
            // 
            this.notaPenyusutanToolStripMenuItem.Name = "notaPenyusutanToolStripMenuItem";
            this.notaPenyusutanToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.notaPenyusutanToolStripMenuItem.Text = "Nota Penyusutan";
            this.notaPenyusutanToolStripMenuItem.Click += new System.EventHandler(this.notaPenyusutanToolStripMenuItem_Click);
            // 
            // menuBarang
            // 
            this.menuBarang.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataBarangToolStripMenuItem2,
            this.stokBarangToolStripMenuItem,
            this.tambahBarangToolStripMenuItem,
            this.ubahBarangToolStripMenuItem,
            this.hapusBarangToolStripMenuItem});
            this.menuBarang.Name = "menuBarang";
            this.menuBarang.Size = new System.Drawing.Size(72, 27);
            this.menuBarang.Text = "Barang";
            // 
            // dataBarangToolStripMenuItem2
            // 
            this.dataBarangToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filterToolStripMenuItem2,
            this.sparePartToolStripMenuItem1});
            this.dataBarangToolStripMenuItem2.Name = "dataBarangToolStripMenuItem2";
            this.dataBarangToolStripMenuItem2.Size = new System.Drawing.Size(191, 28);
            this.dataBarangToolStripMenuItem2.Text = "Data Barang";
            this.dataBarangToolStripMenuItem2.Click += new System.EventHandler(this.dataBarangToolStripMenuItem2_Click);
            // 
            // filterToolStripMenuItem2
            // 
            this.filterToolStripMenuItem2.Name = "filterToolStripMenuItem2";
            this.filterToolStripMenuItem2.Size = new System.Drawing.Size(160, 28);
            this.filterToolStripMenuItem2.Text = "Filter";
            // 
            // sparePartToolStripMenuItem1
            // 
            this.sparePartToolStripMenuItem1.Name = "sparePartToolStripMenuItem1";
            this.sparePartToolStripMenuItem1.Size = new System.Drawing.Size(160, 28);
            this.sparePartToolStripMenuItem1.Text = "Spare Part";
            // 
            // stokBarangToolStripMenuItem
            // 
            this.stokBarangToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.perPartToolStripMenuItem,
            this.globalToolStripMenuItem});
            this.stokBarangToolStripMenuItem.Name = "stokBarangToolStripMenuItem";
            this.stokBarangToolStripMenuItem.Size = new System.Drawing.Size(191, 28);
            this.stokBarangToolStripMenuItem.Text = "Stok Barang";
            this.stokBarangToolStripMenuItem.Click += new System.EventHandler(this.stokBarangToolStripMenuItem_Click);
            // 
            // perPartToolStripMenuItem
            // 
            this.perPartToolStripMenuItem.Name = "perPartToolStripMenuItem";
            this.perPartToolStripMenuItem.Size = new System.Drawing.Size(142, 28);
            this.perPartToolStripMenuItem.Text = "Per-Part";
            // 
            // globalToolStripMenuItem
            // 
            this.globalToolStripMenuItem.Name = "globalToolStripMenuItem";
            this.globalToolStripMenuItem.Size = new System.Drawing.Size(142, 28);
            this.globalToolStripMenuItem.Text = "Global";
            this.globalToolStripMenuItem.Click += new System.EventHandler(this.globalToolStripMenuItem_Click);
            // 
            // tambahBarangToolStripMenuItem
            // 
            this.tambahBarangToolStripMenuItem.Name = "tambahBarangToolStripMenuItem";
            this.tambahBarangToolStripMenuItem.Size = new System.Drawing.Size(191, 28);
            this.tambahBarangToolStripMenuItem.Text = "Tambah Barang";
            this.tambahBarangToolStripMenuItem.Click += new System.EventHandler(this.tambahBarangToolStripMenuItem_Click);
            // 
            // ubahBarangToolStripMenuItem
            // 
            this.ubahBarangToolStripMenuItem.Name = "ubahBarangToolStripMenuItem";
            this.ubahBarangToolStripMenuItem.Size = new System.Drawing.Size(191, 28);
            this.ubahBarangToolStripMenuItem.Text = "Ubah Barang";
            this.ubahBarangToolStripMenuItem.Click += new System.EventHandler(this.ubahBarangToolStripMenuItem_Click);
            // 
            // hapusBarangToolStripMenuItem
            // 
            this.hapusBarangToolStripMenuItem.Name = "hapusBarangToolStripMenuItem";
            this.hapusBarangToolStripMenuItem.Size = new System.Drawing.Size(191, 28);
            this.hapusBarangToolStripMenuItem.Text = "Hapus Barang";
            this.hapusBarangToolStripMenuItem.Click += new System.EventHandler(this.hapusBarangToolStripMenuItem_Click);
            // 
            // menuKaryawan
            // 
            this.menuKaryawan.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataKaryawanToolStripMenuItem1,
            this.tambahKaryawanToolStripMenuItem,
            this.ubahKaryawanToolStripMenuItem,
            this.hapusKaryawanToolStripMenuItem});
            this.menuKaryawan.Name = "menuKaryawan";
            this.menuKaryawan.Size = new System.Drawing.Size(92, 27);
            this.menuKaryawan.Text = "Karyawan";
            // 
            // dataKaryawanToolStripMenuItem1
            // 
            this.dataKaryawanToolStripMenuItem1.Name = "dataKaryawanToolStripMenuItem1";
            this.dataKaryawanToolStripMenuItem1.Size = new System.Drawing.Size(211, 28);
            this.dataKaryawanToolStripMenuItem1.Text = "Data Karyawan";
            this.dataKaryawanToolStripMenuItem1.Click += new System.EventHandler(this.dataKaryawanToolStripMenuItem1_Click);
            // 
            // tambahKaryawanToolStripMenuItem
            // 
            this.tambahKaryawanToolStripMenuItem.Name = "tambahKaryawanToolStripMenuItem";
            this.tambahKaryawanToolStripMenuItem.Size = new System.Drawing.Size(211, 28);
            this.tambahKaryawanToolStripMenuItem.Text = "Tambah Karyawan";
            this.tambahKaryawanToolStripMenuItem.Click += new System.EventHandler(this.tambahKaryawanToolStripMenuItem_Click);
            // 
            // ubahKaryawanToolStripMenuItem
            // 
            this.ubahKaryawanToolStripMenuItem.Name = "ubahKaryawanToolStripMenuItem";
            this.ubahKaryawanToolStripMenuItem.Size = new System.Drawing.Size(211, 28);
            this.ubahKaryawanToolStripMenuItem.Text = "Ubah Karyawan";
            this.ubahKaryawanToolStripMenuItem.Click += new System.EventHandler(this.ubahKaryawanToolStripMenuItem_Click);
            // 
            // hapusKaryawanToolStripMenuItem
            // 
            this.hapusKaryawanToolStripMenuItem.Name = "hapusKaryawanToolStripMenuItem";
            this.hapusKaryawanToolStripMenuItem.Size = new System.Drawing.Size(211, 28);
            this.hapusKaryawanToolStripMenuItem.Text = "Hapus Karyawan";
            this.hapusKaryawanToolStripMenuItem.Click += new System.EventHandler(this.hapusKaryawanToolStripMenuItem_Click);
            // 
            // pembeliToolStripMenuItem
            // 
            this.pembeliToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataPembeliToolStripMenuItem,
            this.tambahPembeliToolStripMenuItem,
            this.ubahPembeliToolStripMenuItem,
            this.hapusPembeliToolStripMenuItem});
            this.pembeliToolStripMenuItem.Name = "pembeliToolStripMenuItem";
            this.pembeliToolStripMenuItem.Size = new System.Drawing.Size(77, 27);
            this.pembeliToolStripMenuItem.Text = "Pembeli";
            // 
            // dataPembeliToolStripMenuItem
            // 
            this.dataPembeliToolStripMenuItem.Name = "dataPembeliToolStripMenuItem";
            this.dataPembeliToolStripMenuItem.Size = new System.Drawing.Size(196, 28);
            this.dataPembeliToolStripMenuItem.Text = "Data Pembeli";
            this.dataPembeliToolStripMenuItem.Click += new System.EventHandler(this.dataPembeliToolStripMenuItem_Click);
            // 
            // tambahPembeliToolStripMenuItem
            // 
            this.tambahPembeliToolStripMenuItem.Name = "tambahPembeliToolStripMenuItem";
            this.tambahPembeliToolStripMenuItem.Size = new System.Drawing.Size(196, 28);
            this.tambahPembeliToolStripMenuItem.Text = "Tambah Pembeli";
            this.tambahPembeliToolStripMenuItem.Click += new System.EventHandler(this.tambahPembeliToolStripMenuItem_Click);
            // 
            // ubahPembeliToolStripMenuItem
            // 
            this.ubahPembeliToolStripMenuItem.Name = "ubahPembeliToolStripMenuItem";
            this.ubahPembeliToolStripMenuItem.Size = new System.Drawing.Size(196, 28);
            this.ubahPembeliToolStripMenuItem.Text = "Ubah Pembeli";
            this.ubahPembeliToolStripMenuItem.Click += new System.EventHandler(this.ubahPembeliToolStripMenuItem_Click);
            // 
            // hapusPembeliToolStripMenuItem
            // 
            this.hapusPembeliToolStripMenuItem.Name = "hapusPembeliToolStripMenuItem";
            this.hapusPembeliToolStripMenuItem.Size = new System.Drawing.Size(196, 28);
            this.hapusPembeliToolStripMenuItem.Text = "Hapus Pembeli";
            this.hapusPembeliToolStripMenuItem.Click += new System.EventHandler(this.hapusPembeliToolStripMenuItem_Click);
            // 
            // supplierToolStripMenuItem
            // 
            this.supplierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataSupplierToolStripMenuItem,
            this.tambahSupplierToolStripMenuItem,
            this.ubahSupplierToolStripMenuItem,
            this.hapusSupplierToolStripMenuItem});
            this.supplierToolStripMenuItem.Name = "supplierToolStripMenuItem";
            this.supplierToolStripMenuItem.Size = new System.Drawing.Size(85, 27);
            this.supplierToolStripMenuItem.Text = "Supplier";
            // 
            // dataSupplierToolStripMenuItem
            // 
            this.dataSupplierToolStripMenuItem.Name = "dataSupplierToolStripMenuItem";
            this.dataSupplierToolStripMenuItem.Size = new System.Drawing.Size(204, 28);
            this.dataSupplierToolStripMenuItem.Text = "Data Supplier";
            this.dataSupplierToolStripMenuItem.Click += new System.EventHandler(this.dataSupplierToolStripMenuItem_Click);
            // 
            // tambahSupplierToolStripMenuItem
            // 
            this.tambahSupplierToolStripMenuItem.Name = "tambahSupplierToolStripMenuItem";
            this.tambahSupplierToolStripMenuItem.Size = new System.Drawing.Size(204, 28);
            this.tambahSupplierToolStripMenuItem.Text = "Tambah Supplier";
            this.tambahSupplierToolStripMenuItem.Click += new System.EventHandler(this.tambahSupplierToolStripMenuItem_Click);
            // 
            // ubahSupplierToolStripMenuItem
            // 
            this.ubahSupplierToolStripMenuItem.Name = "ubahSupplierToolStripMenuItem";
            this.ubahSupplierToolStripMenuItem.Size = new System.Drawing.Size(204, 28);
            this.ubahSupplierToolStripMenuItem.Text = "Ubah Supplier";
            this.ubahSupplierToolStripMenuItem.Click += new System.EventHandler(this.ubahSupplierToolStripMenuItem_Click);
            // 
            // hapusSupplierToolStripMenuItem
            // 
            this.hapusSupplierToolStripMenuItem.Name = "hapusSupplierToolStripMenuItem";
            this.hapusSupplierToolStripMenuItem.Size = new System.Drawing.Size(204, 28);
            this.hapusSupplierToolStripMenuItem.Text = "Hapus Supplier";
            this.hapusSupplierToolStripMenuItem.Click += new System.EventHandler(this.hapusSupplierToolStripMenuItem_Click);
            // 
            // menuLaporan
            // 
            this.menuLaporan.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.penjualanToolStripMenuItem,
            this.stokToolStripMenuItem,
            this.ppnPenjualanToolStripMenuItem,
            this.neracaToolStripMenuItem});
            this.menuLaporan.Name = "menuLaporan";
            this.menuLaporan.Size = new System.Drawing.Size(80, 27);
            this.menuLaporan.Text = "Laporan";
            // 
            // penjualanToolStripMenuItem
            // 
            this.penjualanToolStripMenuItem.Name = "penjualanToolStripMenuItem";
            this.penjualanToolStripMenuItem.Size = new System.Drawing.Size(161, 28);
            this.penjualanToolStripMenuItem.Text = "Jurnal";
            this.penjualanToolStripMenuItem.Click += new System.EventHandler(this.penjualanToolStripMenuItem_Click);
            // 
            // stokToolStripMenuItem
            // 
            this.stokToolStripMenuItem.Name = "stokToolStripMenuItem";
            this.stokToolStripMenuItem.Size = new System.Drawing.Size(161, 28);
            this.stokToolStripMenuItem.Text = "Buku Besar";
            this.stokToolStripMenuItem.Click += new System.EventHandler(this.stokToolStripMenuItem_Click);
            // 
            // ppnPenjualanToolStripMenuItem
            // 
            this.ppnPenjualanToolStripMenuItem.Name = "ppnPenjualanToolStripMenuItem";
            this.ppnPenjualanToolStripMenuItem.Size = new System.Drawing.Size(161, 28);
            this.ppnPenjualanToolStripMenuItem.Text = "Laba Rugi";
            this.ppnPenjualanToolStripMenuItem.Click += new System.EventHandler(this.ppnPenjualanToolStripMenuItem_Click);
            // 
            // neracaToolStripMenuItem
            // 
            this.neracaToolStripMenuItem.Name = "neracaToolStripMenuItem";
            this.neracaToolStripMenuItem.Size = new System.Drawing.Size(161, 28);
            this.neracaToolStripMenuItem.Text = "Neraca";
            this.neracaToolStripMenuItem.Click += new System.EventHandler(this.neracaToolStripMenuItem_Click);
            // 
            // mutasiToolStripMenuItem
            // 
            this.mutasiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.penjualanToolStripMenuItem1,
            this.pembelianToolStripMenuItem});
            this.mutasiToolStripMenuItem.Name = "mutasiToolStripMenuItem";
            this.mutasiToolStripMenuItem.Size = new System.Drawing.Size(64, 27);
            this.mutasiToolStripMenuItem.Text = "Retur";
            // 
            // penjualanToolStripMenuItem1
            // 
            this.penjualanToolStripMenuItem1.Name = "penjualanToolStripMenuItem1";
            this.penjualanToolStripMenuItem1.Size = new System.Drawing.Size(151, 28);
            this.penjualanToolStripMenuItem1.Text = "Penjualan";
            this.penjualanToolStripMenuItem1.Click += new System.EventHandler(this.penjualanToolStripMenuItem1_Click);
            // 
            // pembelianToolStripMenuItem
            // 
            this.pembelianToolStripMenuItem.Name = "pembelianToolStripMenuItem";
            this.pembelianToolStripMenuItem.Size = new System.Drawing.Size(151, 28);
            this.pembelianToolStripMenuItem.Text = "Pembelian";
            this.pembelianToolStripMenuItem.Click += new System.EventHandler(this.pembelianToolStripMenuItem_Click);
            // 
            // lblNama
            // 
            this.lblNama.AutoSize = true;
            this.lblNama.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblNama.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNama.Location = new System.Drawing.Point(23, 46);
            this.lblNama.Name = "lblNama";
            this.lblNama.Size = new System.Drawing.Size(176, 27);
            this.lblNama.TabIndex = 0;
            this.lblNama.Text = "Selamat Datang, ";
            // 
            // lblWaktu
            // 
            this.lblWaktu.AutoSize = true;
            this.lblWaktu.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblWaktu.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWaktu.Location = new System.Drawing.Point(1031, 46);
            this.lblWaktu.Name = "lblWaktu";
            this.lblWaktu.Size = new System.Drawing.Size(72, 27);
            this.lblWaktu.TabIndex = 0;
            this.lblWaktu.Text = "Waktu";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblTanggal
            // 
            this.lblTanggal.AutoSize = true;
            this.lblTanggal.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblTanggal.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTanggal.Location = new System.Drawing.Point(1031, 88);
            this.lblTanggal.Name = "lblTanggal";
            this.lblTanggal.Size = new System.Drawing.Size(82, 27);
            this.lblTanggal.TabIndex = 2;
            this.lblTanggal.Text = "Tanggal";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 633);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(332, 27);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tekan Tombol \'esc\' Untuk Kembali";
            // 
            // fmMenuUtama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 681);
            this.ControlBox = false;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTanggal);
            this.Controls.Add(this.lblWaktu);
            this.Controls.Add(this.lblNama);
            this.Controls.Add(this.menu);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menu;
            this.Name = "fmMenuUtama";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fmMenuUtama";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.fmMenuUtama_Load);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem menuNota;
        private System.Windows.Forms.ToolStripMenuItem filterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuLaporan;
        private System.Windows.Forms.ToolStripMenuItem penjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ppnPenjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notaSementaraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notaAsliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuBarang;
        private System.Windows.Forms.ToolStripMenuItem dataBarangToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem filterToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem sparePartToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem stokBarangToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perPartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem globalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tambahBarangToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hapusBarangToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuKaryawan;
        private System.Windows.Forms.ToolStripMenuItem dataKaryawanToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tambahKaryawanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ubahKaryawanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mutasiToolStripMenuItem;
        private System.Windows.Forms.Label lblNama;
        private System.Windows.Forms.Label lblWaktu;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem hapusKaryawanToolStripMenuItem;
        private System.Windows.Forms.Label lblTanggal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem pembeliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataPembeliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tambahPembeliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ubahPembeliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hapusPembeliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tambahSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ubahSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hapusSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ubahBarangToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penjualanToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pembelianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neracaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notaPenyusutanToolStripMenuItem;
    }
}