﻿namespace kp
{
    partial class fmNeraca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvAset = new System.Windows.Forms.DataGridView();
            this.txtTotalAset = new System.Windows.Forms.Label();
            this.dgvKewajiban = new System.Windows.Forms.DataGridView();
            this.dgvEkuitas = new System.Windows.Forms.DataGridView();
            this.txtTotalKewajiban = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTotalEkuitas = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTotalPasiva = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btn_penutupan = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKewajiban)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEkuitas)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(201, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "LAPORAN NERACA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(222, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "\"ANEKA FILTER\"";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(201, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Periode";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 291);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(160, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "TOTAL KEWAJIBAN:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 291);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(180, 20);
            this.label7.TabIndex = 7;
            this.label7.Text = "TOTAL AKTIVA (ASET):";
            // 
            // dgvAset
            // 
            this.dgvAset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAset.Location = new System.Drawing.Point(0, 3);
            this.dgvAset.Name = "dgvAset";
            this.dgvAset.Size = new System.Drawing.Size(661, 285);
            this.dgvAset.TabIndex = 10;
            // 
            // txtTotalAset
            // 
            this.txtTotalAset.AutoSize = true;
            this.txtTotalAset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalAset.Location = new System.Drawing.Point(186, 291);
            this.txtTotalAset.Name = "txtTotalAset";
            this.txtTotalAset.Size = new System.Drawing.Size(174, 20);
            this.txtTotalAset.TabIndex = 11;
            this.txtTotalAset.Text = "TOTAL PENDAPATAN:";
            // 
            // dgvKewajiban
            // 
            this.dgvKewajiban.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKewajiban.Location = new System.Drawing.Point(3, 6);
            this.dgvKewajiban.Name = "dgvKewajiban";
            this.dgvKewajiban.Size = new System.Drawing.Size(658, 282);
            this.dgvKewajiban.TabIndex = 12;
            // 
            // dgvEkuitas
            // 
            this.dgvEkuitas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEkuitas.Location = new System.Drawing.Point(6, 6);
            this.dgvEkuitas.Name = "dgvEkuitas";
            this.dgvEkuitas.Size = new System.Drawing.Size(655, 282);
            this.dgvEkuitas.TabIndex = 13;
            // 
            // txtTotalKewajiban
            // 
            this.txtTotalKewajiban.AutoSize = true;
            this.txtTotalKewajiban.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalKewajiban.Location = new System.Drawing.Point(172, 291);
            this.txtTotalKewajiban.Name = "txtTotalKewajiban";
            this.txtTotalKewajiban.Size = new System.Drawing.Size(160, 20);
            this.txtTotalKewajiban.TabIndex = 14;
            this.txtTotalKewajiban.Text = "TOTAL KEWAJIBAN:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(319, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 26);
            this.label10.TabIndex = 15;
            this.label10.Text = "Periode";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 291);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 20);
            this.label9.TabIndex = 16;
            this.label9.Text = "TOTAL EKUITAS:";
            // 
            // txtTotalEkuitas
            // 
            this.txtTotalEkuitas.AutoSize = true;
            this.txtTotalEkuitas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalEkuitas.Location = new System.Drawing.Point(148, 291);
            this.txtTotalEkuitas.Name = "txtTotalEkuitas";
            this.txtTotalEkuitas.Size = new System.Drawing.Size(136, 20);
            this.txtTotalEkuitas.TabIndex = 17;
            this.txtTotalEkuitas.Text = "TOTAL EKUITAS:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(115, 483);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(136, 20);
            this.label11.TabIndex = 18;
            this.label11.Text = "TOTAL EKUITAS:";
            // 
            // txtTotalPasiva
            // 
            this.txtTotalPasiva.AutoSize = true;
            this.txtTotalPasiva.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPasiva.Location = new System.Drawing.Point(270, 483);
            this.txtTotalPasiva.Name = "txtTotalPasiva";
            this.txtTotalPasiva.Size = new System.Drawing.Size(136, 20);
            this.txtTotalPasiva.TabIndex = 19;
            this.txtTotalPasiva.Text = "TOTAL EKUITAS:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 90);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(663, 347);
            this.tabControl1.TabIndex = 20;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvAset);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtTotalAset);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(655, 321);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Aktiva(Aset)";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvKewajiban);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.txtTotalKewajiban);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(655, 321);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Kewajiban";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgvEkuitas);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.txtTotalEkuitas);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(655, 321);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Ekuitas";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btn_penutupan
            // 
            this.btn_penutupan.Location = new System.Drawing.Point(603, 11);
            this.btn_penutupan.Name = "btn_penutupan";
            this.btn_penutupan.Size = new System.Drawing.Size(75, 23);
            this.btn_penutupan.TabIndex = 21;
            this.btn_penutupan.Text = "Penutupan";
            this.btn_penutupan.UseVisualStyleBackColor = true;
            this.btn_penutupan.Click += new System.EventHandler(this.btn_penutupan_Click);
            // 
            // fmNeraca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 526);
            this.Controls.Add(this.btn_penutupan);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtTotalPasiva);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "fmNeraca";
            this.Text = "fmNeraca";
            this.Load += new System.EventHandler(this.fmNeraca_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKewajiban)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEkuitas)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgvAset;
        private System.Windows.Forms.Label txtTotalAset;
        private System.Windows.Forms.DataGridView dgvKewajiban;
        private System.Windows.Forms.DataGridView dgvEkuitas;
        private System.Windows.Forms.Label txtTotalKewajiban;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label txtTotalEkuitas;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label txtTotalPasiva;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btn_penutupan;
    }
}