﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmBarangUbah : Form
    {
        MySqlConnection con = new MySqlConnection();
        public fmBarangUbah()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =aneka_filter; uid=root;";
        }

        private void fmUbahBarang_Load(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;
            groupBox2.Enabled = true;

            //Jenis Barang
            cboJenis.Text = "Pilih Jenis Barang ...";
            cboJenis.Items.Add("Oil");
            cboJenis.Items.Add("Fuel");
            cboJenis.Items.Add("Hyd");
            cboJenis.Items.Add("Air");
            cboJenis.Items.Add("Pilot");
            //Jenis Satuan
            cboSatuan.Text = "Pilih Satuan ...";
            cboSatuan.Items.Add("Pcs");
            cboSatuan.Items.Add("Set");
            cboSatuan.Items.Add("Doz");
            cboSatuan.Items.Add("Unit");
        }

        private void btnLihatData_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmBarangData();
            fm1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(txtKodeUbah.Text!="")
            {
                PilihKode();
            }
            else
            {
                MessageBox.Show("Isi Data!!!");
            }
        }
        #region Pilih Kode ke textbox
        public void PilihKode()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id, nama, jenis, min_kuantiti, satuan_kuantiti, harga_satuan FROM barang WHERE id='" + txtKodeUbah.Text + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                txtKode.Text = objReader.GetValue(0).ToString();
                txtNama.Text = objReader.GetValue(1).ToString();
                cboJenis.Text = objReader.GetValue(2).ToString();
                txtMinQty.Text = objReader.GetValue(3).ToString();
                cboSatuan.Text = objReader.GetValue(4).ToString();
                txtHarga.Text = objReader.GetValue(5).ToString();
            }
            MessageBox.Show("Berhasil Pilih Data Barang!!!");
            objReader.Close();
            con.Close();

            groupBox1.Enabled = true;
            groupBox2.Enabled = false;

        }


        #endregion

        private void btnbah_Click(object sender, EventArgs e)
        {
            UbahBarang();
        }
        #region Ubah Barang
        public void UbahBarang()
        {
            if (txtKode.Text != "" && txtNama.Text != "" && cboJenis.Text != "Pilih Jenis Barang ..." && txtMinQty.Text != "" && cboSatuan.Text != "Pilih Satuan ..." && txtHarga.Text != "")
            {
                con.Open();

                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                
                objKomen.CommandText = "UPDATE barang SET nama= @nama, jenis= @jenis, min_kuantiti= @minQty, satuan_kuantiti= @satuan, harga_satuan= @harga WHERE id='" + txtKodeUbah.Text + "'";
                //objKomen.Parameters.AddWithValue("@kode", txtKode.Text);
                objKomen.Parameters.AddWithValue("@nama", txtNama.Text);
                objKomen.Parameters.AddWithValue("@jenis", cboJenis.Text);
                objKomen.Parameters.AddWithValue("@minQty", txtMinQty.Text);
                objKomen.Parameters.AddWithValue("@satuan", cboSatuan.Text);
                objKomen.Parameters.AddWithValue("@harga", Convert.ToInt16(txtHarga.Text));

                int hasil = objKomen.ExecuteNonQuery();
                
                if (hasil > 0)
                {
                    MessageBox.Show("Berhasil Ubah Data Karyawan!!!");
                    txtKode.Text = "";
                    txtNama.Text = "";
                    cboJenis.Text = "Pilih jenis ...";
                    txtMinQty.Text = "";
                    cboSatuan.Text = "Pilih satuan ...";
                    txtHarga.Text = "";
                    txtKodeUbah.Text = "";
                    groupBox1.Enabled = false;
                    groupBox2.Enabled = true;
                }

                con.Close();
            }
            else
            {
                MessageBox.Show("Periksa Data!!!");
            }

        }

        #endregion

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }
    }
}
