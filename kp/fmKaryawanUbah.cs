﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmKaryawanUbah : Form
    {
        MySqlConnection con = new MySqlConnection();
        public fmKaryawanUbah()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =aneka_filter; uid=root;";
        }

        private void btnLihatData_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmKaryawanData();
            fm1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtNamaUbah.Text != "")
            {
                PilihKaryawan();
            }
            else
            {
                MessageBox.Show("Isi Data!!!");
            }
        }
        private void fmKaryawanUbah_Load(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;
            groupBox2.Enabled = true;

            cboJabatan.Text = "Pilih Jabatan ...";
            cboJabatan.Items.Add("Pemilik");
            cboJabatan.Items.Add("Karyawan");
        }

        #region Pilih Karyawan ke textbox
        public void PilihKaryawan()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT alamat, no_telp, kata_sandi, hak_akses, gaji FROM karyawan WHERE nama='"+txtNamaUbah.Text+"'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                txtAlamat.Text = objReader.GetValue(0).ToString();
                txtTelp.Text = objReader.GetValue(1).ToString();
                txtKataSandi.Text = objReader.GetValue(2).ToString();
                cboJabatan.Text = objReader.GetValue(3).ToString();
                txtGaji.Text = objReader.GetValue(4).ToString();
            }
            MessageBox.Show("Berhasil Pilih Data Karyawan!!!");
            objReader.Close();
            con.Close();

            groupBox1.Enabled = true;
            groupBox2.Enabled = false;
            
        }


        #endregion

        private void btnbah_Click(object sender, EventArgs e)
        {
            UbahKaryawan();
        }

        #region Ubah Karyawan
        public void UbahKaryawan()
        {
            if (txtAlamat.Text != "" && txtTelp.Text != "" && txtKataSandi.Text != "" && cboJabatan.Text != "Pilih Jabatan ..." && txtGaji.Text != "")
            {
                con.Open();

                    MySqlCommand objKomen = new MySqlCommand();
                    objKomen.Connection = con;
                    objKomen.CommandType = CommandType.Text;
                    objKomen.CommandText = "UPDATE karyawan SET alamat=@alamat, no_telp=@telp, hak_akses=@jabatan, kata_sandi=@sandi, gaji=@gaji WHERE nama='"+txtNamaUbah.Text+"'";
                    objKomen.Parameters.AddWithValue("@alamat", txtAlamat.Text);
                    objKomen.Parameters.AddWithValue("@telp", txtTelp.Text);
                    objKomen.Parameters.AddWithValue("@jabatan", cboJabatan.Text);
                    objKomen.Parameters.AddWithValue("@sandi", txtKataSandi.Text);
                    objKomen.Parameters.AddWithValue("@gaji", Convert.ToInt16(txtGaji.Text));

                    int hasil = objKomen.ExecuteNonQuery();

                    if (hasil > 0)
                    {
                        MessageBox.Show("Berhasil Ubah Data Karyawan!!!");
                        txtAlamat.Text = "";
                        txtTelp.Text = "";
                        txtKataSandi.Text = "";
                        cboJabatan.Text = "Pilih jabatan ...";
                        txtGaji.Text = "";
                        txtNamaUbah.Text = "";
                        groupBox1.Enabled = false;
                        groupBox2.Enabled = true;
                    }

                con.Close();
            }
            else
            {
                MessageBox.Show("Periksa Data!!!");
            }

        }

        #endregion

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }
    }
}
