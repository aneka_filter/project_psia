﻿namespace kp
{
    partial class fmReturNotaBeli
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvBarangNota = new System.Windows.Forms.DataGridView();
            this.txtKode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtKuantiti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSubHarga = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboNotaBeli = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNoNota = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCetak = new System.Windows.Forms.Button();
            this.dgvbarang = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtBarang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtHarga = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBarangNota)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvbarang)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvBarangNota
            // 
            this.dgvBarangNota.AllowUserToAddRows = false;
            this.dgvBarangNota.AllowUserToResizeColumns = false;
            this.dgvBarangNota.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvBarangNota.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvBarangNota.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBarangNota.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvBarangNota.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBarangNota.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtKode,
            this.txtKuantiti,
            this.txtSubHarga});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBarangNota.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvBarangNota.Location = new System.Drawing.Point(30, 55);
            this.dgvBarangNota.Name = "dgvBarangNota";
            this.dgvBarangNota.Size = new System.Drawing.Size(564, 212);
            this.dgvBarangNota.TabIndex = 141;
            this.dgvBarangNota.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBarangNota_CellContentClick);
            // 
            // txtKode
            // 
            this.txtKode.Frozen = true;
            this.txtKode.HeaderText = "Kode";
            this.txtKode.Name = "txtKode";
            this.txtKode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtKode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtKuantiti
            // 
            this.txtKuantiti.HeaderText = "Qty";
            this.txtKuantiti.Name = "txtKuantiti";
            this.txtKuantiti.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtKuantiti.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtSubHarga
            // 
            this.txtSubHarga.HeaderText = "Sub Harga";
            this.txtSubHarga.Name = "txtSubHarga";
            this.txtSubHarga.ReadOnly = true;
            this.txtSubHarga.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.txtSubHarga.Width = 120;
            // 
            // cboNotaBeli
            // 
            this.cboNotaBeli.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNotaBeli.FormattingEnabled = true;
            this.cboNotaBeli.Location = new System.Drawing.Point(99, 11);
            this.cboNotaBeli.Name = "cboNotaBeli";
            this.cboNotaBeli.Size = new System.Drawing.Size(168, 28);
            this.cboNotaBeli.TabIndex = 140;
            this.cboNotaBeli.SelectedIndexChanged += new System.EventHandler(this.cboNotaBeli_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 20);
            this.label2.TabIndex = 139;
            this.label2.Text = "Nota Beli :";
            // 
            // txtNoNota
            // 
            this.txtNoNota.Enabled = false;
            this.txtNoNota.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoNota.Location = new System.Drawing.Point(426, 14);
            this.txtNoNota.Name = "txtNoNota";
            this.txtNoNota.Size = new System.Drawing.Size(168, 28);
            this.txtNoNota.TabIndex = 147;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(330, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 20);
            this.label5.TabIndex = 148;
            this.label5.Text = "Kode Nota :";
            // 
            // btnCetak
            // 
            this.btnCetak.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCetak.Location = new System.Drawing.Point(1107, 309);
            this.btnCetak.Name = "btnCetak";
            this.btnCetak.Size = new System.Drawing.Size(77, 32);
            this.btnCetak.TabIndex = 142;
            this.btnCetak.Text = "Cetak";
            this.btnCetak.UseVisualStyleBackColor = true;
            this.btnCetak.Click += new System.EventHandler(this.btnCetak_Click);
            // 
            // dgvbarang
            // 
            this.dgvbarang.AllowUserToResizeColumns = false;
            this.dgvbarang.AllowUserToResizeRows = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvbarang.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvbarang.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvbarang.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvbarang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvbarang.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.txtBarang,
            this.txtHarga,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvbarang.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvbarang.Location = new System.Drawing.Point(620, 55);
            this.dgvbarang.Name = "dgvbarang";
            this.dgvbarang.Size = new System.Drawing.Size(564, 212);
            this.dgvbarang.TabIndex = 152;
            this.dgvbarang.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvbarang_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "Kode";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtBarang
            // 
            this.txtBarang.HeaderText = "Barang";
            this.txtBarang.Name = "txtBarang";
            this.txtBarang.ReadOnly = true;
            this.txtBarang.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtBarang.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtHarga
            // 
            this.txtHarga.HeaderText = "Harga";
            this.txtHarga.Name = "txtHarga";
            this.txtHarga.ReadOnly = true;
            this.txtHarga.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtHarga.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Qty";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Sub Harga";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 120;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(954, 280);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 20);
            this.label4.TabIndex = 151;
            this.label4.Text = "Total Harga :";
            // 
            // txtTotal
            // 
            this.txtTotal.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(1063, 275);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(121, 28);
            this.txtTotal.TabIndex = 150;
            // 
            // fmReturNotaBeli
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 409);
            this.Controls.Add(this.dgvbarang);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.dgvBarangNota);
            this.Controls.Add(this.cboNotaBeli);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNoNota);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCetak);
            this.Name = "fmReturNotaBeli";
            this.Text = "fmReturNotaBeli";
            this.Load += new System.EventHandler(this.fmReturNotaBeli_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBarangNota)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvbarang)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvBarangNota;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKode;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKuantiti;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtSubHarga;
        private System.Windows.Forms.ComboBox cboNotaBeli;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNoNota;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCetak;
        private System.Windows.Forms.DataGridView dgvbarang;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtBarang;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtHarga;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTotal;
    }
}