﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kp
{
    public partial class fmTampung : Form
    {
        string m_nama = "";
        public fmTampung(string nama)
        {
            InitializeComponent();
            m_nama = nama;
        }

        private void btnTransaksi_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmMenuTransaksi();
            fm1.Show();
            this.Hide();
        }

        private void btnLaporan_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmMenuUtama(m_nama);
            fm1.Show();
            this.Hide();
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                DialogResult result = MessageBox.Show("Apakah Keluar Program ?", 
                "Keluar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    this.Close();
                }
                else if (result == DialogResult.No)
                {
                }

                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }
    }
}
