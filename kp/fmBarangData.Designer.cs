﻿namespace kp
{
    partial class fmBarangData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.txtKode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtJenis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtKuantiti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSatuan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtHarga = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTotalData = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtKode,
            this.txtNama,
            this.txtJenis,
            this.txtKuantiti,
            this.txtSatuan,
            this.txtHarga});
            this.dgvData.Location = new System.Drawing.Point(40, 124);
            this.dgvData.Name = "dgvData";
            this.dgvData.Size = new System.Drawing.Size(544, 219);
            this.dgvData.TabIndex = 4;
            // 
            // txtKode
            // 
            this.txtKode.HeaderText = "Kode";
            this.txtKode.Name = "txtKode";
            // 
            // txtNama
            // 
            this.txtNama.HeaderText = "Nama";
            this.txtNama.Name = "txtNama";
            this.txtNama.ReadOnly = true;
            this.txtNama.Width = 130;
            // 
            // txtJenis
            // 
            this.txtJenis.HeaderText = "Jenis";
            this.txtJenis.Name = "txtJenis";
            this.txtJenis.ReadOnly = true;
            this.txtJenis.Width = 60;
            // 
            // txtKuantiti
            // 
            this.txtKuantiti.HeaderText = "Qty";
            this.txtKuantiti.Name = "txtKuantiti";
            this.txtKuantiti.ReadOnly = true;
            this.txtKuantiti.Width = 50;
            // 
            // txtSatuan
            // 
            this.txtSatuan.HeaderText = "Satuan";
            this.txtSatuan.Name = "txtSatuan";
            this.txtSatuan.ReadOnly = true;
            this.txtSatuan.Width = 60;
            // 
            // txtHarga
            // 
            this.txtHarga.HeaderText = "Harga";
            this.txtHarga.Name = "txtHarga";
            this.txtHarga.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(251, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 30);
            this.label1.TabIndex = 3;
            this.label1.Text = "DATA BARANG";
            // 
            // lblTotalData
            // 
            this.lblTotalData.AutoSize = true;
            this.lblTotalData.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalData.Location = new System.Drawing.Point(329, 87);
            this.lblTotalData.Name = "lblTotalData";
            this.lblTotalData.Size = new System.Drawing.Size(116, 23);
            this.lblTotalData.TabIndex = 5;
            this.lblTotalData.Text = "Total Data : ";
            // 
            // fmBarangData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 485);
            this.ControlBox = false;
            this.Controls.Add(this.lblTotalData);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.label1);
            this.Name = "fmBarangData";
            this.Text = "Form Data Barang";
            this.Load += new System.EventHandler(this.fmBarangData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTotalData;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKode;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNama;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtJenis;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKuantiti;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtSatuan;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtHarga;
    }
}