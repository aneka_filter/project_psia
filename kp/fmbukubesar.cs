﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmbukubesar : Form
    {
        MySqlConnection con = new MySqlConnection();
        string kodebarang;
        string[] splitTampungDate;
        string ubahFormatBulan = "";
        public string UbahFormatDate(string tahun, string bulan, string tanggal)
        {
            string a = tahun + "-" + bulan + "-" + tanggal;
            return a;
        }
        public fmbukubesar()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";
        }

        private void fmbukubesar_Load(object sender, EventArgs e)
        {
            #region cbodeclare
            cbo_jenisakun.Items.Add("Kas");
            cbo_jenisakun.Items.Add("Tabungan BCA");
            cbo_jenisakun.Items.Add("Tabungan MANDIRI");
            cbo_jenisakun.Items.Add("Piutang Dagang");
            cbo_jenisakun.Items.Add("Piutang Kartu Kredit");
            cbo_jenisakun.Items.Add("Sediaan Barang Dagangan");
            cbo_jenisakun.Items.Add("Sediaan Barang Dagangan rusak");
            cbo_jenisakun.Items.Add("Sediaan Habis Pakai Toko");
            cbo_jenisakun.Items.Add("Peralatan Toko");
            cbo_jenisakun.Items.Add("Akumulasi penyusutan toko");
            cbo_jenisakun.Items.Add("Kendaraan");
            cbo_jenisakun.Items.Add("Akumulasi penyusutan kendaraan");
            cbo_jenisakun.Items.Add("Tanah dan Bangunan");
            cbo_jenisakun.Items.Add("Hutang Dagang");
            cbo_jenisakun.Items.Add("Hutang Kartu Kredit");
            cbo_jenisakun.Items.Add("Hutang Gaji");
            cbo_jenisakun.Items.Add("Hutang Pajak Penjualan");
            cbo_jenisakun.Items.Add("Modal Pemilik");
            cbo_jenisakun.Items.Add("Penjualan Barang");
            cbo_jenisakun.Items.Add("Retur Penjualan & Penyesuaian Harga");
            cbo_jenisakun.Items.Add("HPP");
            cbo_jenisakun.Items.Add("Biaya Gaji Karyawan");
            cbo_jenisakun.Items.Add("Biaya Transportasi");
            cbo_jenisakun.Items.Add("Biaya Kartu Kredit");
            cbo_jenisakun.Items.Add("Biaya Sewa");
            cbo_jenisakun.Items.Add("Biaya Bunga");
            cbo_jenisakun.Items.Add("Biaya Sediaan Habis Pakai Toko");
            cbo_jenisakun.Items.Add("Biaya Penyusutan Peralatan Toko");
            cbo_jenisakun.Items.Add("Biaya Penyusutan Kendaraan");
            cbo_jenisakun.Items.Add("Biaya Administrasi Lain2");
            #endregion

            #region bulan
            string tampungDate = DateTime.Now.ToShortDateString();
            splitTampungDate = tampungDate.Split('/');
            if (splitTampungDate[0] == "1")
                ubahFormatBulan = "I";
            else if (splitTampungDate[0] == "2")
                ubahFormatBulan = "II";
            else if (splitTampungDate[0] == "3")
                ubahFormatBulan = "III";
            else if (splitTampungDate[0] == "4")
                ubahFormatBulan = "IV";
            else if (splitTampungDate[0] == "5")
                ubahFormatBulan = "V";
            else if (splitTampungDate[0] == "6")
                ubahFormatBulan = "VI";
            else if (splitTampungDate[0] == "7")
                ubahFormatBulan = "VII";
            else if (splitTampungDate[0] == "8")
                ubahFormatBulan = "VIII";
            else if (splitTampungDate[0] == "9")
                ubahFormatBulan = "IX";
            else if (splitTampungDate[0] == "10")
                ubahFormatBulan = "X";
            else if (splitTampungDate[0] == "11")
                ubahFormatBulan = "XI";
            else if (splitTampungDate[0] == "12")
                ubahFormatBulan = "XII";
            #endregion

            totalsebelumclosing();
        }
        public void AmbilDataSetelahClosing(string kode)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT saldo_setelah_closing FROM akun where no_akun ='" + kode + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();



            while (objReader.Read() == true)
            {
                lblSetelah.Text = objReader.GetValue(0).ToString();
                
            }

            objReader.Close();
            con.Close();
        }
        private void cbo_jenisakun_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region masukan data
            if (cbo_jenisakun.Text == "Kas")
            {
                cekDataSebelumClosing("10010");
                Saldoawal("10010");
                ambiljurnal("10010");
                totaldebitorkredit("10010");
                AmbilDataSetelahClosing("10010");
            }
            else if (cbo_jenisakun.Text == "Tabungan BCA")
            {
                cekDataSebelumClosing("10020");
                Saldoawal("10020");
                ambiljurnal("10020");
                totaldebitorkredit("10020");
                AmbilDataSetelahClosing("10020");
            }
            else if (cbo_jenisakun.Text == "Tabungan MANDIRI")
            {
                cekDataSebelumClosing("10030");
                Saldoawal("10030");
                ambiljurnal("10030");
                totaldebitorkredit("10030");
                AmbilDataSetelahClosing("10030");
            }
            else if (cbo_jenisakun.Text == "Piutang Dagang")
            {
                cekDataSebelumClosing("10040");
                Saldoawal("10040");
                ambiljurnal("10040");
                totaldebitorkredit("10040");
                AmbilDataSetelahClosing("10040");
            }
            else if (cbo_jenisakun.Text == "Piutang Kartu Kredit")
            {
                cekDataSebelumClosing("10050");
                Saldoawal("10050");
                ambiljurnal("10050");
                totaldebitorkredit("10050");
                AmbilDataSetelahClosing("10050");
            }
            else if (cbo_jenisakun.Text == "Sediaan Barang Dagangan")
            {
                cekDataSebelumClosing("10060");
                Saldoawal("10060");
                ambiljurnal("10060");
                totaldebitorkredit("10060");
                AmbilDataSetelahClosing("10060");
            }
            else if (cbo_jenisakun.Text == "Sediaan Barang Dagangan rusak")
            {
                cekDataSebelumClosing("10070");
                Saldoawal("10070");
                ambiljurnal("10070");
                totaldebitorkredit("10070");
                AmbilDataSetelahClosing("10070");
            }
            else if (cbo_jenisakun.Text == "Sediaan Habis Pakai Toko")
            {
                cekDataSebelumClosing("10080");
                Saldoawal("10080");
                ambiljurnal("10080");
                totaldebitorkredit("10080");
                AmbilDataSetelahClosing("10080");
            }
            else if (cbo_jenisakun.Text == "Peralatan Toko")
            {
                cekDataSebelumClosing("10090");
                Saldoawal("10090");
                ambiljurnal("10090");
                totaldebitorkredit("10090");
                AmbilDataSetelahClosing("10090");
            }
            else if (cbo_jenisakun.Text == "Akumulasi penyusutan toko")
            {
                cekDataSebelumClosing("10100");
                Saldoawal("10100");
                ambiljurnal("10100");
                totaldebitorkredit("10100");
                AmbilDataSetelahClosing("10100");
            }
            else if (cbo_jenisakun.Text == "Kendaraan")
            {
                cekDataSebelumClosing("10110");
                Saldoawal("10110");
                ambiljurnal("10110");
                totaldebitorkredit("10110");
                AmbilDataSetelahClosing("10110");
            }
            else if (cbo_jenisakun.Text == "Akumulasi penyusutan kendaraan")
            {
                cekDataSebelumClosing("10120");
                Saldoawal("10120");
                ambiljurnal("10120");
                totaldebitorkredit("10120");
                AmbilDataSetelahClosing("10120");
            }
            else if (cbo_jenisakun.Text == "Tanah dan Bangunan")
            {
                cekDataSebelumClosing("10130");
                Saldoawal("10130");
                ambiljurnal("10130");
                totaldebitorkredit("10130");
                AmbilDataSetelahClosing("10130");
            }
            else if (cbo_jenisakun.Text == "Hutang Dagang")
            {
                cekDataSebelumClosing("20010");
                Saldoawal("20010");
                ambiljurnal("20010");
                totaldebitorkredit("20010");
                AmbilDataSetelahClosing("20010");
            }
            else if (cbo_jenisakun.Text == "Hutang Kartu Kredit")
            {
                cekDataSebelumClosing("20020");
                Saldoawal("20020");
                ambiljurnal("20020");
                totaldebitorkredit("20020");
                AmbilDataSetelahClosing("20020");
            }
            else if (cbo_jenisakun.Text == "Hutang Gaji")
            {
                cekDataSebelumClosing("20030");
                Saldoawal("20030");
                ambiljurnal("20030");
                totaldebitorkredit("20030");
                AmbilDataSetelahClosing("20040");

            }
            else if (cbo_jenisakun.Text == "Hutang Pajak Penjualan")
            {
                cekDataSebelumClosing("20040");
                Saldoawal("20040");
                ambiljurnal("20040");
                totaldebitorkredit("20040");
                AmbilDataSetelahClosing("20040");
            }
            else if (cbo_jenisakun.Text == "Modal Pemilik")
            {
                cekDataSebelumClosing("30010");
                Saldoawal("30010");
                ambiljurnal("30010");
                totaldebitorkredit("30010");
                AmbilDataSetelahClosing("30030");
            }
            else if (cbo_jenisakun.Text == "Penjualan Barang")
            {
                cekDataSebelumClosing("40010");
                Saldoawal("40010");
                ambiljurnal("40010");
                totaldebitorkredit("40010");
                AmbilDataSetelahClosing("40010");
            }
            else if (cbo_jenisakun.Text == "Retur Penjualan & Penyesuaian Harga")
            {
                cekDataSebelumClosing("40020");
                Saldoawal("40020");
                ambiljurnal("40020");
                totaldebitorkredit("40020");
                AmbilDataSetelahClosing("40020");
            }
            else if (cbo_jenisakun.Text == "HPP")
            {
                cekDataSebelumClosing("50010");
                Saldoawal("50010");
                ambiljurnal("50010");
                totaldebitorkredit("50010");
                AmbilDataSetelahClosing("50010");
            }
            else if (cbo_jenisakun.Text == "Biaya Gaji Karyawan")
            {
                cekDataSebelumClosing("50020");
                Saldoawal("50020");
                ambiljurnal("50020");
                totaldebitorkredit("50020");
                AmbilDataSetelahClosing("50020");
            }
            else if (cbo_jenisakun.Text == "Biaya Transportasi")
            {
                cekDataSebelumClosing("50030");
                Saldoawal("50030");
                ambiljurnal("50030");
                totaldebitorkredit("50030");
                AmbilDataSetelahClosing("50030");
            }
            else if (cbo_jenisakun.Text == "Biaya Kartu Kredit")
            {
                cekDataSebelumClosing("50040");
                Saldoawal("50040");
                ambiljurnal("50040");
                totaldebitorkredit("50040");
                AmbilDataSetelahClosing("50040");
            }
            else if (cbo_jenisakun.Text == "Biaya Sewa")
            {
                cekDataSebelumClosing("50050");
                Saldoawal("50050");
                ambiljurnal("50050");
                totaldebitorkredit("50050");
                AmbilDataSetelahClosing("50050");
            }
           
            else if (cbo_jenisakun.Text == "Biaya Bunga")
            {
                cekDataSebelumClosing("50060");
                Saldoawal("50060");
                ambiljurnal("50060");
                totaldebitorkredit("50060");
                AmbilDataSetelahClosing("50060");
            }
            else if (cbo_jenisakun.Text == "Biaya Sediaan Habis Pakai Toko")
            {
                cekDataSebelumClosing("50070");
                Saldoawal("50070");
                ambiljurnal("50070");
                totaldebitorkredit("50070");
                AmbilDataSetelahClosing("50070");
            }
            else if (cbo_jenisakun.Text == "Biaya Penyusutan Peralatan Toko")
            {
                cekDataSebelumClosing("50080");
                Saldoawal("50080");
                ambiljurnal("50080");
                totaldebitorkredit("50080");
                AmbilDataSetelahClosing("50080");
            }
            else if (cbo_jenisakun.Text == "Biaya Penyusutan Kendaraan")
            {
                cekDataSebelumClosing("50090");
                Saldoawal("50090");
                ambiljurnal("50090");
                totaldebitorkredit("50090");
                AmbilDataSetelahClosing("50090");
            }
            else if (cbo_jenisakun.Text == "Biaya Administrasi Lain2")
            {
                cekDataSebelumClosing("50100");
                Saldoawal("50100");
                ambiljurnal("50100");
                totaldebitorkredit("50100");
                AmbilDataSetelahClosing("50100");
            }
            #endregion
        }
        public void Saldoawal(String kode)
        {
            con.Open();
            string tanggal = "";
            string ket = "";
            string no_bukti = "";
            string debet = "";
            string kredit = "";
            string saldokredit = "";
            string saldodebet = "";

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT nama_akun,saldo_awal,saldo_normal,saldo_sebelum_closing FROM `akun` WHERE no_akun ='" + kode + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            dgvData.Rows.Clear();
            while (objReader.Read() == true)
            {
                if (objReader.GetValue(2).ToString() == "Debet")
                {
                    ket = "saldo awal";
                    saldodebet = objReader.GetValue(1).ToString();
                    dgvData.Rows.Add(tanggal, ket, debet, kredit, saldodebet,saldokredit, no_bukti);
                }
                else
                {
                    ket = "saldo awal";
                    saldokredit = objReader.GetValue(1).ToString();
                    dgvData.Rows.Add(tanggal, ket, debet, kredit, saldodebet, saldokredit, no_bukti);
                }
            }
            lblSebelum.Text = objReader.GetValue(3).ToString();
            objReader.Close();
            con.Close();
        }
        public void totaldebitorkredit(String kode)
        {
            String saldonormal = dapatsaldonormal(kode);
            if(saldonormal == "Debet")
            {
               int panjang=  dgvData.RowCount -1;
                for(int i =1; i< panjang;i++)
                {
                    long awal = Convert.ToInt64(dgvData.Rows[i - 1].Cells[4].Value);
                    long debet = Convert.ToInt64(dgvData.Rows[i].Cells[2].Value);
                    long kredit = Convert.ToInt64(dgvData.Rows[i].Cells[3].Value);
                    long total = awal + (debet - kredit);
                    dgvData.Rows[i].Cells[4].Value = total;
                }
            }
            else
            {
                int panjang = dgvData.RowCount-1;
                for (int i = 1; i < panjang; i++)
                {
                    long awal = Convert.ToInt64(dgvData.Rows[i - 1].Cells[5].Value);
                    long debet = Convert.ToInt64(dgvData.Rows[i].Cells[2].Value);
                    long kredit = Convert.ToInt64(dgvData.Rows[i].Cells[3].Value);
                    long total = awal + (kredit -debet);
                    dgvData.Rows[i].Cells[5].Value = total;
                }
            }

        }
        #region simpansaldosetelahpenutupan
        public void ambiljurnal(String kode)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT j.tanggal, j.keterangan,  aj.debet, aj.kredit, j.no_bukti  "
                +"FROM jurnal j INNER JOIN akun_has_jurnal aj ON j.id_jurnal=aj.id_jurnal INNER JOIN akun a ON aj.no_akun = a.no_akun "+
                "where a.no_akun ='"+kode+"'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            string tanggal = "";
            string ket = "";
            string no_bukti = "";
            string debet = "";
            string kredit = "";
            string saldokredit = "";
            string saldodebet = "";

            
            while (objReader.Read() == true)
            {
                tanggal = objReader.GetValue(0).ToString();
                ket = objReader.GetValue(1).ToString();
                debet = objReader.GetValue(2).ToString();
                kredit = objReader.GetValue(3).ToString();
                no_bukti = objReader.GetValue(4).ToString();
                dgvData.Rows.Add(tanggal, ket, debet, kredit, saldodebet, saldokredit, no_bukti);
            }

            objReader.Close();
            con.Close();
        }
        public String dapatsaldonormal(String kode)
        {
            con.Open(); 
            String saldonomrnaml = "";
            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT saldo_normal FROM `akun` WHERE no_akun = '"+kode+"'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            while (objReader.Read() == true)
            {
                saldonomrnaml = objReader.GetValue(0).ToString();
            }

            objReader.Close();
            con.Close();


            return saldonomrnaml;
        }
        public void totalsebelumclosing()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT no_akun, nama_akun," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak" +
            " ORDER BY saldo_normal";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            int k = 0;
            string[,] data = new string[33, 3];

            while (objReader.Read() == true)
            {
                data[k, 0] = objReader.GetValue(0).ToString();
                data[k, 1] = objReader.GetValue(1).ToString();
                data[k, 2] = objReader.GetValue(2).ToString();
                //MessageBox.Show(data[k, 2]);
                k++;
            }
            objReader.Close();

            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            int hasil = 0;
            for (int i = 0; i < 33;i++ )
            {
                if(data[i,0] == null)
                {
                    break;
                }
                else
                {
                    if(data[i,2] == ""||data[i,2]==null)
                    {
                        data[i, 2] = "0";
                    }
                    objKomen.CommandText = "UPDATE akun SET saldo_sebelum_closing= "+ Convert.ToInt64(data[i,2])+" WHERE no_akun='" + data[i, 0] + "'";
                    
                        //objKomen.Parameters.AddWithValue("@test", data[i, 2]);
                        hasil += objKomen.ExecuteNonQuery();
                }
                
            }

            if (hasil > 0)
            {
                
            }
            con.Close();
        }
        public void cekDataSebelumClosing( string kode)
        {
            con.Open();
            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            objKomen.CommandText = "SELECT saldo_sebelum_closing FROM `akun` Where no_akun='"+kode+"'";
            MySqlDataReader objReader;
            objReader = objKomen.ExecuteReader();
            int saldoSebelum = 0;
            while (objReader.Read() == true)
            {
                saldoSebelum = int.Parse(objReader.GetValue(0).ToString());
            }
            objReader.Close();

            MySqlCommand objKomen2 = new MySqlCommand();
            objKomen2.Connection = con;
            objKomen2.CommandType = CommandType.Text;
            objKomen2.CommandText = "SELECT saldo_awal FROM `akun` Where no_akun='" + kode + "'";
            MySqlDataReader objReader2;
            objReader2 = objKomen2.ExecuteReader();
            int saldoAwal2 =0;
            while (objReader2.Read() == true)
            {
                saldoAwal2 = int.Parse(objReader2.GetValue(0).ToString());
            }
            objReader2.Close();

            MySqlCommand objKomen1 = new MySqlCommand();
            objKomen1.Connection = con;
            objKomen1.CommandType = CommandType.Text;
            int hasil = 0;
            if(saldoSebelum == 0)
                objKomen.CommandText = "UPDATE akun SET saldo_sebelum_closing= " + saldoAwal2 + " WHERE no_akun='" + kode + "'";

            //objKomen.Parameters.AddWithValue("@test", data[i, 2]);
            hasil += objKomen.ExecuteNonQuery();

            if (hasil > 0)
            {

            }
            con.Close();
        }
        public void totalsesudahclosingasetlancar()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT no_akun, nama_akun," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak where id_kelompok_akun = 1 " +
            " ORDER BY saldo_normal";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            int k = 0;
            string[,] data = new string[33, 3];

            while (objReader.Read() == true)
            {
                data[k, 0] = objReader.GetValue(0).ToString();
                data[k, 1] = objReader.GetValue(1).ToString();
                data[k, 2] = objReader.GetValue(2).ToString();
                //MessageBox.Show(data[k, 2]);
                k++;
            }
            objReader.Close();

            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            int hasil = 0;
            for (int i = 0; i < 33; i++)
            {
                if (data[i, 0] != null)
                {

                    if (data[i, 2] == "")
                    {
                        data[i, 2] = "0";
                    }
                    objKomen.CommandText = "UPDATE akun SET saldo_setelah_closing= " + Convert.ToInt64(data[i, 2]) + " WHERE no_akun='" + data[i, 0] + "'";
                    //objKomen.Parameters.AddWithValue("@test", data[i, 2]);
                    hasil += objKomen.ExecuteNonQuery();
                    
                }
                else
                {
                    break;
                }

            }

            if (hasil > 0)
            {

            }
            con.Close();
        }
        public void totalsesudahclosingasettidaklancar()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT no_akun, nama_akun," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak where id_kelompok_akun = 2 " +
            " ORDER BY saldo_normal";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            int k = 0;
            string[,] data = new string[33, 3];

            while (objReader.Read() == true)
            {
                data[k, 0] = objReader.GetValue(0).ToString();
                data[k, 1] = objReader.GetValue(1).ToString();
                data[k, 2] = objReader.GetValue(2).ToString();
                //MessageBox.Show(data[k, 2]);
                k++;
            }
            objReader.Close();

            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            int hasil = 0;
            for (int i = 0; i < 33; i++)
            {
                if (data[i, 0] != null)
                {
                    if (data[i, 2] == "")
                    {
                        data[i, 2] = "0";
                    }
                    objKomen.CommandText = "UPDATE akun SET saldo_setelah_closing= " + Convert.ToInt64(data[i, 2]) + " WHERE no_akun='" + data[i, 0] + "'";
                    //objKomen.Parameters.AddWithValue("@test", data[i, 2]);
                    hasil += objKomen.ExecuteNonQuery();
                    
                }
                else
                {
                    break;
                }

            }

            if (hasil > 0)
            {

            }
            con.Close();
        }
        public void totalsesudahclosingkewajiban()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT no_akun, nama_akun," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak where id_kelompok_akun = 3 " +
            " ORDER BY saldo_normal";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            int k = 0;
            string[,] data = new string[33, 3];

            while (objReader.Read() == true)
            {
                data[k, 0] = objReader.GetValue(0).ToString();
                data[k, 1] = objReader.GetValue(1).ToString();
                data[k, 2] = objReader.GetValue(2).ToString();
                //MessageBox.Show(data[k, 2]);
                k++;
            }
            objReader.Close();

            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            int hasil = 0;
            for (int i = 0; i < 33; i++)
            {
                if (data[i, 0] != null)
                {
                    if (data[i, 2] == "")
                    {
                        data[i, 2] = "0";
                    }
                    objKomen.CommandText = "UPDATE akun SET saldo_setelah_closing= " + Convert.ToInt64(data[i, 2]) + " WHERE no_akun='" + data[i, 0] + "'";
                    //objKomen.Parameters.AddWithValue("@test", data[i, 2]);
                    hasil += objKomen.ExecuteNonQuery();

                }
                else
                {
                    break;
                }

            }

            if (hasil > 0)
            {

            }
            con.Close();
        }
        public void totalsesudahclosingekuitas()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT no_akun, nama_akun," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak where id_kelompok_akun = 4 " +
            " ORDER BY saldo_normal";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            int k = 0;
            string[,] data = new string[33, 3];

            while (objReader.Read() == true)
            {
                data[k, 0] = objReader.GetValue(0).ToString();
                data[k, 1] = objReader.GetValue(1).ToString();
                data[k, 2] = objReader.GetValue(2).ToString();
                //MessageBox.Show(data[k, 2]);
                k++;
            }
            objReader.Close();

            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            int hasil = 0;
            for (int i = 0; i < 33; i++)
            {
                if (data[i, 0] != null)
                {
                    if (data[i, 2] == "")
                    {
                        data[i, 2] = "0";
                    }
                    objKomen.CommandText = "UPDATE akun SET saldo_setelah_closing= " + Convert.ToInt64(data[i, 2]) + " WHERE no_akun='" + data[i, 0] + "'";
                    //objKomen.Parameters.AddWithValue("@test", data[i, 2]);
                    hasil += objKomen.ExecuteNonQuery();

                }
                else
                {
                    break;
                }

            }

            if (hasil > 0)
            {

            }
            con.Close();
        }
        public void totalsesudahclosingpendapatan()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT no_akun, nama_akun," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak where id_kelompok_akun = 4 " +
            " ORDER BY saldo_normal";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            int k = 0;
            string[,] data = new string[33, 3];

            while (objReader.Read() == true)
            {
                data[k, 0] = objReader.GetValue(0).ToString();
                data[k, 1] = objReader.GetValue(1).ToString();
                data[k, 2] = objReader.GetValue(2).ToString();
                //MessageBox.Show(data[k, 2]);
                k++;
            }
            objReader.Close();

            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            int hasil = 0;
            for (int i = 0; i < 33; i++)
            {
                if (data[i, 0] != null)
                {
                    if (data[i, 2] == "")
                    {
                        data[i, 2] = "0";
                    }
                    objKomen.CommandText = "UPDATE akun SET saldo_setelah_closing= " + Convert.ToInt64(data[i, 2]) + " WHERE no_akun='" + data[i, 0] + "'";
                    //objKomen.Parameters.AddWithValue("@test", data[i, 2]);
                    hasil += objKomen.ExecuteNonQuery();

                }
                else
                {
                    break;
                }

            }

            if (hasil > 0)
            {

            }
            con.Close();
        }
        public void totalsesudahclosingbiaya()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT no_akun, nama_akun," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak where id_kelompok_akun = 2 " +
            " ORDER BY saldo_normal";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            int k = 0;
            string[,] data = new string[33, 3];

            while (objReader.Read() == true)
            {
                data[k, 0] = objReader.GetValue(0).ToString();
                data[k, 1] = objReader.GetValue(1).ToString();
                data[k, 2] = objReader.GetValue(2).ToString();
                //MessageBox.Show(data[k, 2]);
                k++;
            }
            objReader.Close();

            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            int hasil = 0;
            for (int i = 0; i < 33; i++)
            {
                if (data[i, 0] != null)
                {
                    if (data[i, 2] == "")
                    {
                        data[i, 2] = "0";
                    }
                    objKomen.CommandText = "UPDATE akun SET saldo_setelah_closing= " + Convert.ToInt64(data[i, 2]) + " WHERE no_akun='" + data[i, 0] + "'";
                    //objKomen.Parameters.AddWithValue("@test", data[i, 2]);
                    hasil += objKomen.ExecuteNonQuery();

                }
                else
                {
                    break;
                }

            }

            if (hasil > 0)
            {

            }
            con.Close();
        }
        #endregion

        #region method penutupan
        public int AmbilIdJurnal(String kode)
        {
            int id = 0;
            con.Open();
            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id_jurnal FROM jurnal WHERE no_bukti='" + kode + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                id = Convert.ToInt16(objReader.GetValue(0));
            }
            objReader.Close();
            con.Close();
            return id;
        }
        private void tutuppendapatan()
        {
            String nomor = NomorNota(ubahFormatBulan, splitTampungDate[2]);
            jurnal("Pendapatan", nomor);
            int idjurnal = AmbilIdJurnal(nomor);
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT no_akun, saldo_normal," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak" +
            " WHERE id_kelompok_akun = 5" +
            " ORDER BY saldo_normal";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            int k = 0;
            double total = 0;
            String[,] data = new string[20, 5];

            while (objReader.Read() == true)
            {

                data[k, 0] = objReader.GetValue(0).ToString();
                data[k, 1] = idjurnal.ToString();
                data[k, 2] = (k + 1).ToString();

                double debet = 0;
                double kredit = 0;
                if (objReader.GetValue(1).ToString() == "Debet")
                {

                    String coba = objReader.GetValue(2).ToString();
                    if (objReader.GetValue(2).ToString() == "")
                    {
                        kredit = 0;
                    }
                    else
                    {
                        kredit = int.Parse(objReader.GetValue(2).ToString());
                    }

                    total += kredit;
                    data[k, 4] = kredit.ToString();
                    data[k, 3] = "0";
                }
                else
                {
                    String coba = objReader.GetValue(2).ToString();
                    if (objReader.GetValue(2).ToString() == "")
                    {
                        debet = 0;
                    }
                    else
                    {
                        debet = int.Parse(objReader.GetValue(2).ToString());
                    }
                    total -= debet;
                    data[k, 3] = debet.ToString();
                    data[k, 4] = "0";
                }

                k++;

                //dgvBarangNota.Rows.Add(kodeTampung, nama, harga_satuan);
            }


            objReader.Close();
            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            for (int i = 0; i < 20; i++)
            {
                if (data[i, 0] != null)
                {
                    objKomen.CommandText = "Insert Into akun_has_jurnal Values (" + int.Parse(data[i, 0]) +
                        ", " + int.Parse(data[i, 1]) + "," + int.Parse(data[i, 2]) + ", " + double.Parse(data[i, 3]) + ", " + double.Parse(data[i, 4]) + ")";
                    objKomen.ExecuteNonQuery();
                }
                else

                    break;
            }
            objKomen.CommandText = "Insert Into akun_has_jurnal Values (" + 70010 +
                        ", " + idjurnal + "," + (k + 1) + ", " + 0 + ", " + total + ")";
            objKomen.ExecuteNonQuery();



            con.Close();

        }
        private void tutupbiaya()
        {
            String nomor = NomorNota(ubahFormatBulan, splitTampungDate[2]);
            jurnal("Biaya", nomor);
            int idjurnal = AmbilIdJurnal(nomor);
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT no_akun, saldo_normal," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak" +
            " WHERE id_kelompok_akun = 6" +
            " ORDER BY saldo_normal";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            int k = 0;
            double total = 0;
            String[,] data = new string[20, 5];

            while (objReader.Read() == true)
            {

                data[k, 0] = objReader.GetValue(0).ToString();
                data[k, 1] = idjurnal.ToString();
                data[k, 2] = (k + 1).ToString();

                double debet = 0;
                double kredit = 0;
                if (objReader.GetValue(1).ToString() == "Debet")
                {
                    
                    String coba = objReader.GetValue(2).ToString();
                    if(objReader.GetValue(2).ToString()=="")
                    {
                        kredit = 0;
                    }
                    else
                    {
                        kredit = int.Parse(objReader.GetValue(2).ToString());
                    }
                   
                    total += kredit;
                    data[k, 4] = kredit.ToString();
                    data[k, 3] = "0";
                }
                else
                {
                    String coba = objReader.GetValue(2).ToString();
                    if (objReader.GetValue(2).ToString() == "")
                    {
                        debet = 0;
                    }
                    else
                    {
                        debet = int.Parse(objReader.GetValue(2).ToString());
                    }
                    total -= debet;
                    data[k, 3] = debet.ToString();
                    data[k, 4] = "0";
                }

                k++;

                //dgvBarangNota.Rows.Add(kodeTampung, nama, harga_satuan);
            }


            objReader.Close();
            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            for (int i = 0; i < 20; i++)
            {
                if (data[i, 0] != null)
                {
                    objKomen.CommandText = "Insert Into akun_has_jurnal Values (" + int.Parse(data[i, 0]) +
                        ", " + int.Parse(data[i, 1]) + "," + int.Parse(data[i, 2]) + ", " + double.Parse(data[i, 3]) + ", " + double.Parse(data[i, 4]) + ")";
                    objKomen.ExecuteNonQuery();
                }
                else

                    break;
            }
            objKomen.CommandText = "Insert Into akun_has_jurnal Values (" + 70020 +
                        ", " + idjurnal + "," + (k + 1) + ", " + total + ", " + 0 + ")";
            objKomen.ExecuteNonQuery();



            con.Close();
        }
        private void tutuplabarugi()
        {
            String nomor = NomorNota(ubahFormatBulan, splitTampungDate[2]);
            jurnal("Laba Rugi", nomor);
            int idjurnal = AmbilIdJurnal(nomor);
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT 'modal',((SELECT IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +

            "   FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            "   FROM akun ak" +
            "    WHERE id_kelompok_akun = 6" +
            "    GROUP by id_kelompok_akun" +
            "    ORDER BY saldo_normal) +(SELECT IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +

            "   FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            "   FROM akun ak" +
            "    WHERE id_kelompok_akun = 5" +
            "    GROUP by id_kelompok_akun" +
            "   ORDER BY saldo_normal) ) as total FROM akun LIMIT 1";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            double total = 0;

            while (objReader.Read() == true)
            {

                total = int.Parse(objReader.GetValue(1).ToString());
                //dgvBarangNota.Rows.Add(kodeTampung, nama, harga_satuan);
            }


            objReader.Close();

            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            objKomen.CommandText = "Insert Into akun_has_jurnal Values (" + 30010 +
                              ", " + idjurnal + "," + 1 + ", " + 0 + ", " + total + ")";
            objKomen.ExecuteNonQuery();


            objKomen.CommandText = "Insert Into akun_has_jurnal Values (" + 70030 +
                        ", " + idjurnal + "," + 2 + ", " + total + ", " + 0 + ")";
            objKomen.ExecuteNonQuery();
            con.Close();
        }
        public String NomorNota(string bulan, String Tahun)
        {
            con.Open();
            String kode = "";
            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT Count(*) FROM jurnal Where jenis = 'penutup'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                int ubahDataInt = Convert.ToInt16(objReader.GetValue(0)) + 1;
                string data = ubahDataInt.ToString();
                string kodeNota = "JP " + data + "/" + bulan + "/" + Tahun;
                kode = kodeNota;
                //MessageBox.Show(data+"/"+bulan+"/"+Tahun);
            }
            objReader.Close();
            con.Close();
            return kode;
        }
        private void jurnal(String Kode, String nomor)
        {
            String kt = "Penutupan - " + Kode;

            try
            {
                con.Open();
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into jurnal Values (@id, @jenis, @no_bukti, @keterangan, @tgl_penyusutan)";
                objKomen.Parameters.AddWithValue("@id", "");
                objKomen.Parameters.AddWithValue("@jenis", "penutup");
                objKomen.Parameters.AddWithValue("@no_bukti", nomor);
                objKomen.Parameters.AddWithValue("@keterangan", kt);
                objKomen.Parameters.AddWithValue("@tgl_penyusutan", UbahFormatDate(splitTampungDate[2], splitTampungDate[0], splitTampungDate[1]));
                int hasil = objKomen.ExecuteNonQuery();
                if (hasil > 0)
                {
                    MessageBox.Show("jurnal masuk");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        #endregion

        private void btn_tutup_Click(object sender, EventArgs e)
        {
            totalsesudahclosingasetlancar();
            totalsesudahclosingasettidaklancar();
            totalsesudahclosingkewajiban();
            tutuplabarugi();
            tutuppendapatan();
            tutupbiaya();
            totalsesudahclosingekuitas();
            totalsesudahclosingpendapatan();
            totalsesudahclosingbiaya();
        }
    }
}
