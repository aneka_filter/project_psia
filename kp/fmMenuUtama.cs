﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kp
{
    public partial class fmMenuUtama : Form
    {
        string m_nama = "";
        public fmMenuUtama(string nama)
        {
            InitializeComponent();
            lblNama.Text += nama;
            m_nama = nama;
        }

        private void spareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmNotaBeli();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void fmMenuUtama_Load(object sender, EventArgs e)
        {
            #region Interval dan Aktifkan Timer
            timer1.Interval = 1000;
            timer1.Enabled = true;
            #endregion

           
        }

        private void stokBarangToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Form fm1 = new fm();
            //fm1.MdiParent = this;
            //fm1.Show();
        }


        private void notaJualKreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmMasuk();
            fm1.MdiParent = this;
            fm1.Show();
        }


        private void globalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Form fm1 = new fmStokBarang();
            //fm1.MdiParent = this;
            //fm1.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            #region inisialisasi ke laberl waktu dan tanggal
            lblWaktu.Text = DateTime.Now.ToLongTimeString();
            lblTanggal.Text = DateTime.Now.ToLongDateString();
            #endregion
        }

        private void dataKaryawanToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmKaryawanData();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void tambahKaryawanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmKaryawanTambah();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void ubahKaryawanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmKaryawanUbah();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void hapusKaryawanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmKaryawanHapus();
            fm1.MdiParent = this;
            fm1.Show();
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                Form fm1 = new fmTampung(m_nama);
                fm1.Show();
                this.Close();
                return true;
            }
            else if (keyData == Keys.Right || keyData == Keys.Left || keyData == Keys.Up || keyData == Keys.Down )
            {
                menu.Select();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }

        private void tambahBarangToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmBarangData();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void ubahBarangToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmBarangUbah();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void hapusBarangToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmKaryawanHapus();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void filterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmNotaJual();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void penjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmLaporanJurnal();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void notaSementaraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmNotaBayar();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void notaAsliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmNotaBiaya();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void penjualanToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmreturnotajual();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void pembelianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmReturNotaBeli();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void stokToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmbukubesar();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void ppnPenjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmLaporanLabaRugi();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void nilaiToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void neracaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmLaporanNeraca();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void notaPenyusutanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmNotaPenyusutan();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void dataBarangToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmBarangData();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void dataPembeliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmPembeliData();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void tambahPembeliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmPembeliTambah();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void ubahPembeliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmPembeliUbah();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void hapusPembeliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmPembeliHapus();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void dataSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmsupplierData();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void tambahSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmSupplierTambah();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void ubahSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmSupplierUbah();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void hapusSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmSupplierHapus();
            fm1.MdiParent = this;
            fm1.Show();
        }
    }
}
