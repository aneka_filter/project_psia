﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmNotaJual : Form
    {
        MySqlConnection con = new MySqlConnection();
        int i = 0;
        string ubahFormatBulan = "";
        string[] splitTampungDate;
        long totalHarga = 0;
        long totalharga_beli = 0;
        long terimaUang = 0;
        long tampungUbah = 0;
        int cekKuanti = 0;
        public fmNotaJual()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";

            #region Cara Bayar Default
            cboCaraBayar.Text = "Pilih Cara Bayar ...";
            cboCaraBayar.Items.Add("Tunai");
            cboCaraBayar.Items.Add("Transfer/Debit");
            cboCaraBayar.Items.Add("Credit Card");
            #endregion

            #region bank
            cboNamaBank.Items.Add("BCA");
            cboNamaBank.Items.Add("MANDIRI");
            #endregion

            #region Daftar Pembeli Default
            cboNamaPembeli.Items.Add("Non-Member");
            DaftarNamaPembeli();
            #endregion
        }

        private void fmNotaJual_Load(object sender, EventArgs e)
        {
            //--Tampilan Default--
            cboNamaPembeli.Text = "Pilih Pembeli ...";
            //--End Tampilan Default--

            string tampungDate = DateTime.Now.ToShortDateString();
            splitTampungDate = tampungDate.Split('/');
            if(splitTampungDate[0]=="1")
                ubahFormatBulan = "I";
            else if(splitTampungDate[0]=="2")
                ubahFormatBulan = "II";
            else if (splitTampungDate[0] == "3")
                ubahFormatBulan = "III";
            else if (splitTampungDate[0] == "4")
                ubahFormatBulan = "IV";
            else if (splitTampungDate[0] == "5")
                ubahFormatBulan = "V";
            else if (splitTampungDate[0] == "6")
                ubahFormatBulan = "VI";
            else if (splitTampungDate[0] == "7")
                ubahFormatBulan = "VII";
            else if (splitTampungDate[0] == "8")
                ubahFormatBulan = "VIII";
            else if (splitTampungDate[0] == "9")
                ubahFormatBulan = "IX";
            else if (splitTampungDate[0] == "10")
                ubahFormatBulan = "X";
            else if (splitTampungDate[0] == "11")
                ubahFormatBulan = "XI";
            else if (splitTampungDate[0] == "12")
                ubahFormatBulan = "XII";


            NomorNota(ubahFormatBulan, splitTampungDate[2]);

            //MessageBox.Show(tampungDate);
            //MessageBox.Show(splitTampungDate[0]);
            //MessageBox.Show(splitTampungDate[2]);
        }

        private void dgvBarangNota_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            #region Isi Data
            int rowIndex = e.RowIndex;
            dgvBarangNota.Rows[rowIndex].Cells[0].Value = "";
            dgvBarangNota.Rows[rowIndex].Cells[1].Value = "";
            dgvBarangNota.Rows[rowIndex].Cells[2].Value = "";
            dgvBarangNota.Rows[rowIndex].Cells[3].Value = "";

            //MessageBox.Show(dgvBarangNota.);
            #endregion
        }

        private void dgvBarangNota_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            #region Isi Data
            int rowIndex = e.RowIndex;
            tampungUbah = Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[4].Value);
            #endregion
        }
        private void dgvBarangNota_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            #region Isi Data
            try
            {
                int rowIndex = e.RowIndex;
                int columnIndex = e.ColumnIndex;

                string cek = dgvBarangNota.Rows[rowIndex].Cells[0].Value.ToString();
                CekData(cek, rowIndex);
                if (dgvBarangNota.Rows[rowIndex].Cells[3].Value != null)
                {
                    totalHarga -= tampungUbah;
                    try{
                        long hitungSub = Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[2].Value) * Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[3].Value);
                        dgvBarangNota.Rows[rowIndex].Cells[4].Value = hitungSub;
                        
                    }catch{
                        dgvBarangNota.Rows[rowIndex].Cells[3].Value = 0;
                        dgvBarangNota.Rows[rowIndex].Cells[4].Value = 0;
                    }
                    totalHarga += Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[4].Value);
                    txtTotal.Text = totalHarga.ToString();
                }
                else
                {
                    long hitungSub = Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[2].Value) * Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[3].Value);
                    dgvBarangNota.Rows[rowIndex].Cells[4].Value = hitungSub;
                    totalHarga += Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[4].Value);
                    txtTotal.Text = totalHarga.ToString();
                }
            }
            catch
            {
                MessageBox.Show("Isi Kode Barang !!!");
            }
            #endregion
        }

        private void dgvBarangNota_KeyPress(object sender, KeyPressEventArgs e)
        {
            #region Isi Data
            try
            {
                if (e.KeyChar == (char)Keys.Back)
                {
                    //int column = dgvBarangNota.CurrentCellAddress.X;
                    int rowIndex = dgvBarangNota.CurrentCellAddress.Y;

                    if (dgvBarangNota.Rows[rowIndex].Cells[0].Value != null)
                    {
                        totalHarga -= Convert.ToInt64(dgvBarangNota.Rows[rowIndex].Cells[4].Value);
                        txtTotal.Text = totalHarga.ToString();
                        //for (int i = rowIndex; i < dgvBarangNota.RowCount; i++ )
                        //{
                        //    dgvBarangNota.Rows[i].Cells[0].Value = dgvBarangNota.Rows[i+1].Cells[0].Value;
                        //    dgvBarangNota.Rows[i].Cells[1].Value = dgvBarangNota.Rows[i + 1].Cells[1].Value;
                        //    dgvBarangNota.Rows[i].Cells[2].Value = dgvBarangNota.Rows[i + 1].Cells[2].Value;
                        //    dgvBarangNota.Rows[i].Cells[3].Value = dgvBarangNota.Rows[i + 1].Cells[3].Value;
                        //    dgvBarangNota.Rows[i].Cells[4].Value = dgvBarangNota.Rows[i + 1].Cells[4].Value;
                        //}
                            dgvBarangNota.Rows.RemoveAt(rowIndex);
                        
                    }
                    else
                    {
                        MessageBox.Show("Tidak Ada Data Yang dihapus!!!");
                    }
                }
            }
            catch{
                    MessageBox.Show(e.ToString());
            }
            #endregion
        }

        private void txtTerimaUang_TextChanged(object sender, EventArgs e)
        {
            #region Isi Data
            try
            {
                if (txtTerimaUang.Text == "")
                {
                    txtTerimaUang.Text = "0";
                    //terimaUang = Convert.ToInt64(txtTerimaUang.Text);
                    txtUangKembali.Text = "0";
                }
                else
                {
                    terimaUang = Convert.ToInt64(txtTerimaUang.Text);
                    txtUangKembali.Text = Convert.ToString(terimaUang - totalHarga);
                }

            }
            catch
            {
                MessageBox.Show("Isi Terima Uang dengan Benar !!!");
                txtUangKembali.Text ="0";
            }
            #endregion
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            #region Isi Data
            if (cboNamaPembeli.Text == "Pilih Pembeli ..." || cboCaraBayar.Text == "Pilih Cara Bayar ..." || txtTotal.Text == "" || txtTotal.Text == "9000" || txtKodeNota.Text == "")
            {
                MessageBox.Show("Data Belum Lengkap!!!");
            }
            else
            {
                if (txtTerimaUang.Text == "")
                {
                    MessageBox.Show("Masukkan Jumlah Uang yang Diterima!!!");
                }
                else
                {
                    if (Convert.ToInt64(txtTerimaUang.Text) < Convert.ToInt64(txtTotal.Text))
                    {
                        MessageBox.Show("Jumlah Uang yang Diterima Kurang!!!");
                    }
                    else
                    {
                        int barisDgv = dgvBarangNota.RowCount;
                        CekKuantitiBarang(barisDgv);                       
                        if (cekKuanti == barisDgv - 1)
                        {
                            SimpanNota();
                            SimpanBarang(barisDgv);
                            SimpanJurnal();
                            if (chk_biayakirim.Checked == true)
                            {
                                SimpanAkunJurnal1();
                                SimpanAkunJurnal2();
                                SimpanAkunJurnal3();
                                SimpanAkunJurnal4();
                                SimpanAkunJurnal5();
                                SimpanAkunJurnal6();
                                cekdefault();
                            }
                            else
                            {
                                SimpanAkunJurnal1();
                                SimpanAkunJurnal2();
                                SimpanAkunJurnal3();
                                SimpanAkunJurnal4();
                                cekdefault();
                            }
                        }

                    }
                }
            }
            tampungUbah = 0;
            #endregion 
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            #region Isi Data
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
            #endregion
        }

        #region Method
        public void CekData(string kode, int indexBaris)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id, nama, kuantiti, harga_jual FROM barang Where hapus=0 and id='" + kode + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            if (objReader.Read() == true)
            {
                i++;
                string kodeTampung = objReader.GetValue(0).ToString();
                string nama = objReader.GetValue(1).ToString();
                string kuantiti = objReader.GetValue(2).ToString();
                string harga_satuan = objReader.GetValue(3).ToString();
                dgvBarangNota.Rows[indexBaris].Cells[0].Value = kodeTampung;
                dgvBarangNota.Rows[indexBaris].Cells[1].Value = nama;
                dgvBarangNota.Rows[indexBaris].Cells[2].Value = harga_satuan;
                //dgvBarangNota.Rows.Add(kodeTampung, nama, harga_satuan);
            }
            else
            {
                MessageBox.Show("Kode Barang Tidak Ada!!!");
                dgvBarangNota.Rows.RemoveAt(indexBaris);
            }

            objReader.Close();
            con.Close();
        }

        public void DaftarNamaPembeli()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT nama FROM pembeli Where status=0";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                string nama = objReader.GetValue(0).ToString();
                cboNamaPembeli.Items.Add(nama);
            }

            objReader.Close();
            con.Close();
        }

        public void NomorNota(string bulan, String Tahun)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT Count(*) FROM nota_jual Where status=0";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                int ubahDataInt = Convert.ToInt16(objReader.GetValue(0))+1;
                string data = ubahDataInt.ToString();
                string kodeNota ="J "+ data + "/" + bulan + "/" + Tahun;
                txtKodeNota.Text = kodeNota;
                //MessageBox.Show(data+"/"+bulan+"/"+Tahun);
            }
            objReader.Close();
            con.Close();
        }

        public string UbahFormatDate(string tahun, string bulan, string tanggal)
        {
            string a = tahun + "-" + bulan + "-" + tanggal;
            return a;
        }

        public void SimpanNota()
        {
            con.Open();
            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            objKomen.CommandText = "Insert Into nota_jual Values (@kode, @totalHarga, @tipeBayar,@biaya_kirim,@nomer_kartu,@jenis_bank, @waktuJual, @id_pembeli, @id_karyawan, @status)";
            objKomen.Parameters.AddWithValue("@kode", txtKodeNota.Text);
            objKomen.Parameters.AddWithValue("@totalHarga", txtTotal.Text);
            objKomen.Parameters.AddWithValue("@tipeBayar", cboCaraBayar.Text);
            if (chk_biayakirim.Checked)
            {
                objKomen.Parameters.AddWithValue("@biaya_kirim", 9000);
            }
            else
            {
                objKomen.Parameters.AddWithValue("@biaya_kirim", 0);
            }
            if (cboCaraBayar.Text.Equals("Transfer/Debit") || cboCaraBayar.Text.Equals("Credit Card"))
            {
                objKomen.Parameters.AddWithValue("@nomer_kartu", txtNomorKartu.Text);
                objKomen.Parameters.AddWithValue("@jenis_bank", cboNamaBank.Text);

            }
            else
            {
                objKomen.Parameters.AddWithValue("@nomer_kartu", null);
                objKomen.Parameters.AddWithValue("@jenis_bank", null);
            }

            objKomen.Parameters.AddWithValue("@waktuJual", UbahFormatDate(splitTampungDate[2], splitTampungDate[0], splitTampungDate[1]));
            objKomen.Parameters.AddWithValue("@id_pembeli", 1);
            objKomen.Parameters.AddWithValue("@id_karyawan", 1);
            objKomen.Parameters.AddWithValue("@status", 0);

            int hasil =objKomen.ExecuteNonQuery();

            if (hasil > 0)
            {
                //MessageBox.Show("Penambahan data yang Anda tambahkan berhasil!");
                //con.Close();
                //NomorNota(ubahFormatBulan, splitTampungDate[2] );
                //con.Open();
                //cboNamaPembeli.Text = "Pilih Pembeli ...";
                //cboCaraBayar.Text = "Pilih Cara Bayar ...";
                //txtTotal.Clear();
                //txtUangKembali.Clear();
                //txtTerimaUang.Clear();
            }
            con.Close();
        }

        public void SimpanBarang(int baris)
        {
            try
            {
                con.Open();
                
                int hasil = 0;
                int hasilUpdate = 0;
                for (int a = 0; a < baris-1; a++)
                {
                    MySqlCommand objKomen = new MySqlCommand();
                    objKomen.Connection = con;
                    objKomen.CommandType = CommandType.Text;
                    objKomen.CommandText = "Insert Into nota_jual_has_barang Values (@kode_nota, @id_barang, @kuantiti, @sub_harga)";
                    objKomen.Parameters.AddWithValue("@kode_nota", txtKodeNota.Text);
                    objKomen.Parameters.AddWithValue("@id_barang", dgvBarangNota.Rows[a].Cells[0].Value.ToString());
                    objKomen.Parameters.AddWithValue("@kuantiti", Convert.ToInt64(dgvBarangNota.Rows[a].Cells[3].Value));
                    objKomen.Parameters.AddWithValue("@sub_harga", Convert.ToInt64(dgvBarangNota.Rows[a].Cells[4].Value));
                    hasil = objKomen.ExecuteNonQuery();

                    //Ambil Data kuantiti barang yang akan ditambahkan
                    MySqlCommand objData = new MySqlCommand();
                    objData.Connection = con;
                    objData.CommandType = CommandType.Text;
                    objData.CommandText = "SELECT kuantiti,harga_satuan FROM barang Where hapus=0 and id='" + dgvBarangNota.Rows[a].Cells[0].Value.ToString() + "'";
                    MySqlDataReader objReader;
                    objReader = objData.ExecuteReader();

                    int kuantiti = 0;
                    if (objReader.Read() == true)
                    {
                        kuantiti = Convert.ToInt16(objReader.GetValue(0).ToString());
                        totalharga_beli += Convert.ToInt64(dgvBarangNota.Rows[a].Cells[3].Value) * Convert.ToInt64(objReader.GetValue(1).ToString());
                        kuantiti -= Convert.ToInt16(dgvBarangNota.Rows[a].Cells[3].Value);

                        //Update Barang
                        MySqlCommand objKomenUpdate = new MySqlCommand();
                        objKomenUpdate.Connection = con;
                        objKomenUpdate.CommandType = CommandType.Text;

                        objKomenUpdate.CommandText = "UPDATE barang SET kuantiti=@stokUpdate WHERE id='" + dgvBarangNota.Rows[a].Cells[0].Value.ToString() + "'";
                        objKomenUpdate.Parameters.AddWithValue("@stokUpdate", kuantiti);

                        objReader.Close();

                        hasilUpdate = objKomenUpdate.ExecuteNonQuery();
                        ////MessageBox.Show("" + hasilUpdate);
                    }
                }
                
                if (hasil > 0)
                {
                    MessageBox.Show("Penambahan data yang Anda tambahkan berhasil!");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void CekKuantitiBarang(int barisDgv)
        {
            con.Open();
            cekKuanti = 0;
            for (int a = 0; a < barisDgv - 1; a++)
            {
                //Ambil Data kuantiti barang yang akan ditambahkan
                MySqlCommand objData = new MySqlCommand();
                objData.Connection = con;
                objData.CommandType = CommandType.Text;
                objData.CommandText = "SELECT kuantiti FROM barang Where hapus=0 and id='" + dgvBarangNota.Rows[a].Cells[0].Value.ToString() + "'";
                MySqlDataReader objReader;
                objReader = objData.ExecuteReader();

                int kuantiti = 0;
                if (objReader.Read() == true)
                {
                    kuantiti = Convert.ToInt16(objReader.GetValue(0).ToString());
                    if (kuantiti >= Convert.ToInt16(dgvBarangNota.Rows[a].Cells[3].Value))
                    {
                        cekKuanti += 1;
                    }
                    else
                    {
                        MessageBox.Show("Stok " + dgvBarangNota.Rows[a].Cells[1].Value + " Di Dalam Toko: " + kuantiti + "\nStok Yang Ingin Dibeli :" + dgvBarangNota.Rows[a].Cells[3].Value + "\nStok Kurang " + (Convert.ToInt16(dgvBarangNota.Rows[a].Cells[3].Value) - kuantiti) + "!!!");
                        tampungUbah = 0;
                        totalHarga = 0;
                        terimaUang = 0;
                        txtTotal.Text =  totalHarga.ToString();
                        txtTerimaUang.Text = terimaUang.ToString();
                        txtUangKembali.Text = Convert.ToString(terimaUang - totalHarga);
                    }
                }
                objReader.Close();
            }
            con.Close();
        }

        public void SimpanJurnal()
        {
            String a = "";
            if(chk_biayakirim.Checked==true)
            {
                if (cboCaraBayar.Text.Equals("Tunai"))
                {
                    a = " barang - barang diambil oleh customer - cara bayar tunai ";
                }
                else if (cboCaraBayar.Text.Equals("Transfer/Debit"))
                {
                    if (cboNamaBank.Text.Equals("BCA"))
                    {
                        a = "Penjualan barang -barang diambil oleh customer - cara bayar transfer/debit bca";
                    }
                    else
                    {
                        a = "Penjualan barang -barang diambil oleh customer - cara bayar transfer/debit mandiri";
                    }

                }
                else if (cboCaraBayar.Text.Equals("Credit Card"))
                {
                    a = "Penjualan barang -barang diambil oleh customer - cara bayar credit card ";
                }
            }
            else
            {
                if (cboCaraBayar.Text.Equals("Tunai"))
                {
                    a = "Penjualan barang - Shipping point - cara bayar tunai ";
                }
                else if (cboCaraBayar.Text.Equals("Transfer/Debit"))
                {
                    if (cboNamaBank.Text.Equals("BCA"))
                    {
                        a = "Penjualan barang - Shipping point - cara bayar transfer / debit bca";
                    }
                    else
                    {
                        a = "Penjualan barang - Shipping point - cara bayar transfer / debit mandiri";
                    }

                }
                else if (cboCaraBayar.Text.Equals("Credit Card"))
                {
                    a = "Penjualan barang - Shipping point - cara bayar credit ";
                }

            }
            String kt =  a+ "dengan jumlah biaya " + txtTotal.Text;
            try
            {
                con.Open();
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into jurnal Values (@id, @jenis, @no_bukti, @keterangan, @tgl_penyusutan)";
                objKomen.Parameters.AddWithValue("@id", "");
                objKomen.Parameters.AddWithValue("@jenis", "umum");
                objKomen.Parameters.AddWithValue("@no_bukti", txtKodeNota.Text);
                objKomen.Parameters.AddWithValue("@keterangan", kt);
                objKomen.Parameters.AddWithValue("@tgl_penyusutan", UbahFormatDate(splitTampungDate[2], splitTampungDate[0], splitTampungDate[1]));
                int hasil = objKomen.ExecuteNonQuery();
                if (hasil > 0)
                {
                    MessageBox.Show("jurnal masuk");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public int AmbilIdJurnal()
        {
            int id = 0;
            con.Open();
            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id_jurnal FROM jurnal WHERE no_bukti='" + txtKodeNota.Text + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                id = Convert.ToInt16(objReader.GetValue(0));
            }
            objReader.Close();
            con.Close();
            return id;
        }
        public void SimpanAkunJurnal1()
        {
            int id =AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun1, @id_jurnal1, @urutan1, @debet1, @kredit1)";
                if(cboCaraBayar.Text.Equals("Tunai"))
                {
                    objKomen.Parameters.AddWithValue("@no_akun1", 10010);
                }
                else if (cboCaraBayar.Text.Equals("Transfer/Debit"))
                {
                    if(cboNamaBank.Text.Equals("BCA"))
                    {
                        objKomen.Parameters.AddWithValue("@no_akun1", 10020);
                    }
                    else
                    {
                        objKomen.Parameters.AddWithValue("@no_akun1", 10030);
                    }
                    
                }
                else if (cboCaraBayar.Text.Equals("Credit Card"))
                {
                    objKomen.Parameters.AddWithValue("@no_akun1", 10050);
                }

                objKomen.Parameters.AddWithValue("@id_jurnal1", id);
                objKomen.Parameters.AddWithValue("@urutan1", 1);
                if(chk_biayakirim.Checked == true)
                {
                    int to = Convert.ToInt16(txtTotal.Text) - 9000;
                    objKomen.Parameters.AddWithValue("@debet1", to);
                }
                else
                {
                    objKomen.Parameters.AddWithValue("@debet1", Convert.ToInt16(txtTotal.Text));
                }
                
                objKomen.Parameters.AddWithValue("@kredit1", 0);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                   
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void SimpanAkunJurnal2()
        {
            int id = AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun2, @id_jurnal2, @urutan2, @debet2, @kredit2)";
                objKomen.Parameters.AddWithValue("@no_akun2", 40010);
                objKomen.Parameters.AddWithValue("@id_jurnal2", id);
                objKomen.Parameters.AddWithValue("@urutan2", 2);
                objKomen.Parameters.AddWithValue("@debet2", 0);
                if(chk_biayakirim.Checked == true)
                {
                    int to = Convert.ToInt16(txtTotal.Text) - 9000;
                    objKomen.Parameters.AddWithValue("@kredit2", to );
                }
                else
                {
                    objKomen.Parameters.AddWithValue("@kredit2", Convert.ToInt16(txtTotal.Text));
                }
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {

                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void SimpanAkunJurnal3()
        {
            int id = AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun3, @id_jurnal3, @urutan3, @debet3, @kredit3)";
                objKomen.Parameters.AddWithValue("@no_akun3", 50010);
                objKomen.Parameters.AddWithValue("@id_jurnal3", id);
                objKomen.Parameters.AddWithValue("@urutan3", 3);
                objKomen.Parameters.AddWithValue("@debet3", totalharga_beli);
                objKomen.Parameters.AddWithValue("@kredit3", 0);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {

                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void SimpanAkunJurnal4()
        {
            int id = AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun4, @id_jurnal4, @urutan4, @debet4, @kredit4)";
                objKomen.Parameters.AddWithValue("@no_akun4", 10060);
                objKomen.Parameters.AddWithValue("@id_jurnal4", id);
                objKomen.Parameters.AddWithValue("@urutan4", 4);
                objKomen.Parameters.AddWithValue("@debet4", 0);
                objKomen.Parameters.AddWithValue("@kredit4", totalharga_beli);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {

                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void SimpanAkunJurnal5()
        {
            int id = AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun5, @id_jurnal5, @urutan5, @debet5, @kredit5)";
                objKomen.Parameters.AddWithValue("@no_akun5", 50030);
                objKomen.Parameters.AddWithValue("@id_jurnal5", id);
                objKomen.Parameters.AddWithValue("@urutan5", 5);
                objKomen.Parameters.AddWithValue("@debet5", 9000);
                objKomen.Parameters.AddWithValue("@kredit5", 0);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {

                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void SimpanAkunJurnal6()
        {
            int id = AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun6, @id_jurnal6, @urutan6, @debet6, @kredit6)";
                objKomen.Parameters.AddWithValue("@no_akun6", 10010);
                objKomen.Parameters.AddWithValue("@id_jurnal6", id);
                objKomen.Parameters.AddWithValue("@urutan6", 6);
                objKomen.Parameters.AddWithValue("@debet6", 0);
                objKomen.Parameters.AddWithValue("@kredit6", 9000);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {

                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }


        #endregion


        private void cboCaraBayar_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCaraBayar.Text == "Tunai")
            {
                cboNamaBank.Enabled = false;
                txtNomorKartu.Enabled = false;
                
                
            }
            else if (cboCaraBayar.Text == "Transfer/Debit")
            {
                cboNamaBank.Enabled = true;
                txtNomorKartu.Enabled = true;
                
            }
            else if (cboCaraBayar.Text == "Credit Card")
            {
                cboNamaBank.Enabled = true;
                txtNomorKartu.Enabled = true;
                
            }
        }
        public void cekdefault()
        {
            NomorNota(ubahFormatBulan, splitTampungDate[2]);
            cboNamaPembeli.Text = "Pilih Pembeli ...";
            cboCaraBayar.Text = "Pilih Cara Bayar ...";
            txtTotal.Clear();
            totalHarga = 0;
            totalharga_beli = 0;
            txtUangKembali.Clear();
            txtTerimaUang.Clear();
            dgvBarangNota.Rows.Clear();
            txtNomorKartu.Clear();
            cboNamaBank.Text = "";
        }

        private void chk_biayakirim_CheckedChanged(object sender, EventArgs e)
        {
            if(chk_biayakirim.Checked == true)
            {
                int tampungtotl = 9000 +(int) totalHarga;
                totalHarga = (long)tampungtotl;
                txtTotal.Text = tampungtotl.ToString();
            }
            else
            {
                int tampungtotl = (int)totalHarga - 9000;
                totalHarga = (long)tampungtotl;
                txtTotal.Text = tampungtotl.ToString();
            }
        }
    }
}
