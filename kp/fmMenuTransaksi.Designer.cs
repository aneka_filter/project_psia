﻿namespace kp
{
    partial class fmMenuTransaksi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menuNota = new System.Windows.Forms.ToolStripMenuItem();
            this.filterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBarang = new System.Windows.Forms.ToolStripMenuItem();
            this.dataBarangToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.filterToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.sparePartToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stokBarangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perPartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.globalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mutasiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formMutasiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mutasiInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mutasiOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMutasiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mutasiInToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mutasiOutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ubahMutasiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mutasiInToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.mutasiOutToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNota,
            this.menuBarang,
            this.mutasiToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(475, 27);
            this.menu.TabIndex = 3;
            this.menu.Text = "menuStrip1";
            // 
            // menuNota
            // 
            this.menuNota.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filterToolStripMenuItem,
            this.spareToolStripMenuItem});
            this.menuNota.Name = "menuNota";
            this.menuNota.Size = new System.Drawing.Size(52, 23);
            this.menuNota.Text = "Nota";
            // 
            // filterToolStripMenuItem
            // 
            this.filterToolStripMenuItem.Name = "filterToolStripMenuItem";
            this.filterToolStripMenuItem.Size = new System.Drawing.Size(136, 24);
            this.filterToolStripMenuItem.Text = "Nota Jual";
            this.filterToolStripMenuItem.Click += new System.EventHandler(this.filterToolStripMenuItem_Click);
            // 
            // spareToolStripMenuItem
            // 
            this.spareToolStripMenuItem.Name = "spareToolStripMenuItem";
            this.spareToolStripMenuItem.Size = new System.Drawing.Size(136, 24);
            this.spareToolStripMenuItem.Text = "Nota Beli";
            this.spareToolStripMenuItem.Click += new System.EventHandler(this.spareToolStripMenuItem_Click);
            // 
            // menuBarang
            // 
            this.menuBarang.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataBarangToolStripMenuItem2,
            this.stokBarangToolStripMenuItem});
            this.menuBarang.Name = "menuBarang";
            this.menuBarang.Size = new System.Drawing.Size(64, 23);
            this.menuBarang.Text = "Barang";
            // 
            // dataBarangToolStripMenuItem2
            // 
            this.dataBarangToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filterToolStripMenuItem2,
            this.sparePartToolStripMenuItem1});
            this.dataBarangToolStripMenuItem2.Name = "dataBarangToolStripMenuItem2";
            this.dataBarangToolStripMenuItem2.Size = new System.Drawing.Size(154, 24);
            this.dataBarangToolStripMenuItem2.Text = "Data Barang";
            this.dataBarangToolStripMenuItem2.Click += new System.EventHandler(this.dataBarangToolStripMenuItem2_Click);
            // 
            // filterToolStripMenuItem2
            // 
            this.filterToolStripMenuItem2.Name = "filterToolStripMenuItem2";
            this.filterToolStripMenuItem2.Size = new System.Drawing.Size(152, 24);
            this.filterToolStripMenuItem2.Text = "Filter";
            // 
            // sparePartToolStripMenuItem1
            // 
            this.sparePartToolStripMenuItem1.Name = "sparePartToolStripMenuItem1";
            this.sparePartToolStripMenuItem1.Size = new System.Drawing.Size(152, 24);
            this.sparePartToolStripMenuItem1.Text = "Spare Part";
            // 
            // stokBarangToolStripMenuItem
            // 
            this.stokBarangToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.perPartToolStripMenuItem,
            this.globalToolStripMenuItem});
            this.stokBarangToolStripMenuItem.Name = "stokBarangToolStripMenuItem";
            this.stokBarangToolStripMenuItem.Size = new System.Drawing.Size(154, 24);
            this.stokBarangToolStripMenuItem.Text = "Stok Barang";
            // 
            // perPartToolStripMenuItem
            // 
            this.perPartToolStripMenuItem.Name = "perPartToolStripMenuItem";
            this.perPartToolStripMenuItem.Size = new System.Drawing.Size(130, 24);
            this.perPartToolStripMenuItem.Text = "Per-Part";
            // 
            // globalToolStripMenuItem
            // 
            this.globalToolStripMenuItem.Name = "globalToolStripMenuItem";
            this.globalToolStripMenuItem.Size = new System.Drawing.Size(130, 24);
            this.globalToolStripMenuItem.Text = "Global";
            // 
            // mutasiToolStripMenuItem
            // 
            this.mutasiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formMutasiToolStripMenuItem,
            this.viewMutasiToolStripMenuItem,
            this.ubahMutasiToolStripMenuItem});
            this.mutasiToolStripMenuItem.Name = "mutasiToolStripMenuItem";
            this.mutasiToolStripMenuItem.Size = new System.Drawing.Size(62, 23);
            this.mutasiToolStripMenuItem.Text = "Mutasi";
            // 
            // formMutasiToolStripMenuItem
            // 
            this.formMutasiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mutasiInToolStripMenuItem,
            this.mutasiOutToolStripMenuItem});
            this.formMutasiToolStripMenuItem.Name = "formMutasiToolStripMenuItem";
            this.formMutasiToolStripMenuItem.Size = new System.Drawing.Size(156, 24);
            this.formMutasiToolStripMenuItem.Text = "Form Mutasi";
            // 
            // mutasiInToolStripMenuItem
            // 
            this.mutasiInToolStripMenuItem.Name = "mutasiInToolStripMenuItem";
            this.mutasiInToolStripMenuItem.Size = new System.Drawing.Size(146, 24);
            this.mutasiInToolStripMenuItem.Text = "Mutasi In";
            // 
            // mutasiOutToolStripMenuItem
            // 
            this.mutasiOutToolStripMenuItem.Name = "mutasiOutToolStripMenuItem";
            this.mutasiOutToolStripMenuItem.Size = new System.Drawing.Size(146, 24);
            this.mutasiOutToolStripMenuItem.Text = "Mutasi Out";
            // 
            // viewMutasiToolStripMenuItem
            // 
            this.viewMutasiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mutasiInToolStripMenuItem1,
            this.mutasiOutToolStripMenuItem1});
            this.viewMutasiToolStripMenuItem.Name = "viewMutasiToolStripMenuItem";
            this.viewMutasiToolStripMenuItem.Size = new System.Drawing.Size(156, 24);
            this.viewMutasiToolStripMenuItem.Text = "Data Mutasi ";
            // 
            // mutasiInToolStripMenuItem1
            // 
            this.mutasiInToolStripMenuItem1.Name = "mutasiInToolStripMenuItem1";
            this.mutasiInToolStripMenuItem1.Size = new System.Drawing.Size(146, 24);
            this.mutasiInToolStripMenuItem1.Text = "Mutasi In";
            // 
            // mutasiOutToolStripMenuItem1
            // 
            this.mutasiOutToolStripMenuItem1.Name = "mutasiOutToolStripMenuItem1";
            this.mutasiOutToolStripMenuItem1.Size = new System.Drawing.Size(146, 24);
            this.mutasiOutToolStripMenuItem1.Text = "Mutasi Out";
            // 
            // ubahMutasiToolStripMenuItem
            // 
            this.ubahMutasiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mutasiInToolStripMenuItem2,
            this.mutasiOutToolStripMenuItem2});
            this.ubahMutasiToolStripMenuItem.Name = "ubahMutasiToolStripMenuItem";
            this.ubahMutasiToolStripMenuItem.Size = new System.Drawing.Size(156, 24);
            this.ubahMutasiToolStripMenuItem.Text = "Ubah Mutasi";
            // 
            // mutasiInToolStripMenuItem2
            // 
            this.mutasiInToolStripMenuItem2.Name = "mutasiInToolStripMenuItem2";
            this.mutasiInToolStripMenuItem2.Size = new System.Drawing.Size(146, 24);
            this.mutasiInToolStripMenuItem2.Text = "Mutasi In";
            // 
            // mutasiOutToolStripMenuItem2
            // 
            this.mutasiOutToolStripMenuItem2.Name = "mutasiOutToolStripMenuItem2";
            this.mutasiOutToolStripMenuItem2.Size = new System.Drawing.Size(146, 24);
            this.mutasiOutToolStripMenuItem2.Text = "Mutasi Out";
            // 
            // fmMenuTransaksi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 308);
            this.Controls.Add(this.menu);
            this.IsMdiContainer = true;
            this.Name = "fmMenuTransaksi";
            this.Text = "fmMenuTransaksi";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.fmMenuTransaksi_Load);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem menuNota;
        private System.Windows.Forms.ToolStripMenuItem filterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuBarang;
        private System.Windows.Forms.ToolStripMenuItem dataBarangToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem filterToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem sparePartToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem stokBarangToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perPartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem globalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mutasiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formMutasiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mutasiInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mutasiOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewMutasiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mutasiInToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mutasiOutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ubahMutasiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mutasiInToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mutasiOutToolStripMenuItem2;

    }
}