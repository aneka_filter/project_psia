﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmNotaBiaya : Form
    {
        MySqlConnection con = new MySqlConnection();
        int i = 0;
        string ubahFormatBulan = "";
        string[] splitTampungDate;
        long totalHarga = 0;
        long tampungUbah = 0;
        int totalBaris = 0;
        public fmNotaBiaya()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";

            #region Cara Bayar Default
            cboCaraBayar.Text = "Pilih Cara Bayar ...";
            cboCaraBayar.Items.Add("Tunai");
            cboCaraBayar.Items.Add("Transfer/Debit");
            #endregion
            #region Jenis Biaya Default
            cboJenisBiaya.Text = "Pilih Jenis Biaya ...";
            cboJenisBiaya.Items.Add("Biaya Gaji");
            cboJenisBiaya.Items.Add("Biaya Transportasi");
            cboJenisBiaya.Items.Add("Biaya Sewa");
            cboJenisBiaya.Items.Add("Biaya Bunga");
            cboJenisBiaya.Items.Add("Biaya Kartu Kredit");
            cboJenisBiaya.Items.Add("Biaya Administrasi Lain-Lain");
            #endregion
            #region Cara Bayar Default
            cboNamaBank.Text = "Pilih Nama Bank ...";
            cboNamaBank.Items.Add("BCA");
            cboNamaBank.Items.Add("Mandiri");
            #endregion
            
        }

        private void fmNotaBiaya_Load(object sender, EventArgs e)
        {
            string tampungDate = DateTime.Now.ToShortDateString();
            splitTampungDate = tampungDate.Split('/');
            if (splitTampungDate[0] == "1")
                ubahFormatBulan = "I";
            else if (splitTampungDate[0] == "2")
                ubahFormatBulan = "II";
            else if (splitTampungDate[0] == "3")
                ubahFormatBulan = "III";
            else if (splitTampungDate[0] == "4")
                ubahFormatBulan = "IV";
            else if (splitTampungDate[0] == "5")
                ubahFormatBulan = "V";
            else if (splitTampungDate[0] == "6")
                ubahFormatBulan = "VI";
            else if (splitTampungDate[0] == "7")
                ubahFormatBulan = "VII";
            else if (splitTampungDate[0] == "8")
                ubahFormatBulan = "VIII";
            else if (splitTampungDate[0] == "9")
                ubahFormatBulan = "IX";
            else if (splitTampungDate[0] == "10")
                ubahFormatBulan = "X";
            else if (splitTampungDate[0] == "11")
                ubahFormatBulan = "XI";
            else if (splitTampungDate[0] == "12")
                ubahFormatBulan = "XII";

            NomorNota(ubahFormatBulan, splitTampungDate[2]);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            #region Isi Data
            if (cboCaraBayar.Text == "Pilih Cara Bayar ..." || txtTotal.Text == "" || txtTotal.Text == "0" || txtNoNota.Text == "" || cboJenisBiaya.Text == "Pilih Jenis Biaya ...")
            {
                MessageBox.Show("Data Belum Lengkap!!!");
            }
            else
            {
                SimpanNota();
                simpanJurnal();
                SimpanAkunJurnal1();
                SimpanAkunJurnal2();
            }
            tampungUbah = 0;
            #endregion
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }

        public void NomorNota(string bulan, String Tahun)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT Count(*) FROM nota_biaya";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                int ubahDataInt = Convert.ToInt16(objReader.GetValue(0)) + 1;
                string data = ubahDataInt.ToString();
                string kodeNota = "A " + data + "/" + bulan + "/" + Tahun;
                txtNoNota.Text = kodeNota;
                //MessageBox.Show(data+"/"+bulan+"/"+Tahun);
            }
            objReader.Close();
            con.Close();
        }
        public string UbahFormatDate(string tahun, string bulan, string tanggal)
        {
            string a = tahun + "-" + bulan + "-" + tanggal;
            return a;
        }

        public void SimpanNota()
        {
            try
            {
                con.Open();
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into nota_biaya Values (@kode, @jenis_Bayar, @total_biaya, @waktuJual, @cara_bayar, @no_kartu, @nama_bank)";
                objKomen.Parameters.AddWithValue("@kode", txtNoNota.Text);
                objKomen.Parameters.AddWithValue("@jenis_Bayar", cboJenisBiaya.Text);
                objKomen.Parameters.AddWithValue("@total_biaya", int.Parse(txtTotal.Text));
                objKomen.Parameters.AddWithValue("@waktujual", UbahFormatDate(splitTampungDate[2], splitTampungDate[0], splitTampungDate[1]));
                objKomen.Parameters.AddWithValue("@cara_Bayar", cboCaraBayar.Text);
                objKomen.Parameters.AddWithValue("@no_kartu", txtNoKartu);
                objKomen.Parameters.AddWithValue("@nama_bank", cboNamaBank);
                int hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("Penambahan data yang Anda tambahkan berhasil!");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

        }

        private void cboCaraBayar_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cboCaraBayar.SelectedItem =="Tunai" && cboCaraBayar.SelectedItem == "Pilih Cara Bayar ...")
            {
                txtNoKartu.Enabled = false;
                cboNamaBank.Enabled = false;
            }
            else if(cboCaraBayar.SelectedItem == "Transfer/Debit")
            {
                txtNoKartu.Enabled = true;
                cboNamaBank.Enabled = true;
            }
        }

        public void simpanJurnal()
        {
            string keterangan = "";
            if(cboCaraBayar.Text == "Tunai")
            {
                keterangan = "Umum - " + cboJenisBiaya.Text + " secara Tunai dengan jumlah biaya " + txtTotal.Text;
            }
            else if(cboCaraBayar.Text =="Transfer/Debit")
            {
                keterangan = "Umum - " + cboJenisBiaya.Text + " secara Transfer / Debit Menggunakan Bank "+cboNamaBank.Text+" dengan jumlah biaya " + txtTotal.Text;
            }
            //abc
            try
            {
                con.Open();
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into jurnal Values (@kode, @jenis, @no_bukti, @keterangan, @tgl)";
                objKomen.Parameters.AddWithValue("@kode", "");
                objKomen.Parameters.AddWithValue("@jenis", "umum");
                objKomen.Parameters.AddWithValue("@no_bukti", txtNoNota.Text);
                objKomen.Parameters.AddWithValue("@keterangan", keterangan);
                objKomen.Parameters.AddWithValue("@tgl", UbahFormatDate(splitTampungDate[2], splitTampungDate[0], splitTampungDate[1]));
                int hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("jurnal masuk");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        int id = 0;
        public void AmbilIdJurnal()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id_jurnal FROM jurnal WHERE no_bukti='" + txtNoNota.Text + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                id = Convert.ToInt16(objReader.GetValue(0));
            }
            objReader.Close();
            con.Close();
        }
        public void SimpanAkunJurnal1()
        {
            AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun2, @id_jurnal2, @urutan2, @debet2, @kredit2)";
                if (cboJenisBiaya.Text =="Biaya Gaji")
                {
                    objKomen.Parameters.AddWithValue("@no_akun2", 50020);
                }
                else if (cboJenisBiaya.Text == "Biaya Transportasi")
                {
                    objKomen.Parameters.AddWithValue("@no_akun2", 50030);
                }
                else if (cboJenisBiaya.Text == "Biaya Sewa")
                {
                    objKomen.Parameters.AddWithValue("@no_akun2", 50050);
                }
                else if (cboJenisBiaya.Text == "Biaya Bunga")
                {
                    objKomen.Parameters.AddWithValue("@no_akun2", 50060);
                }
                else if (cboJenisBiaya.Text == "Biaya Kartu Kredit")
                {
                    objKomen.Parameters.AddWithValue("@no_akun2", 50040);
                }
                else if (cboJenisBiaya.Text == "Biaya Administrasi Lain-Lain")
                {
                    objKomen.Parameters.AddWithValue("@no_akun2", 50100);
                }

                objKomen.Parameters.AddWithValue("@id_jurnal2", id);
                objKomen.Parameters.AddWithValue("@urutan2", 1);
                objKomen.Parameters.AddWithValue("@debet2", int.Parse(txtTotal.Text));
                objKomen.Parameters.AddWithValue("@kredit2", 0);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("Akun Jurnal Masuk");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void SimpanAkunJurnal2()
        {
            AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun, @id_jurnal, @urutan, @debet, @kredit)";
                if(cboCaraBayar.Text == "Tunai")
                {
                    objKomen.Parameters.AddWithValue("@no_akun", 10010);
                }
                else if(cboCaraBayar.Text =="Transfer/Debit")
                {
                    if(cboNamaBank.Text == "BCA")
                    {
                        objKomen.Parameters.AddWithValue("@no_akun", 10020);
                    }
                    else if(cboNamaBank.Text == "Mandiri")
                    {
                        objKomen.Parameters.AddWithValue("@no_akun", 10030);
                    }
                    
                }

                objKomen.Parameters.AddWithValue("@id_jurnal", id);
                objKomen.Parameters.AddWithValue("@urutan", 2);
                objKomen.Parameters.AddWithValue("@debet", 0);
                objKomen.Parameters.AddWithValue("@kredit", int.Parse(txtTotal.Text));
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("Akun Jurnal Masuk");
                    con.Close();
                    NomorNota(ubahFormatBulan, splitTampungDate[2]);
                    con.Open();
                    cboCaraBayar.Text = "Pilih Cara Bayar ...";
                    cboJenisBiaya.Text = "Pilih Jenis Biaya ...";
                    cboNamaBank.Text = "Pilih Nama Bank ...";
                    txtNoKartu.Clear();
                    txtTotal.Clear();
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }    
    }
}
