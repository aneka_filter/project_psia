﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kp
{
    public partial class fmMenuTransaksi : Form
    {
        public fmMenuTransaksi()
        {
            InitializeComponent();
        }

        private void fmMenuTransaksi_Load(object sender, EventArgs e)
        {

        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }

        private void filterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmNotaJual();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void spareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmNotaBeli();
            fm1.MdiParent = this;
            fm1.Show();
        }

        private void dataBarangToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }
    }
}
