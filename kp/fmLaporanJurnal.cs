﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmLaporanJurnal : Form
    {
        MySqlConnection con = new MySqlConnection();
        public fmLaporanJurnal()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";
        }

        private void fmLaporanJurnal_Load(object sender, EventArgs e)
        {
            TampilkanData();
        }

        public void TampilkanData()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT j.tanggal, j.keterangan, j.no_bukti , aj.debet, aj.kredit, a.nama_akun "+
                "FROM jurnal j INNER JOIN akun_has_jurnal aj ON j.id_jurnal=aj.id_jurnal INNER JOIN akun a ON aj.no_akun = a.no_akun "+
                "ORDER by aj.id_jurnal , aj.urutan";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            string tanggal = "";
            string ket = "";
            string no_bukti = "";
            string debet = "";
            string kredit = "";
            string nama_akun = "";

            dgvData.Rows.Clear();
            while (objReader.Read() == true)
            {
                tanggal = objReader.GetValue(0).ToString();
                ket = objReader.GetValue(1).ToString();
                no_bukti = objReader.GetValue(2).ToString();
                debet = objReader.GetValue(3).ToString();
                kredit = objReader.GetValue(4).ToString();
                nama_akun = objReader.GetValue(5).ToString();
                dgvData.Rows.Add(tanggal, ket, nama_akun, debet, kredit, no_bukti);
            }

            objReader.Close();
            con.Close();
        }
    }
}

