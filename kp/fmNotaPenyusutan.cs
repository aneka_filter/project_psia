﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmNotaPenyusutan : Form
    {
        MySqlConnection con = new MySqlConnection();
        int i = 0;
        string ubahFormatBulan = "";
        string[] tampungNamaAset;
        string[] tampungDateAset;
        string[] splitTampungDate;
        long totalHarga = 0;
        long tampungUbah = 0;
        int totalBaris = 0;
        int id = 0;
        public fmNotaPenyusutan()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";
        }

        private void fmNotaPenyusutan_Load(object sender, EventArgs e)
        {
            string tampungDate = DateTime.Now.ToShortDateString();
            splitTampungDate = tampungDate.Split('/');
            if (splitTampungDate[0] == "1")
                ubahFormatBulan = "I";
            else if (splitTampungDate[0] == "2")
                ubahFormatBulan = "II";
            else if (splitTampungDate[0] == "3")
                ubahFormatBulan = "III";
            else if (splitTampungDate[0] == "4")
                ubahFormatBulan = "IV";
            else if (splitTampungDate[0] == "5")
                ubahFormatBulan = "V";
            else if (splitTampungDate[0] == "6")
                ubahFormatBulan = "VI";
            else if (splitTampungDate[0] == "7")
                ubahFormatBulan = "VII";
            else if (splitTampungDate[0] == "8")
                ubahFormatBulan = "VIII";
            else if (splitTampungDate[0] == "9")
                ubahFormatBulan = "IX";
            else if (splitTampungDate[0] == "10")
                ubahFormatBulan = "X";
            else if (splitTampungDate[0] == "11")
                ubahFormatBulan = "XI";
            else if (splitTampungDate[0] == "12")
                ubahFormatBulan = "XII";

            NomorNota(ubahFormatBulan, splitTampungDate[2]);

            AmbilDataAset(banyakdata());

        }
        public void NomorNota(string bulan, String Tahun)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT Count(*) FROM nota_penyusutan";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                int ubahDataInt = int.Parse(objReader.GetValue(0).ToString()) + 1;
                string data = ubahDataInt.ToString();
                string kodeNota = "P " + data + "/" + bulan + "/" + Tahun;
                txtNoNota.Text = kodeNota;
                //MessageBox.Show(data+"/"+bulan+"/"+Tahun);
            }
            objReader.Close();
            con.Close();
        }

        public int banyakdata()
        {
            int ubahDataInt = 0;
            con.Open();
            
            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT Count(*) FROM nota_penyusutan";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                 ubahDataInt = int.Parse(objReader.GetValue(0).ToString());
            }
            objReader.Close();
            con.Close();
            return ubahDataInt;
        }

        public void AmbilDataAset(int k)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT nama_aset, waktu_bayar FROM nota_bayar";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            tampungNamaAset = new string[k];
            tampungDateAset = new string[k];

            int index = 0;
            while (objReader.Read() == true)
            {
                
                tampungNamaAset[index] = objReader.GetValue(0).ToString();
                tampungDateAset[index] = objReader.GetValue(1).ToString();
                index++;
            }
            for (int i = 0; i < index; i++ )
            {
                cboNamaAset.Items.Add(tampungNamaAset[i]);
            }
            objReader.Close();
            con.Close();
        }

        private void txtTanggalPembelian_TextChanged(object sender, EventArgs e)
        {

        }

        private void cboNamaAset_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime a = DateTime.Parse(tampungDateAset[cboNamaAset.SelectedIndex]);
            string b = a.ToShortDateString();
            txtTanggalPembelian.Text = b;
        }
        string tampungDtp = "";
        string [] splitDtp;
        private void btnPrint_Click(object sender, EventArgs e)
        {
            tampungDtp = dtpDate.Value.ToShortDateString();
            splitDtp = tampungDtp.Split('/');

            #region Isi Data
            if (cboNamaAset.Text == "" || txtTotal.Text == "" || txtTotal.Text == "0" || txtNoNota.Text == "" || txtTanggalPembelian.Text == "")
            {
                MessageBox.Show("Data Belum Lengkap!!!");
            }
            else
            {
                SimpanNota();
                simpanJurnal();
                SimpanAkunJurnal1();
                SimpanAkunJurnal2();
            }
            tampungUbah = 0;
            #endregion
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }
        public string UbahFormatDate(string tahun, string bulan, string tanggal)
        {
            string a = tahun + "-" + bulan + "-" + tanggal;
            return a;
        }

        public void SimpanNota()
        {
            try
            {
                con.Open();
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into nota_penyusutan Values (@kode, @nama_aset, @tgl_penyusutan, @total_biaya)";
                objKomen.Parameters.AddWithValue("@kode", txtNoNota.Text);
                objKomen.Parameters.AddWithValue("@nama_aset", cboNamaAset.Text);
                objKomen.Parameters.AddWithValue("@tgl_penyusutan", UbahFormatDate(splitDtp[2], splitDtp[0], splitDtp[1]));
                objKomen.Parameters.AddWithValue("@total_biaya", Convert.ToInt16(txtTotal.Text));
                int hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("Penambahan data yang Anda tambahkan berhasil!");                    
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void simpanJurnal()
        {
            string keterangan = "Penyusutan "+cboNamaAset.Text+" dengan jumlah biaya "+txtTotal.Text;
            try
            {
                con.Open();
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into jurnal Values (@kode, @jenis, @no_bukti, @keterangan, @tgl_penyusutan)";
                objKomen.Parameters.AddWithValue("@kode", "");
                objKomen.Parameters.AddWithValue("@jenis", "penyesuaian");
                objKomen.Parameters.AddWithValue("@no_bukti", txtNoNota.Text);
                objKomen.Parameters.AddWithValue("@keterangan", keterangan);
                objKomen.Parameters.AddWithValue("@tgl_penyusutan", UbahFormatDate(splitDtp[2], splitDtp[0], splitDtp[1]));
                int hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("jurnal masuk");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void AmbilIdJurnal()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id_jurnal FROM jurnal WHERE no_bukti='"+txtNoNota.Text+"'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                id = Convert.ToInt16(objReader.GetValue(0));
            }
            objReader.Close();
            con.Close();
        }
        string nama = "";
        public void AmbilJenisAset()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT jenis_aset FROM nota_bayar WHERE nama_aset='" + cboNamaAset.Text + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                nama = objReader.GetValue(0).ToString();
            }
            objReader.Close();
            con.Close();
        }
        public void SimpanAkunJurnal1()
        {
            AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun, @id_jurnal, @urutan, @debet, @kredit)";
                objKomen.Parameters.AddWithValue("@no_akun", 50090);
                objKomen.Parameters.AddWithValue("@id_jurnal", id);
                objKomen.Parameters.AddWithValue("@urutan", 1);
                objKomen.Parameters.AddWithValue("@debet", int.Parse(txtTotal.Text));
                objKomen.Parameters.AddWithValue("@kredit", 0);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("Akun Jurnal Masuk");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void SimpanAkunJurnal2()
        {
            AmbilIdJurnal();
            AmbilJenisAset();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun2, @id_jurnal2, @urutan2, @debet2, @kredit2)";
                if (nama == "Kendaraan")
                {
                    objKomen.Parameters.AddWithValue("@no_akun2", 10120);
                }
                else if (nama == "Toko")
                {
                    objKomen.Parameters.AddWithValue("@no_akun2", 10100);
                }

                objKomen.Parameters.AddWithValue("@id_jurnal2", id);
                objKomen.Parameters.AddWithValue("@urutan2", 2);
                objKomen.Parameters.AddWithValue("@debet2", 0);
                objKomen.Parameters.AddWithValue("@kredit2", double.Parse(txtTotal.Text));
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("Akun Jurnal Masuk");
                    con.Close();
                    NomorNota(ubahFormatBulan, splitTampungDate[2]);
                    con.Open();
                    cboNamaAset.Text = "";
                    txtTanggalPembelian.Clear();
                    txtTotal.Clear();
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }    
    }
}
