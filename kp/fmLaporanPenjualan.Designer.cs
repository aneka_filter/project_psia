﻿namespace kp
{
    partial class fmLaporanPenjualan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cboPeriode = new System.Windows.Forms.ComboBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtKodeNota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNamaPembeli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtBayarTunai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtBayarKredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(42, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 20);
            this.label8.TabIndex = 62;
            this.label8.Text = "Tanggal :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(42, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 20);
            this.label7.TabIndex = 60;
            this.label7.Text = "Periode :";
            // 
            // cboPeriode
            // 
            this.cboPeriode.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPeriode.FormattingEnabled = true;
            this.cboPeriode.Location = new System.Drawing.Point(117, 75);
            this.cboPeriode.Name = "cboPeriode";
            this.cboPeriode.Size = new System.Drawing.Size(121, 28);
            this.cboPeriode.TabIndex = 1;
            this.cboPeriode.SelectedIndexChanged += new System.EventHandler(this.cboPeriode_SelectedIndexChanged);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(517, 116);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(77, 32);
            this.btnPrint.TabIndex = 3;
            this.btnPrint.Text = "Cetak";
            this.btnPrint.UseVisualStyleBackColor = true;
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToResizeColumns = false;
            this.dgvData.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtKodeNota,
            this.txtNamaPembeli,
            this.txtBayarTunai,
            this.txtBayarKredit});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvData.Location = new System.Drawing.Point(30, 183);
            this.dgvData.Name = "dgvData";
            this.dgvData.Size = new System.Drawing.Size(564, 290);
            this.dgvData.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(202, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(251, 30);
            this.label1.TabIndex = 45;
            this.label1.Text = "LAPORAN PENJUALAN";
            // 
            // dtpDate
            // 
            this.dtpDate.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDate.Location = new System.Drawing.Point(119, 126);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(121, 28);
            this.dtpDate.TabIndex = 2;
            this.dtpDate.Value = new System.DateTime(2016, 11, 29, 3, 36, 49, 0);
            // 
            // txtKodeNota
            // 
            this.txtKodeNota.Frozen = true;
            this.txtKodeNota.HeaderText = "Kode Nota";
            this.txtKodeNota.Name = "txtKodeNota";
            this.txtKodeNota.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtKodeNota.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.txtKodeNota.Width = 120;
            // 
            // txtNamaPembeli
            // 
            this.txtNamaPembeli.HeaderText = "Pembeli";
            this.txtNamaPembeli.Name = "txtNamaPembeli";
            this.txtNamaPembeli.ReadOnly = true;
            this.txtNamaPembeli.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtNamaPembeli.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.txtNamaPembeli.Width = 160;
            // 
            // txtBayarTunai
            // 
            this.txtBayarTunai.HeaderText = "Tunai";
            this.txtBayarTunai.Name = "txtBayarTunai";
            this.txtBayarTunai.ReadOnly = true;
            this.txtBayarTunai.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtBayarTunai.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.txtBayarTunai.Width = 120;
            // 
            // txtBayarKredit
            // 
            this.txtBayarKredit.HeaderText = "Kredit";
            this.txtBayarKredit.Name = "txtBayarKredit";
            this.txtBayarKredit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtBayarKredit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.txtBayarKredit.Width = 120;
            // 
            // fmLaporanPenjualan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 485);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cboPeriode);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.label1);
            this.Name = "fmLaporanPenjualan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fmLaporanPenjualan";
            this.Load += new System.EventHandler(this.fmLaporanPenjualan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboPeriode;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKodeNota;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNamaPembeli;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtBayarTunai;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtBayarKredit;
    }
}