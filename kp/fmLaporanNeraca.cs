﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmLaporanNeraca : Form
    {

        MySqlConnection con = new MySqlConnection();
        string[] nama;
        int[] setelah;
        public fmLaporanNeraca()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";
        }

        private void fmLaporanNeraca_Load(object sender, EventArgs e)
        {
            AmbilNeracaAset();
        }

        public void AmbilNeracaAset()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT saldo_setelah_closing, nama_akun FROM akun WHERE id_kelompok_akun=1 OR id_kelompok_akun=2 OR id_kelompok_akun=3 OR id_kelompok_akun=4 ORDER BY no_akun ASC";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            nama = new string[18];
            setelah = new int[18];
            int cek = 0;
            while (objReader.Read() == true)
            {
                setelah[cek] = int.Parse(objReader.GetValue(0).ToString());
                nama[cek] = objReader.GetValue(1).ToString();
                cek++;
            }

            if(setelah[0]<0)
                lblKas.Text = "("+setelah[0].ToString()+")";
            else
                lblKas.Text = setelah[0].ToString();

            if (setelah[1] < 0)
                lblBca.Text = "(" + setelah[1].ToString() + ")";
            else
                lblBca.Text = setelah[1].ToString();

            if (setelah[2] < 0)
                lblMandiri.Text = "(" + setelah[2].ToString() + ")";
            else
                lblMandiri.Text = setelah[2].ToString();

            if (setelah[3] < 0)
                lblPiutangDagang.Text = "(" + setelah[3].ToString() + ")";
            else
                lblPiutangDagang.Text = setelah[3].ToString();

            if (setelah[4] < 0)
                lblPiutangKredit.Text = "(" + setelah[4].ToString() + ")";
            else
                lblPiutangKredit.Text = setelah[4].ToString();

            if (setelah[5] < 0)
                lblSediaanBarang.Text = "(" + setelah[5].ToString() + ")";
            else
                lblSediaanBarang.Text = setelah[5].ToString();
            
            if (setelah[6] < 0)
                lblSediaanRusak.Text = "(" + setelah[6].ToString() + ")";
            else
                lblSediaanRusak.Text = setelah[6].ToString();

            if (setelah[7] < 0)
                lblSediaanHabisToko.Text = "(" + setelah[7].ToString() + ")";
            else
                lblSediaanHabisToko.Text = setelah[7].ToString();
            
            if (setelah[8] < 0)
                lblPeralatanToko.Text = "(" + setelah[8].ToString() + ")";
            else
                lblPeralatanToko.Text = setelah[8].ToString();
            
            if (setelah[9] < 0)
                lblAkumulasiToko.Text = "(" + setelah[9].ToString() + ")";
            else
                lblAkumulasiToko.Text = setelah[9].ToString();

            if (setelah[10] < 0)
                lblKendaraan.Text = "(" + setelah[10].ToString() + ")";
            else
                lblKendaraan.Text = setelah[10].ToString();
            
            if (setelah[11] < 0)
                lblAkumulasiKendaraan.Text = "(" + setelah[11].ToString() + ")";
            else
                lblAkumulasiKendaraan.Text = setelah[11].ToString();
            
            if (setelah[12] < 0)
                lblTanah.Text = "(" + setelah[12].ToString() + ")";
            else
                lblTanah.Text = setelah[12].ToString();

            int totalAset = 0;
            for (int i = 0; i < 13; i++ )
            {
                totalAset += setelah[i];
                //MessageBox.Show(nama[i]);
            }
            lblTotalAset.Text = totalAset.ToString();

            if (setelah[13] < 0)
                lblHutangDagang.Text = "(" + setelah[13].ToString() + ")";
            else
                lblHutangDagang.Text = setelah[13].ToString();
            if (setelah[14] < 0)
                lblHutangKredit.Text = "(" + setelah[14].ToString() + ")";
            else
                lblHutangKredit.Text = setelah[14].ToString();
            if (setelah[15] < 0)
                lblHutangPajak.Text = "(" + setelah[15].ToString() + ")";
            else
                lblHutangPajak.Text = setelah[15].ToString();
            if (setelah[16] < 0)
                lblHutangGaji.Text = "(" + setelah[16].ToString() + ")";
            else
                lblHutangGaji.Text = setelah[16].ToString();

            int totalBeban = 0;
            for (int i = 13; i < 17; i++)
            {
                totalBeban += setelah[i];
                //MessageBox.Show(nama[i]);
            }
            lblTotalKewajiban.Text = totalBeban.ToString();

            if (setelah[17] < 0)
                lblModalPemilik.Text = "(" + setelah[17].ToString() + ")";
            else
                lblModalPemilik.Text = setelah[17].ToString();

            int ekui = 0;
            for (int i = 17; i < 18; i++)
            {
                ekui += setelah[i];
                //MessageBox.Show(nama[i]);
            }
            ekui += totalBeban;
            lblTotalEkuitasKewajiban.Text = ekui.ToString();
                objReader.Close();
            con.Close();
        }

        Bitmap bmp;

        private void btnCetak_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            bmp = new Bitmap(this.Size.Width, this.Size.Height, g);
            Graphics ng = Graphics.FromImage(bmp);
            ng.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, this.Size);
            printPreviewDialog1.ShowDialog();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(bmp, 0, 0);
        }
    }
}
