﻿namespace kp
{
    partial class fmLaporanNeraca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fmLaporanNeraca));
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblAkumulasiKendaraan = new System.Windows.Forms.Label();
            this.lblKendaraan = new System.Windows.Forms.Label();
            this.lblAkumulasiToko = new System.Windows.Forms.Label();
            this.lblPeralatanToko = new System.Windows.Forms.Label();
            this.lblSediaanRusak = new System.Windows.Forms.Label();
            this.lblSediaanBarang = new System.Windows.Forms.Label();
            this.lblTanah = new System.Windows.Forms.Label();
            this.lblSediaanHabisToko = new System.Windows.Forms.Label();
            this.lblPiutangKredit = new System.Windows.Forms.Label();
            this.lblPiutangDagang = new System.Windows.Forms.Label();
            this.lblMandiri = new System.Windows.Forms.Label();
            this.lblBca = new System.Windows.Forms.Label();
            this.lblKas = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lblTotalAset = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblHutangGaji = new System.Windows.Forms.Label();
            this.lblHutangPajak = new System.Windows.Forms.Label();
            this.lblHutangKredit = new System.Windows.Forms.Label();
            this.lblHutangDagang = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblTotalKewajiban = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblModalPemilik = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblTotalEkuitasKewajiban = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btnCetak = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(206, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "LAPORAN NERACA";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblAkumulasiKendaraan);
            this.groupBox1.Controls.Add(this.lblKendaraan);
            this.groupBox1.Controls.Add(this.lblAkumulasiToko);
            this.groupBox1.Controls.Add(this.lblPeralatanToko);
            this.groupBox1.Controls.Add(this.lblSediaanRusak);
            this.groupBox1.Controls.Add(this.lblSediaanBarang);
            this.groupBox1.Controls.Add(this.lblTanah);
            this.groupBox1.Controls.Add(this.lblSediaanHabisToko);
            this.groupBox1.Controls.Add(this.lblPiutangKredit);
            this.groupBox1.Controls.Add(this.lblPiutangDagang);
            this.groupBox1.Controls.Add(this.lblMandiri);
            this.groupBox1.Controls.Add(this.lblBca);
            this.groupBox1.Controls.Add(this.lblKas);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.lbl);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 83);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(773, 268);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Aktiva(Aset)";
            // 
            // lblAkumulasiKendaraan
            // 
            this.lblAkumulasiKendaraan.AutoSize = true;
            this.lblAkumulasiKendaraan.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAkumulasiKendaraan.Location = new System.Drawing.Point(638, 167);
            this.lblAkumulasiKendaraan.Name = "lblAkumulasiKendaraan";
            this.lblAkumulasiKendaraan.Size = new System.Drawing.Size(19, 21);
            this.lblAkumulasiKendaraan.TabIndex = 30;
            this.lblAkumulasiKendaraan.Text = "-";
            // 
            // lblKendaraan
            // 
            this.lblKendaraan.AutoSize = true;
            this.lblKendaraan.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKendaraan.Location = new System.Drawing.Point(510, 140);
            this.lblKendaraan.Name = "lblKendaraan";
            this.lblKendaraan.Size = new System.Drawing.Size(19, 21);
            this.lblKendaraan.TabIndex = 29;
            this.lblKendaraan.Text = "-";
            // 
            // lblAkumulasiToko
            // 
            this.lblAkumulasiToko.AutoSize = true;
            this.lblAkumulasiToko.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAkumulasiToko.Location = new System.Drawing.Point(594, 107);
            this.lblAkumulasiToko.Name = "lblAkumulasiToko";
            this.lblAkumulasiToko.Size = new System.Drawing.Size(19, 21);
            this.lblAkumulasiToko.TabIndex = 28;
            this.lblAkumulasiToko.Text = "-";
            // 
            // lblPeralatanToko
            // 
            this.lblPeralatanToko.AutoSize = true;
            this.lblPeralatanToko.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeralatanToko.Location = new System.Drawing.Point(510, 77);
            this.lblPeralatanToko.Name = "lblPeralatanToko";
            this.lblPeralatanToko.Size = new System.Drawing.Size(19, 21);
            this.lblPeralatanToko.TabIndex = 27;
            this.lblPeralatanToko.Text = "-";
            // 
            // lblSediaanRusak
            // 
            this.lblSediaanRusak.AutoSize = true;
            this.lblSediaanRusak.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSediaanRusak.Location = new System.Drawing.Point(619, 45);
            this.lblSediaanRusak.Name = "lblSediaanRusak";
            this.lblSediaanRusak.Size = new System.Drawing.Size(19, 21);
            this.lblSediaanRusak.TabIndex = 26;
            this.lblSediaanRusak.Text = "-";
            // 
            // lblSediaanBarang
            // 
            this.lblSediaanBarang.AutoSize = true;
            this.lblSediaanBarang.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSediaanBarang.Location = new System.Drawing.Point(571, 16);
            this.lblSediaanBarang.Name = "lblSediaanBarang";
            this.lblSediaanBarang.Size = new System.Drawing.Size(19, 21);
            this.lblSediaanBarang.TabIndex = 25;
            this.lblSediaanBarang.Text = "-";
            // 
            // lblTanah
            // 
            this.lblTanah.AutoSize = true;
            this.lblTanah.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTanah.Location = new System.Drawing.Point(185, 199);
            this.lblTanah.Name = "lblTanah";
            this.lblTanah.Size = new System.Drawing.Size(19, 21);
            this.lblTanah.TabIndex = 24;
            this.lblTanah.Text = "-";
            // 
            // lblSediaanHabisToko
            // 
            this.lblSediaanHabisToko.AutoSize = true;
            this.lblSediaanHabisToko.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSediaanHabisToko.Location = new System.Drawing.Point(223, 167);
            this.lblSediaanHabisToko.Name = "lblSediaanHabisToko";
            this.lblSediaanHabisToko.Size = new System.Drawing.Size(19, 21);
            this.lblSediaanHabisToko.TabIndex = 23;
            this.lblSediaanHabisToko.Text = "-";
            // 
            // lblPiutangKredit
            // 
            this.lblPiutangKredit.AutoSize = true;
            this.lblPiutangKredit.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPiutangKredit.Location = new System.Drawing.Point(185, 140);
            this.lblPiutangKredit.Name = "lblPiutangKredit";
            this.lblPiutangKredit.Size = new System.Drawing.Size(19, 21);
            this.lblPiutangKredit.TabIndex = 22;
            this.lblPiutangKredit.Text = "-";
            // 
            // lblPiutangDagang
            // 
            this.lblPiutangDagang.AutoSize = true;
            this.lblPiutangDagang.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPiutangDagang.Location = new System.Drawing.Point(146, 107);
            this.lblPiutangDagang.Name = "lblPiutangDagang";
            this.lblPiutangDagang.Size = new System.Drawing.Size(19, 21);
            this.lblPiutangDagang.TabIndex = 21;
            this.lblPiutangDagang.Text = "-";
            // 
            // lblMandiri
            // 
            this.lblMandiri.AutoSize = true;
            this.lblMandiri.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMandiri.Location = new System.Drawing.Point(164, 77);
            this.lblMandiri.Name = "lblMandiri";
            this.lblMandiri.Size = new System.Drawing.Size(19, 21);
            this.lblMandiri.TabIndex = 20;
            this.lblMandiri.Text = "-";
            // 
            // lblBca
            // 
            this.lblBca.AutoSize = true;
            this.lblBca.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBca.Location = new System.Drawing.Point(140, 45);
            this.lblBca.Name = "lblBca";
            this.lblBca.Size = new System.Drawing.Size(19, 21);
            this.lblBca.TabIndex = 19;
            this.lblBca.Text = "-";
            // 
            // lblKas
            // 
            this.lblKas.AutoSize = true;
            this.lblKas.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKas.Location = new System.Drawing.Point(61, 16);
            this.lblKas.Name = "lblKas";
            this.lblKas.Size = new System.Drawing.Size(19, 21);
            this.lblKas.TabIndex = 18;
            this.lblKas.Text = "-";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblTotalAset);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Location = new System.Drawing.Point(377, 199);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(378, 56);
            this.groupBox4.TabIndex = 17;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Total";
            // 
            // lblTotalAset
            // 
            this.lblTotalAset.AutoSize = true;
            this.lblTotalAset.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalAset.Location = new System.Drawing.Point(133, 16);
            this.lblTotalAset.Name = "lblTotalAset";
            this.lblTotalAset.Size = new System.Drawing.Size(19, 21);
            this.lblTotalAset.TabIndex = 31;
            this.lblTotalAset.Text = "-";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(26, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(101, 21);
            this.label20.TabIndex = 18;
            this.label20.Text = "Total Aset :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 167);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(210, 21);
            this.label9.TabIndex = 11;
            this.label9.Text = "Sediaan Habis Pakai Toko :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 199);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(171, 21);
            this.label14.TabIndex = 16;
            this.label14.Text = "Tanah dan Bangunan :";
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl.Location = new System.Drawing.Point(373, 167);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(259, 21);
            this.lbl.TabIndex = 15;
            this.lbl.Text = "Akumulasi Penyusutan Kendaraan :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(373, 140);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 21);
            this.label12.TabIndex = 14;
            this.label12.Text = "Kendaraan :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(373, 107);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(220, 21);
            this.label11.TabIndex = 13;
            this.label11.Text = "Akumulasi Penyusutan Toko :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(373, 77);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(131, 21);
            this.label10.TabIndex = 12;
            this.label10.Text = "Peralatan Toko :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(373, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(240, 21);
            this.label8.TabIndex = 10;
            this.label8.Text = "Sediaan Barang Dagang Rusak :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(373, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(192, 21);
            this.label7.TabIndex = 9;
            this.label7.Text = "Sediaan Barang Dagang :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(172, 21);
            this.label6.TabIndex = 8;
            this.label6.Text = "Piutang Kartu Kredit :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(7, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 21);
            this.label5.TabIndex = 7;
            this.label5.Text = "Piutang Dagang :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Tabungan Mandiri :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "Tabungan BCA :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "Kas :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblHutangGaji);
            this.groupBox2.Controls.Add(this.lblHutangPajak);
            this.groupBox2.Controls.Add(this.lblHutangKredit);
            this.groupBox2.Controls.Add(this.lblHutangDagang);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Location = new System.Drawing.Point(12, 357);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(497, 171);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Kewajiban";
            // 
            // lblHutangGaji
            // 
            this.lblHutangGaji.AutoSize = true;
            this.lblHutangGaji.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHutangGaji.Location = new System.Drawing.Point(124, 113);
            this.lblHutangGaji.Name = "lblHutangGaji";
            this.lblHutangGaji.Size = new System.Drawing.Size(19, 21);
            this.lblHutangGaji.TabIndex = 35;
            this.lblHutangGaji.Text = "-";
            // 
            // lblHutangPajak
            // 
            this.lblHutangPajak.AutoSize = true;
            this.lblHutangPajak.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHutangPajak.Location = new System.Drawing.Point(207, 78);
            this.lblHutangPajak.Name = "lblHutangPajak";
            this.lblHutangPajak.Size = new System.Drawing.Size(19, 21);
            this.lblHutangPajak.TabIndex = 34;
            this.lblHutangPajak.Text = "-";
            // 
            // lblHutangKredit
            // 
            this.lblHutangKredit.AutoSize = true;
            this.lblHutangKredit.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHutangKredit.Location = new System.Drawing.Point(186, 47);
            this.lblHutangKredit.Name = "lblHutangKredit";
            this.lblHutangKredit.Size = new System.Drawing.Size(19, 21);
            this.lblHutangKredit.TabIndex = 33;
            this.lblHutangKredit.Text = "-";
            // 
            // lblHutangDagang
            // 
            this.lblHutangDagang.AutoSize = true;
            this.lblHutangDagang.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHutangDagang.Location = new System.Drawing.Point(147, 16);
            this.lblHutangDagang.Name = "lblHutangDagang";
            this.lblHutangDagang.Size = new System.Drawing.Size(19, 21);
            this.lblHutangDagang.TabIndex = 32;
            this.lblHutangDagang.Text = "-";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblTotalKewajiban);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Location = new System.Drawing.Point(317, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(162, 139);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Total";
            // 
            // lblTotalKewajiban
            // 
            this.lblTotalKewajiban.AutoSize = true;
            this.lblTotalKewajiban.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalKewajiban.Location = new System.Drawing.Point(18, 66);
            this.lblTotalKewajiban.Name = "lblTotalKewajiban";
            this.lblTotalKewajiban.Size = new System.Drawing.Size(19, 21);
            this.lblTotalKewajiban.TabIndex = 36;
            this.lblTotalKewajiban.Text = "-";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(18, 35);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(138, 21);
            this.label21.TabIndex = 18;
            this.label21.Text = "Total Kewajiban :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(8, 78);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(193, 21);
            this.label18.TabIndex = 20;
            this.label18.Text = "Hutang Pajak Penjualan :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(8, 113);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(110, 21);
            this.label17.TabIndex = 19;
            this.label17.Text = "Hutang Gaji :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 47);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(172, 21);
            this.label16.TabIndex = 18;
            this.label16.Text = "Hutang Kartu Kredit :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(133, 21);
            this.label15.TabIndex = 17;
            this.label15.Text = "Hutang Dagang :";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblModalPemilik);
            this.groupBox3.Controls.Add(this.groupBox6);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Location = new System.Drawing.Point(515, 357);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(270, 171);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ekuitas";
            // 
            // lblModalPemilik
            // 
            this.lblModalPemilik.AutoSize = true;
            this.lblModalPemilik.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModalPemilik.Location = new System.Drawing.Point(135, 30);
            this.lblModalPemilik.Name = "lblModalPemilik";
            this.lblModalPemilik.Size = new System.Drawing.Size(19, 21);
            this.lblModalPemilik.TabIndex = 37;
            this.lblModalPemilik.Text = "-";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lblTotalEkuitasKewajiban);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Location = new System.Drawing.Point(6, 78);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(258, 87);
            this.groupBox6.TabIndex = 19;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Total";
            // 
            // lblTotalEkuitasKewajiban
            // 
            this.lblTotalEkuitasKewajiban.AutoSize = true;
            this.lblTotalEkuitasKewajiban.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalEkuitasKewajiban.Location = new System.Drawing.Point(26, 52);
            this.lblTotalEkuitasKewajiban.Name = "lblTotalEkuitasKewajiban";
            this.lblTotalEkuitasKewajiban.Size = new System.Drawing.Size(19, 21);
            this.lblTotalEkuitasKewajiban.TabIndex = 38;
            this.lblTotalEkuitasKewajiban.Text = "-";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(26, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(212, 21);
            this.label22.TabIndex = 18;
            this.label22.Text = "Total Ekuitas + Kewajiban :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(16, 30);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(121, 21);
            this.label19.TabIndex = 21;
            this.label19.Text = "Modal Pemilik :";
            // 
            // btnCetak
            // 
            this.btnCetak.Location = new System.Drawing.Point(710, 534);
            this.btnCetak.Name = "btnCetak";
            this.btnCetak.Size = new System.Drawing.Size(75, 23);
            this.btnCetak.TabIndex = 4;
            this.btnCetak.Text = "Cetak";
            this.btnCetak.UseVisualStyleBackColor = true;
            this.btnCetak.Click += new System.EventHandler(this.btnCetak_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // fmLaporanNeraca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 565);
            this.Controls.Add(this.btnCetak);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "fmLaporanNeraca";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fmLaporanNeraca";
            this.Load += new System.EventHandler(this.fmLaporanNeraca_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblAkumulasiKendaraan;
        private System.Windows.Forms.Label lblKendaraan;
        private System.Windows.Forms.Label lblAkumulasiToko;
        private System.Windows.Forms.Label lblPeralatanToko;
        private System.Windows.Forms.Label lblSediaanRusak;
        private System.Windows.Forms.Label lblSediaanBarang;
        private System.Windows.Forms.Label lblTanah;
        private System.Windows.Forms.Label lblSediaanHabisToko;
        private System.Windows.Forms.Label lblPiutangKredit;
        private System.Windows.Forms.Label lblPiutangDagang;
        private System.Windows.Forms.Label lblMandiri;
        private System.Windows.Forms.Label lblBca;
        private System.Windows.Forms.Label lblKas;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label lblTotalAset;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblHutangGaji;
        private System.Windows.Forms.Label lblHutangPajak;
        private System.Windows.Forms.Label lblHutangKredit;
        private System.Windows.Forms.Label lblHutangDagang;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblTotalKewajiban;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblModalPemilik;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label lblTotalEkuitasKewajiban;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnCetak;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
    }
}