﻿namespace kp
{
    partial class fmNotaJual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvBarangNota = new System.Windows.Forms.DataGridView();
            this.txtKode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtBarang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtHarga = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtKuantiti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSubHarga = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnPrint = new System.Windows.Forms.Button();
            this.cboNamaPembeli = new System.Windows.Forms.ComboBox();
            this.txtKodeNota = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTerimaUang = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtUangKembali = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboCaraBayar = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboNamaBank = new System.Windows.Forms.ComboBox();
            this.txtNomorKartu = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chk_biayakirim = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBarangNota)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(251, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "NOTA JUAL";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(79, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Kode Nota ";
            // 
            // dgvBarangNota
            // 
            this.dgvBarangNota.AllowUserToResizeColumns = false;
            this.dgvBarangNota.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvBarangNota.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBarangNota.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBarangNota.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBarangNota.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBarangNota.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtKode,
            this.txtBarang,
            this.txtHarga,
            this.txtKuantiti,
            this.txtSubHarga});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBarangNota.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvBarangNota.Location = new System.Drawing.Point(31, 179);
            this.dgvBarangNota.Name = "dgvBarangNota";
            this.dgvBarangNota.Size = new System.Drawing.Size(564, 212);
            this.dgvBarangNota.TabIndex = 0;
            this.dgvBarangNota.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvBarangNota_CellBeginEdit);
            this.dgvBarangNota.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBarangNota_CellEndEdit);
            this.dgvBarangNota.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBarangNota_CellEndEdit);
            this.dgvBarangNota.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvBarangNota_KeyPress);
            // 
            // txtKode
            // 
            this.txtKode.Frozen = true;
            this.txtKode.HeaderText = "Kode";
            this.txtKode.Name = "txtKode";
            this.txtKode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtKode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtBarang
            // 
            this.txtBarang.HeaderText = "Barang";
            this.txtBarang.Name = "txtBarang";
            this.txtBarang.ReadOnly = true;
            this.txtBarang.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtBarang.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtHarga
            // 
            this.txtHarga.HeaderText = "Harga";
            this.txtHarga.Name = "txtHarga";
            this.txtHarga.ReadOnly = true;
            this.txtHarga.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtHarga.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtKuantiti
            // 
            this.txtKuantiti.HeaderText = "Qty";
            this.txtKuantiti.Name = "txtKuantiti";
            this.txtKuantiti.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtKuantiti.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtSubHarga
            // 
            this.txtSubHarga.HeaderText = "Sub Harga";
            this.txtSubHarga.Name = "txtSubHarga";
            this.txtSubHarga.ReadOnly = true;
            this.txtSubHarga.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.txtSubHarga.Width = 120;
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Location = new System.Drawing.Point(518, 141);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(77, 32);
            this.btnPrint.TabIndex = 10;
            this.btnPrint.Text = "Cetak";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // cboNamaPembeli
            // 
            this.cboNamaPembeli.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNamaPembeli.FormattingEnabled = true;
            this.cboNamaPembeli.Location = new System.Drawing.Point(206, 144);
            this.cboNamaPembeli.Name = "cboNamaPembeli";
            this.cboNamaPembeli.Size = new System.Drawing.Size(121, 28);
            this.cboNamaPembeli.TabIndex = 1;
            // 
            // txtKodeNota
            // 
            this.txtKodeNota.Enabled = false;
            this.txtKodeNota.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKodeNota.Location = new System.Drawing.Point(59, 144);
            this.txtKodeNota.Name = "txtKodeNota";
            this.txtKodeNota.Size = new System.Drawing.Size(121, 28);
            this.txtKodeNota.TabIndex = 0;
            // 
            // txtTotal
            // 
            this.txtTotal.Enabled = false;
            this.txtTotal.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(474, 400);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(121, 28);
            this.txtTotal.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(378, 403);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 20);
            this.label11.TabIndex = 27;
            this.label11.Text = "Total :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(438, 403);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 20);
            this.label12.TabIndex = 29;
            this.label12.Text = "Rp.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(438, 449);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 20);
            this.label17.TabIndex = 38;
            this.label17.Text = "Rp.";
            // 
            // txtTerimaUang
            // 
            this.txtTerimaUang.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTerimaUang.Location = new System.Drawing.Point(474, 446);
            this.txtTerimaUang.Name = "txtTerimaUang";
            this.txtTerimaUang.Size = new System.Drawing.Size(121, 28);
            this.txtTerimaUang.TabIndex = 11;
            this.txtTerimaUang.TextChanged += new System.EventHandler(this.txtTerimaUang_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(328, 449);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(108, 20);
            this.label18.TabIndex = 36;
            this.label18.Text = "Terima Uang : ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(438, 500);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 20);
            this.label19.TabIndex = 41;
            this.label19.Text = "Rp.";
            // 
            // txtUangKembali
            // 
            this.txtUangKembali.Enabled = false;
            this.txtUangKembali.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUangKembali.Location = new System.Drawing.Point(474, 497);
            this.txtUangKembali.Name = "txtUangKembali";
            this.txtUangKembali.Size = new System.Drawing.Size(121, 28);
            this.txtUangKembali.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(323, 500);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(113, 20);
            this.label20.TabIndex = 39;
            this.label20.Text = "Uang kembali : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(214, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 20);
            this.label7.TabIndex = 42;
            this.label7.Text = "Nama Pembeli";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(360, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 20);
            this.label8.TabIndex = 44;
            this.label8.Text = "Cara Bayar";
            // 
            // cboCaraBayar
            // 
            this.cboCaraBayar.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCaraBayar.FormattingEnabled = true;
            this.cboCaraBayar.Location = new System.Drawing.Point(346, 144);
            this.cboCaraBayar.Name = "cboCaraBayar";
            this.cboCaraBayar.Size = new System.Drawing.Size(121, 28);
            this.cboCaraBayar.TabIndex = 43;
            this.cboCaraBayar.SelectedIndexChanged += new System.EventHandler(this.cboCaraBayar_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(28, 427);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 20);
            this.label2.TabIndex = 46;
            this.label2.Text = "No. Kartu :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 473);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 20);
            this.label3.TabIndex = 48;
            this.label3.Text = "Nama Bank :";
            // 
            // cboNamaBank
            // 
            this.cboNamaBank.Enabled = false;
            this.cboNamaBank.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNamaBank.FormattingEnabled = true;
            this.cboNamaBank.Location = new System.Drawing.Point(117, 470);
            this.cboNamaBank.Name = "cboNamaBank";
            this.cboNamaBank.Size = new System.Drawing.Size(121, 28);
            this.cboNamaBank.TabIndex = 49;
            // 
            // txtNomorKartu
            // 
            this.txtNomorKartu.Enabled = false;
            this.txtNomorKartu.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomorKartu.Location = new System.Drawing.Point(117, 427);
            this.txtNomorKartu.Name = "txtNomorKartu";
            this.txtNomorKartu.Size = new System.Drawing.Size(121, 28);
            this.txtNomorKartu.TabIndex = 52;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(424, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(189, 15);
            this.label6.TabIndex = 71;
            this.label6.Text = "* Termasuk Biaya Kirim (Rp. 9.000)";
            // 
            // chk_biayakirim
            // 
            this.chk_biayakirim.AutoSize = true;
            this.chk_biayakirim.Location = new System.Drawing.Point(117, 404);
            this.chk_biayakirim.Name = "chk_biayakirim";
            this.chk_biayakirim.Size = new System.Drawing.Size(95, 17);
            this.chk_biayakirim.TabIndex = 72;
            this.chk_biayakirim.Text = "Barang dikirim ";
            this.chk_biayakirim.UseVisualStyleBackColor = true;
            this.chk_biayakirim.CheckedChanged += new System.EventHandler(this.chk_biayakirim_CheckedChanged);
            // 
            // fmNotaJual
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 540);
            this.ControlBox = false;
            this.Controls.Add(this.chk_biayakirim);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNomorKartu);
            this.Controls.Add(this.cboNamaBank);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cboCaraBayar);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtUangKembali);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtTerimaUang);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtKodeNota);
            this.Controls.Add(this.cboNamaPembeli);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.dgvBarangNota);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Name = "fmNotaJual";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Nota Jual";
            this.Load += new System.EventHandler(this.fmNotaJual_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBarangNota)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dgvBarangNota;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.ComboBox cboNamaPembeli;
        private System.Windows.Forms.TextBox txtKodeNota;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTerimaUang;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtUangKembali;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboCaraBayar;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKode;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtBarang;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtHarga;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKuantiti;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtSubHarga;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboNamaBank;
        private System.Windows.Forms.TextBox txtNomorKartu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chk_biayakirim;
    }
}