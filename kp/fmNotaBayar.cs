﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmNotaBayar : Form
    {
        MySqlConnection con = new MySqlConnection();
        int i = 0;
        string ubahFormatBulan = "";
        string[] splitTampungDate;
        long totalHarga = 0;
        long tampungUbah = 0;
        int totalBaris = 0;
        public fmNotaBayar()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";

            #region Cara Bayar Default
            cboCaraBayar.Text = "Pilih Cara Bayar ...";
            cboCaraBayar.Items.Add("Tunai");
            cboCaraBayar.Items.Add("Transfer/Debit");
            #endregion

            #region Jenis Aset Default
            cboJenisAset.Text = "Pilih Jenis Aset ...";
            cboJenisAset.Items.Add("Kendaraan");
            cboJenisAset.Items.Add("Toko");
            #endregion
        }

        private void fmNotaBayar_Load(object sender, EventArgs e)
        {
            string tampungDate = DateTime.Now.ToShortDateString();
            splitTampungDate = tampungDate.Split('/');
            if (splitTampungDate[0] == "1")
                ubahFormatBulan = "I";
            else if (splitTampungDate[0] == "2")
                ubahFormatBulan = "II";
            else if (splitTampungDate[0] == "3")
                ubahFormatBulan = "III";
            else if (splitTampungDate[0] == "4")
                ubahFormatBulan = "IV";
            else if (splitTampungDate[0] == "5")
                ubahFormatBulan = "V";
            else if (splitTampungDate[0] == "6")
                ubahFormatBulan = "VI";
            else if (splitTampungDate[0] == "7")
                ubahFormatBulan = "VII";
            else if (splitTampungDate[0] == "8")
                ubahFormatBulan = "VIII";
            else if (splitTampungDate[0] == "9")
                ubahFormatBulan = "IX";
            else if (splitTampungDate[0] == "10")
                ubahFormatBulan = "X";
            else if (splitTampungDate[0] == "11")
                ubahFormatBulan = "XI";
            else if (splitTampungDate[0] == "12")
                ubahFormatBulan = "XII";

            NomorNota(ubahFormatBulan, splitTampungDate[2]);
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            #region Isi Data
            
            if (cboCaraBayar.Text == "Pilih Cara Bayar ..." || txtTotal.Text == "" || txtTotal.Text == "0" || txtNoNota.Text == "" || cboJenisAset.Text == "Pilih Jenis Aset ..." || txtNamaAset.Text=="" || txtDeskripsi.Text=="" || txtPenyusutan.Text == "")
            {
                MessageBox.Show("Data Belum Lengkap!!!");
            }
            else
            {
                SimpanNota();
                simpanJurnal();
                SimpanAkunJurnal1();
                SimpanAkunJurnal2();
            }
            tampungUbah = 0;
            #endregion 
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }

        public void NomorNota(string bulan, String Tahun)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT Count(*) FROM nota_bayar Where status=0";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                
                int ubahDataInt = int.Parse(objReader.GetValue(0).ToString()) + 1;
                string data = ubahDataInt.ToString();
                string kodeNota = "Y " + data + "/" + bulan + "/" + Tahun;
                txtNoNota.Text = kodeNota;
                //MessageBox.Show(data+"/"+bulan+"/"+Tahun);
            }
            objReader.Close();
            con.Close();
        }
        public string UbahFormatDate(string tahun, string bulan, string tanggal)
        {
            string a = tahun + "-" + bulan + "-" + tanggal;
            return a;
        }

        public void SimpanNota()
        {
            try
            {
                con.Open();
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into nota_bayar Values (@kode, @jenis_aset, @nama_aset, @deskripsi, @total_Bayar, @penyusutan, @cara_bayar, @waktuJual, @id_karyawan, @status)";
                objKomen.Parameters.AddWithValue("@kode", txtNoNota.Text);
                objKomen.Parameters.AddWithValue("@jenis_aset", cboJenisAset.Text);
                objKomen.Parameters.AddWithValue("@nama_aset", txtNamaAset.Text);
                objKomen.Parameters.AddWithValue("@deskripsi", txtDeskripsi.Text);
                objKomen.Parameters.AddWithValue("@penyusutan", int.Parse(txtPenyusutan.Text));
                objKomen.Parameters.AddWithValue("@total_Bayar", int.Parse(txtTotal.Text));
                objKomen.Parameters.AddWithValue("@cara_Bayar", cboCaraBayar.Text);
                objKomen.Parameters.AddWithValue("@waktuJual", UbahFormatDate(splitTampungDate[2], splitTampungDate[0], splitTampungDate[1]));
                objKomen.Parameters.AddWithValue("@id_karyawan", 1);
                objKomen.Parameters.AddWithValue("@status", 0);
                int hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("Penambahan data yang Anda tambahkan berhasil!");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

        }
        public void simpanJurnal()
        {
            string keterangan = "Umum - Nama Aset" + txtNamaAset.Text + " dengan jumlah biaya " + txtTotal.Text;
            try
            {
                con.Open();
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into jurnal Values (@kode, @jenis, @no_bukti, @keterangan, @tgl)";
                objKomen.Parameters.AddWithValue("@kode", "");
                objKomen.Parameters.AddWithValue("@jenis", "umum");
                objKomen.Parameters.AddWithValue("@no_bukti", txtNoNota.Text);
                objKomen.Parameters.AddWithValue("@keterangan", keterangan);
                objKomen.Parameters.AddWithValue("@tgl", UbahFormatDate(splitTampungDate[2], splitTampungDate[0], splitTampungDate[1]));
                int hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("jurnal masuk");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        int id = 0;
        public void AmbilIdJurnal()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id_jurnal FROM jurnal WHERE no_bukti='" + txtNoNota.Text + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                id = int.Parse(objReader.GetValue(0).ToString());
            }
            objReader.Close();
            con.Close();
        }
        public void SimpanAkunJurnal1()
        {
            AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun, @id_jurnal, @urutan, @debet, @kredit)";
                if (cboJenisAset.Text == "Kendaraan")
                {
                    objKomen.Parameters.AddWithValue("@no_akun", 10110);
                }
                else if (cboJenisAset.Text == "Toko")
                {
                    objKomen.Parameters.AddWithValue("@no_akun", 10090);
                }
                objKomen.Parameters.AddWithValue("@id_jurnal", id);
                objKomen.Parameters.AddWithValue("@urutan", 1);
                objKomen.Parameters.AddWithValue("@debet", double.Parse(txtTotal.Text));
                objKomen.Parameters.AddWithValue("@kredit", 0);
                hasil = objKomen.ExecuteNonQuery();
                //test
                if (hasil > 0)
                {
                    MessageBox.Show("Akun Jurnal Masuk");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void SimpanAkunJurnal2()
        {
            AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun2, @id_jurnal2, @urutan2, @debet2, @kredit2)";
                objKomen.Parameters.AddWithValue("@no_akun2", 10010);
                objKomen.Parameters.AddWithValue("@id_jurnal2", id);
                objKomen.Parameters.AddWithValue("@urutan2", 2);
                objKomen.Parameters.AddWithValue("@debet2", 0);
                objKomen.Parameters.AddWithValue("@kredit2", double.Parse(txtTotal.Text));
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("Akun Jurnal Masuk");
                    con.Close();
                    NomorNota(ubahFormatBulan, splitTampungDate[2]);
                    con.Open();
                    cboCaraBayar.Text = "Pilih Cara Bayar ...";
                    cboJenisAset.Text = "Pilih Cara Bayar ...";
                    txtDeskripsi.Clear();
                    txtNamaAset.Clear();
                    txtPenyusutan.Clear();
                    txtTotal.Clear();
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }    

    }
}
