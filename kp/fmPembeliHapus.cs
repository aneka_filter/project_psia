﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmPembeliHapus : Form
    {
        MySqlConnection con = new MySqlConnection();
        string a = "";
        string b = "";
        string c = "";
        string d = "";
        public fmPembeliHapus()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =aneka_filter; uid=root;";
        }

        private void btnLihatData_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmPembeliData();
            fm1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(txtKodeUbah.Text!="")
            {
                HapusData();
            }
            else
            {
                MessageBox.Show("Isi Data!!!");

            }
            HapusData();

            txtKodeUbah.Text = "";
        }

        #region Hapus Data
        public void HapusData()
        {
                con.Open();

                MySqlCommand objData = new MySqlCommand();
                objData.Connection = con;
                objData.CommandType = CommandType.Text;
                objData.CommandText = "SELECT id, nama, alamat, no_telp FROM supplier WHERE id='" + txtKodeUbah.Text + "'";
                MySqlDataReader objReader;
                objReader = objData.ExecuteReader();

                while (objReader.Read() == true)
                {
                    a = objReader.GetValue(0).ToString();
                    b = objReader.GetValue(1).ToString();
                    c = objReader.GetValue(2).ToString();
                    d = objReader.GetValue(3).ToString();
                }
                objReader.Close();

                DialogResult result = MessageBox.Show("Apakah Anda Yakin Menghapus Data \nKode: " + a + "\nNama: " + b + "\nAlamat: " + c + "\nNo. Telp: " + d,
                    "Hapus Data",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    objData.CommandText = "UPDATE supplier SET status='1' WHERE id='" + txtKodeUbah.Text + "'";

                    int hasil = objData.ExecuteNonQuery();

                    if (hasil > 0)
                    {
                        MessageBox.Show("Berhasil Hapus Data Supplier!!!");
                        txtKodeUbah.Text = "";
                    }
                }
                else if (result == DialogResult.No)
                {
                    MessageBox.Show("Batal Hapus Data Supplier!!!");
                    txtKodeUbah.Text = "";
                }

                con.Close();
            
        }


        #endregion

        private void fmPembeliHapus_Load(object sender, EventArgs e)
        {

        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }

    }
}
