﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmKaryawanHapus : Form
    {
        MySqlConnection con = new MySqlConnection();
        string a = "";
        string b = "";
        string c = "";
        string d = "";
        string e = "";
        string f = "";
        public fmKaryawanHapus()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =aneka_filter; uid=root;";
        }

        private void btnLihatData_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmKaryawanData();
            fm1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtNamaUbah.Text != "")
            {
                HapusData();
            }
            else
            {
                MessageBox.Show("Isi Data!!!");
            }

            txtNamaUbah.Text = "";
        }

        #region Hapus Data
        public void HapusData()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT nama,alamat, no_telp, kata_sandi, hak_akses, gaji FROM karyawan WHERE nama='" + txtNamaUbah.Text + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                a = objReader.GetValue(0).ToString();
                b = objReader.GetValue(1).ToString();
                c = objReader.GetValue(2).ToString();
                d = objReader.GetValue(3).ToString();
                e = objReader.GetValue(4).ToString();
                f = objReader.GetValue(5).ToString();
            }
            objReader.Close();

            DialogResult result = MessageBox.Show("Apakah Anda Yakin Menghapus Data \nNama: " + a + "\nAlamat: " + b + "\nNo. Telp: " + c + "\nJabatan: " + e + "Gaji: " + f, 
                "Hapus Data", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                objData.CommandText = "UPDATE karyawan SET status='1' WHERE nama='" + txtNamaUbah.Text + "'";

                int hasil = objData.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("Berhasil Hapus Data Karyawan!!!");
                    txtNamaUbah.Text = ""; 
                }
            }
            else if (result == DialogResult.No)
            {
                MessageBox.Show("Batal Hapus Data Karyawan!!!");
                txtNamaUbah.Text = ""; 
            }
            
            con.Close();
        }


        #endregion

        private void fmKaryawanHapus_Load(object sender, EventArgs e)
        {

        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }
    }
}
