﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmNeraca : Form
    {
        MySqlConnection con = new MySqlConnection();
        double totalPasiva = 0;
        string[] splitTampungDate;
        string ubahFormatBulan = "";
        public string UbahFormatDate(string tahun, string bulan, string tanggal)
        {
            string a = tahun + "-" + bulan + "-" + tanggal;
            return a;
        }
        public fmNeraca()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";

            con.Open();
            totalPasiva += loadAset();
            totalPasiva += loadKewajiban();
            totalPasiva += loadEkuitas();
            txtTotalPasiva.Text = totalPasiva.ToString();
            con.Close();
            string tampungDate = DateTime.Now.ToShortDateString();
            splitTampungDate = tampungDate.Split('/');
            if (splitTampungDate[0] == "1")
                ubahFormatBulan = "I";
            else if (splitTampungDate[0] == "2")
                ubahFormatBulan = "II";
            else if (splitTampungDate[0] == "3")
                ubahFormatBulan = "III";
            else if (splitTampungDate[0] == "4")
                ubahFormatBulan = "IV";
            else if (splitTampungDate[0] == "5")
                ubahFormatBulan = "V";
            else if (splitTampungDate[0] == "6")
                ubahFormatBulan = "VI";
            else if (splitTampungDate[0] == "7")
                ubahFormatBulan = "VII";
            else if (splitTampungDate[0] == "8")
                ubahFormatBulan = "VIII";
            else if (splitTampungDate[0] == "9")
                ubahFormatBulan = "IX";
            else if (splitTampungDate[0] == "10")
                ubahFormatBulan = "X";
            else if (splitTampungDate[0] == "11")
                ubahFormatBulan = "XI";
            else if (splitTampungDate[0] == "12")
                ubahFormatBulan = "XII";
        }

        public double loadAset()
        {
            #region Aset
            DataTable t = new DataTable();
            //MySqlCommand objKomen = new MySqlCommand();
            MySqlCommand cmd = new MySqlCommand("SELECT no_akun, nama_akun," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak" +
            " WHERE id_kelompok_akun <= 2" +
            " ORDER BY no_akun", con);
            MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
            adapter.Fill(t);
            dgvAset.DataSource = t;
            double total = 0;
            for (int i = 0; i < dgvAset.RowCount - 1; i++)
            {

                if (dgvAset.Rows[i].Cells[0].Value.ToString() == "10100" || dgvAset.Rows[i].Cells[0].Value.ToString() == "10120")
                {
                    if (dgvAset.Rows[i].Cells[2].Value.ToString() != "")
                        total -= double.Parse(dgvAset.Rows[i].Cells[2].Value.ToString());
                }
                else
                {
                    if (dgvAset.Rows[i].Cells[2].Value.ToString() != "")
                        total += double.Parse(dgvAset.Rows[i].Cells[2].Value.ToString());
                }
            }
            txtTotalAset.Text = total.ToString();
            return total;
            #endregion
        }

        public double loadKewajiban()
        {
            DataTable t = new DataTable();
            //MySqlCommand objKomen = new MySqlCommand();
            MySqlCommand cmd = new MySqlCommand("SELECT no_akun, nama_akun," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak" +
            " WHERE id_kelompok_akun = 3" +
            " ORDER BY no_akun", con);
            MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
            adapter.Fill(t);
            dgvKewajiban.DataSource = t;
            double total = 0;
            for (int i = 0; i < dgvKewajiban.RowCount - 1; i++)
            {
                //MessageBox.Show(dgvKewajiban.Rows[i].Cells[2].ToString());
                if (dgvKewajiban.Rows[i].Cells[2].Value.ToString() != "")
                
                total += double.Parse(dgvKewajiban.Rows[i].Cells[2].Value.ToString());
            }
            txtTotalKewajiban.Text = total.ToString();
            return total;
            //sintya gatel
        }

        public double loadEkuitas()
        {
            DataTable t = new DataTable();
            MySqlCommand cmd = new MySqlCommand("SELECT 'modal',((SELECT IF (ak.saldo_normal = 'kredit',(SELECT saldo_awal - SUM(debet) + SUM(kredit)"+
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)"+
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total"+
            "    FROM akun ak"+
            "   WHERE id_kelompok_akun = 4"+
            "   GROUP by id_kelompok_akun"+
            "   ORDER BY saldo_normal) -(SELECT IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)"+

            "   FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)"+
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total"+
            "   FROM akun ak"+
            "    WHERE id_kelompok_akun = 6"+
            "    GROUP by id_kelompok_akun"+
            "    ORDER BY saldo_normal) +(SELECT IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)"+

            "   FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)"+
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total"+
            "   FROM akun ak"+
            "    WHERE id_kelompok_akun = 5"+
            "    GROUP by id_kelompok_akun"+
            "   ORDER BY saldo_normal) ) as total FROM akun LIMIT 1", con);
            MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
            adapter.Fill(t);
            dgvEkuitas.DataSource = t;
            double total = 0;
            if (dgvEkuitas.Rows[0].Cells[1].Value.ToString()!= "")
            {
                 total = double.Parse(dgvEkuitas.Rows[0].Cells[1].Value.ToString());
            }
            txtTotalEkuitas.Text = total.ToString();
            return total;
        }

        private void btn_penutupan_Click(object sender, EventArgs e)
        {
            tutuplabarugi();
            tutuppendapatan();
            tutupbiaya();
        }

        #region method penutupan
        public int AmbilIdJurnal(String kode)
        {
            int id = 0;
            con.Open();
            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id_jurnal FROM jurnal WHERE no_bukti='" + kode + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                id = Convert.ToInt16(objReader.GetValue(0));
            }
            objReader.Close();
            con.Close();
            return id;
        }
        private void tutuppendapatan()
        {
            String nomor = NomorNota(ubahFormatBulan, splitTampungDate[2]);
            jurnal("Pendapatan", nomor);
            int idjurnal = AmbilIdJurnal(nomor);
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT no_akun, nama_akun," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak" +
            " WHERE id_kelompok_akun = 5" +
            " ORDER BY saldo_normal";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            int k = 0;
            double total = 0;
            String[,] data = new string[20, 5];

            while (objReader.Read() == true)
            {

                data[k, 0] = objReader.GetValue(0).ToString();
                data[k, 1] = idjurnal.ToString();
                data[k, 2] = (k + 1).ToString();

                double debet = 0;
                double kredit = 0;
                if (objReader.GetValue(1).ToString() == "Debet")
                {
                    kredit = int.Parse(objReader.GetValue(2).ToString());
                    total += kredit;
                    data[k, 4] = kredit.ToString();
                    data[k, 3] = "0";
                }
                else
                {
                    debet = int.Parse(objReader.GetValue(2).ToString());
                    total -= debet;
                    data[k, 3] = debet.ToString();
                    data[k, 4] = "0";
                }

                k++;

                //dgvBarangNota.Rows.Add(kodeTampung, nama, harga_satuan);
            }


            objReader.Close();
            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            for (int i = 0; i < 20; i++)
            {
                if (data[i, 0] != null)
                { 
                    objKomen.CommandText = "Insert Into akun_has_jurnal Values ("+ int.Parse(data[i, 0]) + 
                        ", "+ int.Parse(data[i, 1]) + ","+ int.Parse(data[i, 2]) + ", "+ double.Parse(data[i, 3]) + ", "+ double.Parse(data[i, 4]) + ")";
                    objKomen.ExecuteNonQuery();
                }
                else

                    break;
            }
            objKomen.CommandText = "Insert Into akun_has_jurnal Values (" + 70010+
                        ", " + idjurnal + "," + (k+1) + ", " + 0 + ", " +total + ")";
            objKomen.ExecuteNonQuery();



            con.Close();

        }
        private void tutupbiaya()
        {
            String nomor = NomorNota(ubahFormatBulan, splitTampungDate[2]);
            jurnal("Biaya", nomor);
            int idjurnal = AmbilIdJurnal(nomor);
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT no_akun, nama_akun," +
            "IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            " FROM akun ak" +
            " WHERE id_kelompok_akun = 5" +
            " ORDER BY saldo_normal";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();
            int k = 0;
            double total = 0;
            String[,] data = new string[20, 5];

            while (objReader.Read() == true)
            {

                data[k, 0] = objReader.GetValue(0).ToString();
                data[k, 1] = idjurnal.ToString();
                data[k, 2] = (k + 1).ToString();

                double debet = 0;
                double kredit = 0;
                if (objReader.GetValue(1).ToString() == "Debet")
                {
                    kredit = int.Parse(objReader.GetValue(2).ToString());
                    total += kredit;
                    data[k, 4] = kredit.ToString();
                    data[k, 3] = "0";
                }
                else
                {
                    debet = int.Parse(objReader.GetValue(2).ToString());
                    total -= debet;
                    data[k, 3] = debet.ToString();
                    data[k, 4] = "0";
                }

                k++;

                //dgvBarangNota.Rows.Add(kodeTampung, nama, harga_satuan);
            }


            objReader.Close();
            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            for (int i = 0; i < 20; i++)
            {
                if (data[i, 0] != null)
                {
                    objKomen.CommandText = "Insert Into akun_has_jurnal Values (" + int.Parse(data[i, 0]) +
                        ", " + int.Parse(data[i, 1]) + "," + int.Parse(data[i, 2]) + ", " + double.Parse(data[i, 3]) + ", " + double.Parse(data[i, 4]) + ")";
                    objKomen.ExecuteNonQuery();
                }
                else

                    break;
            }
            objKomen.CommandText = "Insert Into akun_has_jurnal Values (" + 70020 +
                        ", " + idjurnal + "," + (k + 1) + ", " + total + ", " + 0 + ")";
            objKomen.ExecuteNonQuery();



            con.Close();
        }
        private void tutuplabarugi()
        {
            String nomor = NomorNota(ubahFormatBulan, splitTampungDate[2]);
            jurnal("Laba Rugi", nomor);
            int idjurnal = AmbilIdJurnal(nomor);
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT 'modal',((SELECT IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +

            "   FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            "   FROM akun ak" +
            "    WHERE id_kelompok_akun = 6" +
            "    GROUP by id_kelompok_akun" +
            "    ORDER BY saldo_normal) +(SELECT IF(ak.saldo_normal = 'kredit', (SELECT saldo_awal - SUM(debet) + SUM(kredit)" +

            "   FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun), (SELECT saldo_awal + SUM(debet) - SUM(kredit)" +
            "    FROM akun a, akun_has_jurnal ahj WHERE a.no_akun = ak.no_akun AND ahj.no_akun = a.no_akun)) as total" +
            "   FROM akun ak" +
            "    WHERE id_kelompok_akun = 5" +
            "    GROUP by id_kelompok_akun" +
            "   ORDER BY saldo_normal) ) as total FROM akun LIMIT 1";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            double total = 0;

            while (objReader.Read() == true)
            {

                total = int.Parse(objReader.GetValue(1).ToString());
                //dgvBarangNota.Rows.Add(kodeTampung, nama, harga_satuan);
            }


            objReader.Close();

            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            objKomen.CommandText = "Insert Into akun_has_jurnal Values (" + 30010 +
                              ", " + idjurnal + "," + 1+ ", " + 0 + ", " + total + ")";
            objKomen.ExecuteNonQuery();


            objKomen.CommandText = "Insert Into akun_has_jurnal Values (" + 70030 +
                        ", " + idjurnal + "," + 2 + ", " + total + ", " + 0 + ")";
            objKomen.ExecuteNonQuery();
            con.Close();
        }
        public String NomorNota(string bulan, String Tahun)
        {
            con.Open();
            String kode = "";
            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT Count(*) FROM jurnal Where jenis = 'penutup'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                int ubahDataInt = Convert.ToInt16(objReader.GetValue(0)) + 1;
                string data = ubahDataInt.ToString();
                string kodeNota = "JP " + data + "/" + bulan + "/" + Tahun;
                kode = kodeNota;
                //MessageBox.Show(data+"/"+bulan+"/"+Tahun);
            }
            objReader.Close();
            con.Close();
            return kode;
        }
        private void jurnal(String Kode, String nomor)
        {
            String kt = "Penutupan - " + Kode;
            
            try
            {
                con.Open();
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into jurnal Values (@id, @jenis, @no_bukti, @keterangan, @tgl_penyusutan)";
                objKomen.Parameters.AddWithValue("@id", "");
                objKomen.Parameters.AddWithValue("@jenis", "penutup");
                objKomen.Parameters.AddWithValue("@no_bukti", nomor);
                objKomen.Parameters.AddWithValue("@keterangan", kt);
                objKomen.Parameters.AddWithValue("@tgl_penyusutan", UbahFormatDate(splitTampungDate[2], splitTampungDate[0], splitTampungDate[1]));
                int hasil = objKomen.ExecuteNonQuery();
                if (hasil > 0)
                {
                    MessageBox.Show("jurnal masuk");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        #endregion

        private void fmNeraca_Load(object sender, EventArgs e)
        {

        }
    }
}
