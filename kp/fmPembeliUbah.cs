﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmPembeliUbah : Form
    {
        MySqlConnection con = new MySqlConnection();
        public fmPembeliUbah()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =aneka_filter; uid=root;";
        }

        private void fmPembeliUbah_Load(object sender, EventArgs e)
        {
            groupBox1.Enabled = false;
            groupBox2.Enabled = true;
        }

        private void btnLihatData_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmPembeliData();
            fm1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtKodeUbah.Text != "")
            {
                PilihData();
            }
            else
            {
                MessageBox.Show("Isi Data!!!");
            }
        }
        #region Pilih Data ke textbox
        public void PilihData()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id, nama, alamat, no_telp FROM pembeli WHERE id='" + txtKodeUbah.Text + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                txtKode.Text = objReader.GetValue(0).ToString();
                txtNama.Text = objReader.GetValue(1).ToString();
                txtAlamat.Text = objReader.GetValue(2).ToString();
                txtTelp.Text = objReader.GetValue(3).ToString();
            }
            MessageBox.Show("Berhasil Pilih Data pembeli!!!");
            objReader.Close();
            con.Close();

            groupBox1.Enabled = true;
            groupBox2.Enabled = false;

        }

        #endregion

        private void btnbah_Click(object sender, EventArgs e)
        {
            UbahData();
        }
        #region Ubah Data
        public void UbahData()
        {
            if (txtKode.Text != "" && txtNama.Text != "" && txtAlamat.Text != "" && txtTelp.Text != "")
            {
                con.Open();

                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "UPDATE pembeli SET nama=@nama, alamat=@alamat, no_telp=@telp WHERE id='" + txtKodeUbah.Text + "'";
                objKomen.Parameters.AddWithValue("@nama", txtNama.Text);
                objKomen.Parameters.AddWithValue("@alamat", txtAlamat.Text);
                objKomen.Parameters.AddWithValue("@telp", txtTelp.Text);

                int hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {
                    MessageBox.Show("Berhasil Ubah Data pembeli!!!");
                    txtKode.Text = "";
                    txtNama.Text = "";
                    txtAlamat.Text = "";
                    txtTelp.Text = "";
                    txtKodeUbah.Text = "";
                    groupBox1.Enabled = false;
                    groupBox2.Enabled = true;
                }

                con.Close();
            }
            else
            {
                MessageBox.Show("Periksa Data!!!");
            }

        }

        #endregion

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }
    }
}
