﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmBarangTambah : Form
    {
        MySqlConnection con = new MySqlConnection();
        public fmBarangTambah()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =aneka_filter; uid=root;";

        }
        int cek = 0;
        private void fmBarangTambah_Load(object sender, EventArgs e)
        {
            //Jenis Barang
            cboJenis.Text = "Pilih Jenis Barang ...";
            cboJenis.Items.Add("Oil");
            cboJenis.Items.Add("Fuel");
            cboJenis.Items.Add("Hyd");
            cboJenis.Items.Add("Air");
            cboJenis.Items.Add("Pilot");
            //Jenis Satuan
            cboSatuan.Text = "Pilih Satuan ...";
            cboSatuan.Items.Add("Pcs");
            cboSatuan.Items.Add("Set");
            cboSatuan.Items.Add("Doz");
            cboSatuan.Items.Add("Unit");

        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            if (txtKode.Text != "" || txtNama.Text != "" && cboJenis.Text != "Pilih Jenis Barang ..." && txtMinStok.Text != "" && txtHarga.Text != "" && cboSatuan.Text != "Pilih Barang ...")
            {
                NamaSama();

                TambahBarang();
            }
            else
            {
                MessageBox.Show("Persiksa Data!!!");
            }

            
        }

        #region Cek Nama Barang
        public void NamaSama()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "Select nama From barang";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            string tampungTxtNama = txtNama.Text;
            List<string> namaTampung = new List<string>();

            while (objReader.Read() == true)
            {
                namaTampung.Add(objReader.GetValue(0).ToString());
            }

            for (int j = 0; j < namaTampung.Count; j++)
            {
                if (namaTampung[j] == tampungTxtNama)
                {
                    MessageBox.Show("Nama Sama!!!");
                    cek = 1;
                    j = namaTampung.Count;
                }
            }
            objReader.Close();
            con.Close();
        }
#endregion 

        #region Tambah Barang
        public void TambahBarang()
        {
            con.Open();
                if (cek == 0)
                {
                    MySqlCommand objKomen = new MySqlCommand();
                    objKomen.Connection = con;
                    objKomen.CommandType = CommandType.Text;
                    objKomen.CommandText = "Insert Into barang Values (@kode, @nama, @jenis, @status, @stok, @minstok, @satuan, @harga)";
                    objKomen.Parameters.AddWithValue("@kode", "Test");
                    objKomen.Parameters.AddWithValue("@nama", txtNama.Text);
                    objKomen.Parameters.AddWithValue("@jenis", cboJenis.Text);
                    objKomen.Parameters.AddWithValue("@status", "jual");
                    objKomen.Parameters.AddWithValue("@stok", 0);
                    objKomen.Parameters.AddWithValue("@minstok", txtMinStok.Text);
                    objKomen.Parameters.AddWithValue("@satuan", cboSatuan.Text);
                    objKomen.Parameters.AddWithValue("@harga", txtHarga.Text);

                    int hasil = objKomen.ExecuteNonQuery();

                    if (hasil > 0)
                    {
                        MessageBox.Show("Penambahan data yang Anda tambahkan berhasil!");
                        txtKode.Clear();
                        txtNama.Clear();
                        cboJenis.Text="Pilih Jenis ...";
                        txtMinStok.Clear();
                        cboSatuan.Text = "Pilih Jabatan ...";
                        txtHarga.Clear();
                    }
                    else
                    {
                        MessageBox.Show("Mohon maaf, Anda gagal menambahkan data");
                    }
                }

                con.Close();            
        }

        #endregion 

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }

    }
}
