﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmBarangData : Form
    {
        MySqlConnection con = new MySqlConnection();
        int i = 0;
        public fmBarangData()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =aneka_filter; uid=root;";
        }

        private void fmBarangData_Load(object sender, EventArgs e)
        {
            DataBarang();
            lblTotalData.Text += i +" Item";
        }

        #region Tampilkan Data Barang
        public void DataBarang()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id, nama, jenis, kuantiti, satuan_kuantiti, harga_satuan FROM barang Where hapus=0";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            dgvData.Rows.Clear();
            while (objReader.Read() == true)
            {
                i++;
                string kode = objReader.GetValue(0).ToString();
                string nama = objReader.GetValue(1).ToString();
                string jenis = objReader.GetValue(2).ToString();
                string kuantiti = objReader.GetValue(3).ToString();
                string satuan_kuantiti = objReader.GetValue(4).ToString();
                string harga_satuan = objReader.GetValue(5).ToString();
                dgvData.Rows.Add(kode, nama, jenis, kuantiti, satuan_kuantiti, harga_satuan);
            }
            objReader.Close();
            con.Close();
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }

        #endregion
    }
}
