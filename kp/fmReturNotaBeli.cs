﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmReturNotaBeli : Form
    {

        MySqlConnection con = new MySqlConnection();
        int i = 0;
        string ubahFormatBulan = "";
        string[] splitTampungDate;
        long totalHarga = 0;
        long totalharga_beli = 0;
        long terimaUang = 0;
        long tampungUbah = 0;
        int cekKuanti = 0;

        public fmReturNotaBeli()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";
        }

        private void cboNotaBeli_SelectedIndexChanged(object sender, EventArgs e)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id_barang, kuantiti,sub_harga FROM nota_beli_has_barang Where kode_nota_beli='" + cboNotaBeli.Text + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            int k = 0;
            while (objReader.Read() == true)
            {

                string kodett = objReader.GetValue(0).ToString();
                string qty = objReader.GetValue(1).ToString();
                string sub = objReader.GetValue(2).ToString();
                dgvBarangNota.Rows.Add(kodett, qty, sub);
                //dgvBarangNota.Rows.Add(kodeTampung, nama, harga_satuan);
                k++;
            }


            objReader.Close();
            con.Close();
        }

        private void txtKodeBarang_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnCetak_Click(object sender, EventArgs e)
        {
            int barisDgv = dgvbarang.RowCount;
            CekKuantitiBarang(barisDgv);
            SimpanNota();
            SimpanJurnal();
            SimpanBarang(barisDgv);
            SimpanAkunJurnal1();
            SimpanAkunJurnal2();
            cekdefault();
        }

        #region Method
        public void SimpanNota()
        {
            con.Open();
            MySqlCommand objKomen = new MySqlCommand();
            objKomen.Connection = con;
            objKomen.CommandType = CommandType.Text;
            objKomen.CommandText = "Insert Into nota_retur  Values (@id_nota_retur, @total_harga, @waktu_retur, @status)";
            objKomen.Parameters.AddWithValue("@id_nota_retur", txtNoNota.Text);
            objKomen.Parameters.AddWithValue("@total_harga", double.Parse(txtTotal.Text));
            objKomen.Parameters.AddWithValue("@waktu_retur", DateTime.Now);
            objKomen.Parameters.AddWithValue("@status", 0);
            int hasil = objKomen.ExecuteNonQuery();

            if (hasil > 0)
            {
                //MessageBox.Show("Penambahan data yang Anda tambahkan berhasil!");
                //con.Close();
                //NomorNota(ubahFormatBulan, splitTampungDate[2] );
                //con.Open();
                //cboNamaPembeli.Text = "Pilih Pembeli ...";
                //cboCaraBayar.Text = "Pilih Cara Bayar ...";
                //txtTotal.Clear();
                //txtUangKembali.Clear();
                //txtTerimaUang.Clear();
            }
            con.Close();

        }

        public void SimpanBarang(int baris)
        {
            try
            {
                con.Open();

                int hasil = 0;
                int hasilUpdate = 0;
                for (int a = 0; a < baris - 1; a++)
                {
                    MySqlCommand objKomen = new MySqlCommand();
                    objKomen.Connection = con;
                    objKomen.CommandType = CommandType.Text;
                    objKomen.CommandText = "Insert Into nota_retur_has_barang Values (@kode_nota, @id_barang, @kuantiti, @sub_harga)";
                    objKomen.Parameters.AddWithValue("@kode_nota", txtNoNota.Text);
                    objKomen.Parameters.AddWithValue("@id_barang", dgvbarang.Rows[a].Cells[0].Value.ToString());
                    objKomen.Parameters.AddWithValue("@kuantiti", Convert.ToInt64(dgvbarang.Rows[a].Cells[3].Value));
                    objKomen.Parameters.AddWithValue("@sub_harga", Convert.ToInt64(dgvbarang.Rows[a].Cells[4].Value));
                    hasil = objKomen.ExecuteNonQuery();

                    //Ambil Data kuantiti barang yang akan ditambahkan
                    MySqlCommand objData = new MySqlCommand();
                    objData.Connection = con;
                    objData.CommandType = CommandType.Text;
                    objData.CommandText = "SELECT kuantiti,harga_satuan FROM barang Where hapus=0 and id='" + dgvbarang.Rows[a].Cells[0].Value.ToString() + "'";
                    MySqlDataReader objReader;
                    objReader = objData.ExecuteReader();

                    int kuantiti = 0;
                    if (objReader.Read() == true)
                    {
                        kuantiti = Convert.ToInt16(objReader.GetValue(0).ToString());
                        kuantiti -= Convert.ToInt16(dgvbarang.Rows[a].Cells[3].Value);

                        //Update Barang
                        MySqlCommand objKomenUpdate = new MySqlCommand();
                        objKomenUpdate.Connection = con;
                        objKomenUpdate.CommandType = CommandType.Text;

                        objKomenUpdate.CommandText = "UPDATE barang SET kuantiti=@stokUpdate WHERE id='" + dgvbarang.Rows[a].Cells[0].Value.ToString() + "'";
                        objKomenUpdate.Parameters.AddWithValue("@stokUpdate", kuantiti);

                        objReader.Close();

                        hasilUpdate = objKomenUpdate.ExecuteNonQuery();
                        ////MessageBox.Show("" + hasilUpdate);
                    }
                }

                if (hasil > 0)
                {
                    MessageBox.Show("Penambahan data yang Anda tambahkan berhasil!");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void ambilNotaBeli()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT kode FROM nota_beli ";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                string nama = objReader.GetValue(0).ToString();
                cboNotaBeli.Items.Add(nama);
            }
            objReader.Close();
            con.Close();
        }

        private void fmReturNotaBeli_Load(object sender, EventArgs e)
        {
            string tampungDate = DateTime.Now.ToShortDateString();
            splitTampungDate = tampungDate.Split('/');
            if (splitTampungDate[0] == "1")
                ubahFormatBulan = "I";
            else if (splitTampungDate[0] == "2")
                ubahFormatBulan = "II";
            else if (splitTampungDate[0] == "3")
                ubahFormatBulan = "III";
            else if (splitTampungDate[0] == "4")
                ubahFormatBulan = "IV";
            else if (splitTampungDate[0] == "5")
                ubahFormatBulan = "V";
            else if (splitTampungDate[0] == "6")
                ubahFormatBulan = "VI";
            else if (splitTampungDate[0] == "7")
                ubahFormatBulan = "VII";
            else if (splitTampungDate[0] == "8")
                ubahFormatBulan = "VIII";
            else if (splitTampungDate[0] == "9")
                ubahFormatBulan = "IX";
            else if (splitTampungDate[0] == "10")
                ubahFormatBulan = "X";
            else if (splitTampungDate[0] == "11")
                ubahFormatBulan = "XI";
            else if (splitTampungDate[0] == "12")
                ubahFormatBulan = "XII";
            NomorNota(ubahFormatBulan, splitTampungDate[2]);
            ambilDataNotaBeli();
        }

        public void ambilDataNotaBeli()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT kode FROM nota_beli ";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                string nama = objReader.GetValue(0).ToString();
                cboNotaBeli.Items.Add(nama);
            }
            objReader.Close();
            con.Close();
        }

        public void NomorNota(string bulan, String Tahun)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT Count(*) FROM nota_retur Where status=0";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                int ubahDataInt = Convert.ToInt16(objReader.GetValue(0)) + 1;
                string data = ubahDataInt.ToString();
                string kodeNota = "RB " + data + "/" + bulan + "/" + Tahun;
                txtNoNota.Text = kodeNota;
                //MessageBox.Show(data+"/"+bulan+"/"+Tahun);
            }
            objReader.Close();
            con.Close();
        }

        public string UbahFormatDate(string tahun, string bulan, string tanggal)
        {
            string a = tahun + "-" + bulan + "-" + tanggal;
            return a;
        }

        #region simpan data
        public void CekData(string kode, int indexBaris)
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id, nama, kuantiti, harga_satuan FROM barang Where hapus=0 and id='" + kode + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            if (objReader.Read() == true)
            {
                i++;
                string kodeTampung = objReader.GetValue(0).ToString();
                string nama = objReader.GetValue(1).ToString();
                string kuantiti = objReader.GetValue(2).ToString();
                string harga_satuan = objReader.GetValue(3).ToString();
                dgvbarang.Rows[indexBaris].Cells[0].Value = kodeTampung;
                dgvbarang.Rows[indexBaris].Cells[1].Value = nama;
                dgvbarang.Rows[indexBaris].Cells[2].Value = harga_satuan;
                //dgvBarangNota.Rows.Add(kodeTampung, nama, harga_satuan);
            }
            else
            {
                MessageBox.Show("Kode Barang Tidak Ada!!!");
                dgvbarang.Rows.RemoveAt(indexBaris);
            }

            objReader.Close();
            con.Close();
        }

        public int AmbilIdJurnal()
        {
            int id = 0;
            con.Open();
            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT id_jurnal FROM jurnal WHERE no_bukti='" + txtNoNota.Text + "'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                id = Convert.ToInt16(objReader.GetValue(0));
            }
            objReader.Close();
            con.Close();
            return id;
        }

        public void SimpanJurnal()
        {


            String kt = " Penjualan barang - retur barang pembelian dengan jumlah biaya " + totalHarga.ToString();
            try
            {
                con.Open();
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text;
                objKomen.CommandText = "Insert Into jurnal Values (@id, @jenis, @no_bukti, @keterangan, @tanggal)";
                objKomen.Parameters.AddWithValue("@id", "");
                objKomen.Parameters.AddWithValue("@jenis", "umum");
                objKomen.Parameters.AddWithValue("@no_bukti", txtNoNota.Text);
                objKomen.Parameters.AddWithValue("@keterangan", kt);
                objKomen.Parameters.AddWithValue("@tanggal", DateTime.Now);
                int hasil = objKomen.ExecuteNonQuery();
                if (hasil > 0)
                {
                    MessageBox.Show("jurnal masuk");
                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void SimpanAkunJurnal1()
        {
            int id = AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun1, @id_jurnal1, @urutan1, @debet1, @kredit1)";
                objKomen.Parameters.AddWithValue("@no_akun1", 10040);
                objKomen.Parameters.AddWithValue("@id_jurnal1", id);
                objKomen.Parameters.AddWithValue("@urutan1", 1);
                objKomen.Parameters.AddWithValue("@debet1", totalHarga);
                objKomen.Parameters.AddWithValue("@kredit1", 0);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {

                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public void SimpanAkunJurnal2()
        {
            int id = AmbilIdJurnal();
            try
            {
                con.Open();

                int hasil = 0;
                MySqlCommand objKomen = new MySqlCommand();
                objKomen.Connection = con;
                objKomen.CommandType = CommandType.Text; ;
                objKomen.CommandText = "Insert Into akun_has_jurnal Values (@no_akun2, @id_jurnal2, @urutan2, @debet2, @kredit2)";
                objKomen.Parameters.AddWithValue("@no_akun2", 10070);
                objKomen.Parameters.AddWithValue("@id_jurnal2", id);
                objKomen.Parameters.AddWithValue("@urutan2", 2);
                objKomen.Parameters.AddWithValue("@debet2", 0);
                objKomen.Parameters.AddWithValue("@kredit2", totalHarga);
                hasil = objKomen.ExecuteNonQuery();

                if (hasil > 0)
                {

                }
                con.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        
        #endregion

        #region simpan barang
        public void CekKuantitiBarang(int barisDgv)
        {
            con.Open();
            cekKuanti = 0;
            for (int a = 0; a < barisDgv - 1; a++)
            {
                //Ambil Data kuantiti barang yang akan ditambahkan
                MySqlCommand objData = new MySqlCommand();
                objData.Connection = con;
                objData.CommandType = CommandType.Text;
                objData.CommandText = "SELECT kuantiti FROM barang Where hapus=0 and id='" + dgvbarang.Rows[a].Cells[0].Value.ToString() + "'";
                MySqlDataReader objReader;
                objReader = objData.ExecuteReader();

                int kuantiti = 0;
                if (objReader.Read() == true)
                {
                    kuantiti = Convert.ToInt16(objReader.GetValue(0).ToString());
                    if (kuantiti >= Convert.ToInt16(dgvbarang.Rows[a].Cells[3].Value))
                    {
                        cekKuanti += 1;
                    }
                    else
                    {
                        MessageBox.Show("Stok " + dgvbarang.Rows[a].Cells[1].Value + " Di Dalam Toko: " + kuantiti + "\nStok Yang Ingin Dibeli :" + dgvbarang.Rows[a].Cells[3].Value + "\nStok Kurang " + (Convert.ToInt16(dgvbarang.Rows[a].Cells[3].Value) - kuantiti) + "!!!");
                        tampungUbah = 0;
                        totalHarga = 0;
                        terimaUang = 0;
                        txtTotal.Text = totalHarga.ToString();
                    }
                }
                objReader.Close();
            }
            con.Close();
        }

        #endregion

        public void cekdefault()
        {
            NomorNota(ubahFormatBulan, splitTampungDate[2]);
            cboNotaBeli.Text = "";

        }

        #endregion

        private void dgvBarangNota_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvbarang_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            #region Isi Data
            try
            {
                int rowIndex = e.RowIndex;
                int columnIndex = e.ColumnIndex;

                string cek = dgvbarang.Rows[rowIndex].Cells[0].Value.ToString();
                CekData(cek, rowIndex);
                if (dgvbarang.Rows[rowIndex].Cells[3].Value != null)
                {
                    totalHarga -= tampungUbah;
                    try
                    {
                        long hitungSub = Convert.ToInt64(dgvbarang.Rows[rowIndex].Cells[2].Value) * Convert.ToInt64(dgvbarang.Rows[rowIndex].Cells[3].Value);
                        dgvbarang.Rows[rowIndex].Cells[4].Value = hitungSub;

                    }
                    catch
                    {
                        dgvbarang.Rows[rowIndex].Cells[3].Value = 0;
                        dgvbarang.Rows[rowIndex].Cells[4].Value = 0;
                    }
                    totalHarga += Convert.ToInt64(dgvbarang.Rows[rowIndex].Cells[4].Value);
                    txtTotal.Text = totalHarga.ToString();
                }
                else
                {
                    long hitungSub = Convert.ToInt64(dgvbarang.Rows[rowIndex].Cells[2].Value) * Convert.ToInt64(dgvbarang.Rows[rowIndex].Cells[3].Value);
                    dgvbarang.Rows[rowIndex].Cells[4].Value = hitungSub;
                    totalHarga += Convert.ToInt64(dgvbarang.Rows[rowIndex].Cells[4].Value);
                    txtTotal.Text = totalHarga.ToString();
                }
            }
            catch
            {
                MessageBox.Show("Isi Kode Barang !!!");
            }
            #endregion

        }
       
    }
}
