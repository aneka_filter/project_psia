﻿namespace kp
{
    partial class fmLaporanLabaRugi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fmLaporanLabaRugi));
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.lblTotalPendapatan = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblReturPenjualan = new System.Windows.Forms.Label();
            this.lblPenjualanBarang = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.lblBiayaAdmin = new System.Windows.Forms.Label();
            this.lblBiayaBunga = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lblBiayaSewa = new System.Windows.Forms.Label();
            this.lblBiayaKartuKredit = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lblTotalBiaya = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblGajiTransportasi = new System.Windows.Forms.Label();
            this.lblGajiKaryawan = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblLabaRugi = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btnPindahFormEkuitas = new System.Windows.Forms.Button();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.btnCetak = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(174, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(239, 30);
            this.label1.TabIndex = 68;
            this.label1.Text = "LAPORAN LABA RUGI";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(149, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 20);
            this.label5.TabIndex = 70;
            this.label5.Text = "Penjualan Barang :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.lblTotalPendapatan);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.lblReturPenjualan);
            this.groupBox1.Controls.Add(this.lblPenjualanBarang);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(55, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(506, 116);
            this.groupBox1.TabIndex = 72;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pendapatan";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(56, 71);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(375, 20);
            this.label33.TabIndex = 94;
            this.label33.Text = "-------------------------------------------------------------";
            // 
            // lblTotalPendapatan
            // 
            this.lblTotalPendapatan.AutoSize = true;
            this.lblTotalPendapatan.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalPendapatan.Location = new System.Drawing.Point(325, 90);
            this.lblTotalPendapatan.Name = "lblTotalPendapatan";
            this.lblTotalPendapatan.Size = new System.Drawing.Size(44, 20);
            this.lblTotalPendapatan.TabIndex = 80;
            this.lblTotalPendapatan.Text = "Uang";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(290, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 20);
            this.label7.TabIndex = 79;
            this.label7.Text = "Rp.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(146, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(138, 20);
            this.label8.TabIndex = 78;
            this.label8.Text = "Total Pendapatan :";
            // 
            // lblReturPenjualan
            // 
            this.lblReturPenjualan.AutoSize = true;
            this.lblReturPenjualan.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReturPenjualan.Location = new System.Drawing.Point(325, 51);
            this.lblReturPenjualan.Name = "lblReturPenjualan";
            this.lblReturPenjualan.Size = new System.Drawing.Size(44, 20);
            this.lblReturPenjualan.TabIndex = 77;
            this.lblReturPenjualan.Text = "Uang";
            // 
            // lblPenjualanBarang
            // 
            this.lblPenjualanBarang.AutoSize = true;
            this.lblPenjualanBarang.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPenjualanBarang.Location = new System.Drawing.Point(325, 22);
            this.lblPenjualanBarang.Name = "lblPenjualanBarang";
            this.lblPenjualanBarang.Size = new System.Drawing.Size(44, 20);
            this.lblPenjualanBarang.TabIndex = 76;
            this.lblPenjualanBarang.Text = "Uang";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(290, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 20);
            this.label4.TabIndex = 73;
            this.label4.Text = "Rp.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(290, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 20);
            this.label3.TabIndex = 72;
            this.label3.Text = "Rp.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(264, 20);
            this.label2.TabIndex = 71;
            this.label2.Text = "Retur Penjualan & Penyesuaian Harga :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.lblBiayaAdmin);
            this.groupBox2.Controls.Add(this.lblBiayaBunga);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.lblBiayaSewa);
            this.groupBox2.Controls.Add(this.lblBiayaKartuKredit);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.lblTotalBiaya);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.lblGajiTransportasi);
            this.groupBox2.Controls.Add(this.lblGajiKaryawan);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Location = new System.Drawing.Point(55, 217);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(506, 230);
            this.groupBox2.TabIndex = 81;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Biaya";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(56, 189);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(375, 20);
            this.label32.TabIndex = 93;
            this.label32.Text = "-------------------------------------------------------------";
            // 
            // lblBiayaAdmin
            // 
            this.lblBiayaAdmin.AutoSize = true;
            this.lblBiayaAdmin.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBiayaAdmin.Location = new System.Drawing.Point(325, 163);
            this.lblBiayaAdmin.Name = "lblBiayaAdmin";
            this.lblBiayaAdmin.Size = new System.Drawing.Size(44, 20);
            this.lblBiayaAdmin.TabIndex = 92;
            this.lblBiayaAdmin.Text = "Uang";
            // 
            // lblBiayaBunga
            // 
            this.lblBiayaBunga.AutoSize = true;
            this.lblBiayaBunga.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBiayaBunga.Location = new System.Drawing.Point(325, 134);
            this.lblBiayaBunga.Name = "lblBiayaBunga";
            this.lblBiayaBunga.Size = new System.Drawing.Size(44, 20);
            this.lblBiayaBunga.TabIndex = 91;
            this.lblBiayaBunga.Text = "Uang";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(290, 163);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(29, 20);
            this.label28.TabIndex = 90;
            this.label28.Text = "Rp.";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(290, 134);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 20);
            this.label29.TabIndex = 89;
            this.label29.Text = "Rp.";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(73, 163);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(211, 20);
            this.label30.TabIndex = 88;
            this.label30.Text = "Biaya Administrasi Lain-Lain :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(185, 134);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(99, 20);
            this.label31.TabIndex = 87;
            this.label31.Text = "Biaya Bunga :";
            // 
            // lblBiayaSewa
            // 
            this.lblBiayaSewa.AutoSize = true;
            this.lblBiayaSewa.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBiayaSewa.Location = new System.Drawing.Point(325, 104);
            this.lblBiayaSewa.Name = "lblBiayaSewa";
            this.lblBiayaSewa.Size = new System.Drawing.Size(44, 20);
            this.lblBiayaSewa.TabIndex = 86;
            this.lblBiayaSewa.Text = "Uang";
            // 
            // lblBiayaKartuKredit
            // 
            this.lblBiayaKartuKredit.AutoSize = true;
            this.lblBiayaKartuKredit.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBiayaKartuKredit.Location = new System.Drawing.Point(325, 75);
            this.lblBiayaKartuKredit.Name = "lblBiayaKartuKredit";
            this.lblBiayaKartuKredit.Size = new System.Drawing.Size(44, 20);
            this.lblBiayaKartuKredit.TabIndex = 85;
            this.lblBiayaKartuKredit.Text = "Uang";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(290, 104);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(29, 20);
            this.label22.TabIndex = 84;
            this.label22.Text = "Rp.";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(290, 75);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 20);
            this.label23.TabIndex = 83;
            this.label23.Text = "Rp.";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(189, 104);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(95, 20);
            this.label24.TabIndex = 82;
            this.label24.Text = "Biaya Sewa :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(139, 75);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(145, 20);
            this.label25.TabIndex = 81;
            this.label25.Text = "Biaya Kartu Kredit :";
            // 
            // lblTotalBiaya
            // 
            this.lblTotalBiaya.AutoSize = true;
            this.lblTotalBiaya.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalBiaya.Location = new System.Drawing.Point(325, 205);
            this.lblTotalBiaya.Name = "lblTotalBiaya";
            this.lblTotalBiaya.Size = new System.Drawing.Size(44, 20);
            this.lblTotalBiaya.TabIndex = 80;
            this.lblTotalBiaya.Text = "Uang";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(290, 205);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 20);
            this.label9.TabIndex = 79;
            this.label9.Text = "Rp.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(189, 205);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 20);
            this.label10.TabIndex = 78;
            this.label10.Text = "Total Biaya :";
            // 
            // lblGajiTransportasi
            // 
            this.lblGajiTransportasi.AutoSize = true;
            this.lblGajiTransportasi.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGajiTransportasi.Location = new System.Drawing.Point(325, 47);
            this.lblGajiTransportasi.Name = "lblGajiTransportasi";
            this.lblGajiTransportasi.Size = new System.Drawing.Size(44, 20);
            this.lblGajiTransportasi.TabIndex = 77;
            this.lblGajiTransportasi.Text = "Uang";
            // 
            // lblGajiKaryawan
            // 
            this.lblGajiKaryawan.AutoSize = true;
            this.lblGajiKaryawan.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGajiKaryawan.Location = new System.Drawing.Point(325, 18);
            this.lblGajiKaryawan.Name = "lblGajiKaryawan";
            this.lblGajiKaryawan.Size = new System.Drawing.Size(44, 20);
            this.lblGajiKaryawan.TabIndex = 76;
            this.lblGajiKaryawan.Text = "Uang";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(290, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 20);
            this.label13.TabIndex = 73;
            this.label13.Text = "Rp.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(290, 18);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 20);
            this.label14.TabIndex = 72;
            this.label14.Text = "Rp.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(137, 47);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(147, 20);
            this.label15.TabIndex = 71;
            this.label15.Text = "Biaya Transportasi :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(127, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(157, 20);
            this.label16.TabIndex = 70;
            this.label16.Text = "Biaya Gaji Karyawan :";
            // 
            // lblLabaRugi
            // 
            this.lblLabaRugi.AutoSize = true;
            this.lblLabaRugi.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLabaRugi.Location = new System.Drawing.Point(380, 456);
            this.lblLabaRugi.Name = "lblLabaRugi";
            this.lblLabaRugi.Size = new System.Drawing.Size(44, 20);
            this.lblLabaRugi.TabIndex = 83;
            this.lblLabaRugi.Text = "Uang";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(345, 456);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 20);
            this.label18.TabIndex = 82;
            this.label18.Text = "Rp.";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(151, 456);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(188, 20);
            this.label19.TabIndex = 81;
            this.label19.Text = "Laba Rugi Sebelum Pajak :";
            // 
            // btnPindahFormEkuitas
            // 
            this.btnPindahFormEkuitas.Location = new System.Drawing.Point(538, 12);
            this.btnPindahFormEkuitas.Name = "btnPindahFormEkuitas";
            this.btnPindahFormEkuitas.Size = new System.Drawing.Size(75, 23);
            this.btnPindahFormEkuitas.TabIndex = 84;
            this.btnPindahFormEkuitas.Text = "Ekuitas";
            this.btnPindahFormEkuitas.UseVisualStyleBackColor = true;
            this.btnPindahFormEkuitas.Click += new System.EventHandler(this.btnPindahFormEkuitas_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // btnCetak
            // 
            this.btnCetak.Location = new System.Drawing.Point(486, 456);
            this.btnCetak.Name = "btnCetak";
            this.btnCetak.Size = new System.Drawing.Size(75, 23);
            this.btnCetak.TabIndex = 94;
            this.btnCetak.Text = "Cetak";
            this.btnCetak.UseVisualStyleBackColor = true;
            this.btnCetak.Click += new System.EventHandler(this.btnCetak_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // fmLaporanLabaRugi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 485);
            this.Controls.Add(this.btnCetak);
            this.Controls.Add(this.btnPindahFormEkuitas);
            this.Controls.Add(this.lblLabaRugi);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "fmLaporanLabaRugi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fmLaporanLabaRugi";
            this.Load += new System.EventHandler(this.fmLaporanLabaRugi_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label lblTotalPendapatan;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblReturPenjualan;
        private System.Windows.Forms.Label lblPenjualanBarang;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label lblBiayaAdmin;
        private System.Windows.Forms.Label lblBiayaBunga;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label lblBiayaSewa;
        private System.Windows.Forms.Label lblBiayaKartuKredit;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblTotalBiaya;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblGajiTransportasi;
        private System.Windows.Forms.Label lblGajiKaryawan;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblLabaRugi;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnPindahFormEkuitas;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Button btnCetak;
        private System.Drawing.Printing.PrintDocument printDocument1;
    }
}