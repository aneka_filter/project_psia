﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmLaporanLabaRugi : Form
    {
        MySqlConnection con = new MySqlConnection();
        int labarugi = 0;
        int pendapatan = 0;
        int biaya = 0;
        public fmLaporanLabaRugi()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";
        }

        private void fmLaporanLabaRugi_Load(object sender, EventArgs e)
        {
            HitungNilaiPendapatan();
            HitungNilaiBiaya();
            labarugi = pendapatan - biaya;
            if(labarugi>=0)
            {
                lblLabaRugi.Text = labarugi.ToString();
            }
            else
            {
                lblLabaRugi.Text = "-("+(labarugi*-1).ToString()+")";
            }
            
        }

        public void HitungNilaiPendapatan()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT aj.debet, aj.kredit FROM akun_has_jurnal aj INNER JOIN jurnal j ON aj.id_jurnal = j.id_jurnal Where aj.no_akun=40010 AND j.jenis = 'umum'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            int debet = 0;
            int kredit = 0;

            int totalPenjualanBarang = 0;
            while (objReader.Read() == true)
            {
                debet = Convert.ToInt32(objReader.GetValue(0));
                kredit = Convert.ToInt32(objReader.GetValue(1));

                totalPenjualanBarang += (kredit - debet);
            }
            objReader.Close();
            lblPenjualanBarang.Text = totalPenjualanBarang.ToString();

            objData.CommandText = "SELECT aj.debet, aj.kredit FROM akun_has_jurnal aj INNER JOIN jurnal j ON aj.id_jurnal = j.id_jurnal Where aj.no_akun=40020 AND j.jenis = 'umum'";
            objReader = objData.ExecuteReader();

            debet = 0;
            kredit = 0;

            int totalReturPenjualan = 0;

            while (objReader.Read() == true)
            {
                debet = Convert.ToInt32(objReader.GetValue(0));
                kredit = Convert.ToInt32(objReader.GetValue(1));
                totalReturPenjualan += (debet - kredit);
            }
            lblReturPenjualan.Text = "-("+totalReturPenjualan.ToString()+")";
            pendapatan = totalPenjualanBarang - totalReturPenjualan;
            if (pendapatan >= 0)
            {
                lblTotalPendapatan.Text = pendapatan.ToString();
            }
            else
            {
                lblTotalPendapatan.Text = "-("+ pendapatan.ToString()+")"; 
            }
            

            objReader.Close();
            con.Close();
        }

        public void HitungNilaiBiaya()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT aj.debet, aj.kredit FROM akun_has_jurnal aj INNER JOIN jurnal j ON aj.id_jurnal = j.id_jurnal Where aj.no_akun=50020 AND j.jenis = 'umum'";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            int debet = 0;
            int kredit = 0;

            int totalBiayaGaji = 0;
            while (objReader.Read() == true)
            {
                debet = Convert.ToInt16(objReader.GetValue(0));
                kredit = Convert.ToInt16(objReader.GetValue(1));
                totalBiayaGaji += (debet - kredit);
            }
            objReader.Close();
            lblGajiKaryawan.Text = totalBiayaGaji.ToString();

            objData.CommandText = "SELECT aj.debet, aj.kredit FROM akun_has_jurnal aj INNER JOIN jurnal j ON aj.id_jurnal = j.id_jurnal Where aj.no_akun=50030 AND j.jenis = 'umum'";
            objReader = objData.ExecuteReader();

            debet = 0;
            kredit = 0;

            int totalBiayaTransport = 0;

            while (objReader.Read() == true)
            {
                debet = Convert.ToInt16(objReader.GetValue(0));
                kredit = Convert.ToInt16(objReader.GetValue(1));
                totalBiayaTransport += (debet-kredit);
            }
            objReader.Close();
            lblGajiTransportasi.Text =  totalBiayaTransport.ToString();

            objData.CommandText = "SELECT aj.debet, aj.kredit FROM akun_has_jurnal aj INNER JOIN jurnal j ON aj.id_jurnal = j.id_jurnal Where aj.no_akun=50040 AND j.jenis = 'umum'";
            objReader = objData.ExecuteReader();

            debet = 0;
            kredit = 0;

            int totalBiayaKartu = 0;

            while (objReader.Read() == true)
            {
                debet = Convert.ToInt16(objReader.GetValue(0));
                kredit = Convert.ToInt16(objReader.GetValue(1));
                totalBiayaKartu += (debet-kredit);
            }
            objReader.Close();
            lblBiayaKartuKredit.Text = totalBiayaKartu.ToString();

            objData.CommandText = "SELECT aj.debet, aj.kredit FROM akun_has_jurnal aj INNER JOIN jurnal j ON aj.id_jurnal = j.id_jurnal Where aj.no_akun=50050 AND j.jenis = 'umum'";
            objReader = objData.ExecuteReader();

            debet = 0;
            kredit = 0;

            int totalBiayaSewa = 0;

            while (objReader.Read() == true)
            {
                debet = Convert.ToInt16(objReader.GetValue(0));
                kredit = Convert.ToInt16(objReader.GetValue(1));
                totalBiayaSewa += (debet-kredit);
            }
            objReader.Close();
            lblBiayaSewa.Text = totalBiayaSewa.ToString();

            objData.CommandText = "SELECT aj.debet, aj.kredit FROM akun_has_jurnal aj INNER JOIN jurnal j ON aj.id_jurnal = j.id_jurnal Where aj.no_akun=50060 AND j.jenis = 'umum'";
            objReader = objData.ExecuteReader();

            debet = 0;
            kredit = 0;

            int totalBiayaBunga = 0;

            while (objReader.Read() == true)
            {
                debet = Convert.ToInt16(objReader.GetValue(0));
                kredit = Convert.ToInt16(objReader.GetValue(1));
                totalBiayaBunga += (debet-kredit);
            }
            objReader.Close();
            lblBiayaBunga.Text = totalBiayaBunga.ToString();

            objData.CommandText = "SELECT aj.debet, aj.kredit FROM akun_has_jurnal aj INNER JOIN jurnal j ON aj.id_jurnal = j.id_jurnal Where aj.no_akun=50100 AND j.jenis = 'umum'";
            objReader = objData.ExecuteReader();

            debet = 0;
            kredit = 0;

            int totalBiayaLain = 0;

            while (objReader.Read() == true)
            {
                debet = Convert.ToInt16(objReader.GetValue(0));
                kredit = Convert.ToInt16(objReader.GetValue(1));
                totalBiayaLain += (debet-kredit);
            }
            objReader.Close();
            lblBiayaAdmin.Text = totalBiayaLain.ToString();
            biaya = totalBiayaGaji + totalBiayaTransport + totalBiayaSewa + totalBiayaKartu + totalBiayaBunga + totalBiayaLain;
            if (biaya >= 0)
            {
                lblTotalBiaya.Text = biaya.ToString();
            }
            else
            {
                lblTotalBiaya.Text = "-(" + biaya.ToString() + ")";
            }

            con.Close();
        }

        private void btnPindahFormEkuitas_Click(object sender, EventArgs e)
        {
            Form fm1 = new fmLaporanEkuitas(labarugi);
            //this.Close();
            fm1.Show();            
        }

        Bitmap bmp;

        private void btnCetak_Click(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            bmp = new Bitmap(this.Size.Width, this.Size.Height, g);
            Graphics ng = Graphics.FromImage(bmp);
            ng.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, this.Size);
            printPreviewDialog1.ShowDialog();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(bmp, 0, 0);
        }

    }
}
