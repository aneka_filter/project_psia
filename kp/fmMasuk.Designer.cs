﻿namespace kp
{
    partial class fmMasuk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblNamaMasuk = new System.Windows.Forms.Label();
            this.lblKataSandi = new System.Windows.Forms.Label();
            this.txtNamaMasuk = new System.Windows.Forms.TextBox();
            this.txtKataSandi = new System.Windows.Forms.TextBox();
            this.btnMasuk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(381, 51);
            this.label1.TabIndex = 0;
            this.label1.Text = "PT. ANEKA FILTER";
            // 
            // lblNamaMasuk
            // 
            this.lblNamaMasuk.AutoSize = true;
            this.lblNamaMasuk.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNamaMasuk.Location = new System.Drawing.Point(82, 81);
            this.lblNamaMasuk.Name = "lblNamaMasuk";
            this.lblNamaMasuk.Size = new System.Drawing.Size(209, 38);
            this.lblNamaMasuk.TabIndex = 0;
            this.lblNamaMasuk.Text = "Nama Pengguna";
            this.lblNamaMasuk.EnabledChanged += new System.EventHandler(this.lblNamaMasuk_EnabledChanged);
            this.lblNamaMasuk.TabIndexChanged += new System.EventHandler(this.lblNamaMasuk_TabIndexChanged);
            this.lblNamaMasuk.TextChanged += new System.EventHandler(this.lblNamaMasuk_TextChanged);
            this.lblNamaMasuk.Click += new System.EventHandler(this.lblNamaMasuk_Click);
            this.lblNamaMasuk.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.lblNamaMasuk_PreviewKeyDown);
            // 
            // lblKataSandi
            // 
            this.lblKataSandi.AutoSize = true;
            this.lblKataSandi.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKataSandi.Location = new System.Drawing.Point(109, 160);
            this.lblKataSandi.Name = "lblKataSandi";
            this.lblKataSandi.Size = new System.Drawing.Size(153, 38);
            this.lblKataSandi.TabIndex = 0;
            this.lblKataSandi.Text = "Kata Sandi";
            // 
            // txtNamaMasuk
            // 
            this.txtNamaMasuk.BackColor = System.Drawing.SystemColors.Control;
            this.txtNamaMasuk.Location = new System.Drawing.Point(101, 123);
            this.txtNamaMasuk.Name = "txtNamaMasuk";
            this.txtNamaMasuk.Size = new System.Drawing.Size(161, 20);
            this.txtNamaMasuk.TabIndex = 1;
            // 
            // txtKataSandi
            // 
            this.txtKataSandi.BackColor = System.Drawing.SystemColors.Control;
            this.txtKataSandi.Location = new System.Drawing.Point(101, 201);
            this.txtKataSandi.Name = "txtKataSandi";
            this.txtKataSandi.Size = new System.Drawing.Size(161, 20);
            this.txtKataSandi.TabIndex = 2;
            // 
            // btnMasuk
            // 
            this.btnMasuk.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMasuk.Location = new System.Drawing.Point(126, 257);
            this.btnMasuk.Name = "btnMasuk";
            this.btnMasuk.Size = new System.Drawing.Size(109, 49);
            this.btnMasuk.TabIndex = 3;
            this.btnMasuk.Text = "Masuk";
            this.btnMasuk.UseVisualStyleBackColor = true;
            this.btnMasuk.Click += new System.EventHandler(this.btnMasuk_Click);
            // 
            // fmMasuk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 337);
            this.Controls.Add(this.btnMasuk);
            this.Controls.Add(this.txtKataSandi);
            this.Controls.Add(this.txtNamaMasuk);
            this.Controls.Add(this.lblKataSandi);
            this.Controls.Add(this.lblNamaMasuk);
            this.Controls.Add(this.label1);
            this.Name = "fmMasuk";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Masuk";
            this.Load += new System.EventHandler(this.fmMasuk_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNamaMasuk;
        private System.Windows.Forms.Label lblKataSandi;
        private System.Windows.Forms.TextBox txtNamaMasuk;
        private System.Windows.Forms.TextBox txtKataSandi;
        private System.Windows.Forms.Button btnMasuk;
    }
}

