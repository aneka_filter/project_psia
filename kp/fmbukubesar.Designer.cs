﻿namespace kp
{
    partial class fmbukubesar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lbl_jenisakun = new System.Windows.Forms.Label();
            this.cbo_jenisakun = new System.Windows.Forms.ComboBox();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.txtTanggal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtKeterangan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDebet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtKredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtsaldodebet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtsaldokredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNoBukti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSebelum = new System.Windows.Forms.Label();
            this.lblSetelah = new System.Windows.Forms.Label();
            this.btn_tutup = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_jenisakun
            // 
            this.lbl_jenisakun.AutoSize = true;
            this.lbl_jenisakun.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_jenisakun.Location = new System.Drawing.Point(7, 15);
            this.lbl_jenisakun.Name = "lbl_jenisakun";
            this.lbl_jenisakun.Size = new System.Drawing.Size(94, 20);
            this.lbl_jenisakun.TabIndex = 145;
            this.lbl_jenisakun.Text = "Jenis Akun :";
            // 
            // cbo_jenisakun
            // 
            this.cbo_jenisakun.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_jenisakun.FormattingEnabled = true;
            this.cbo_jenisakun.Location = new System.Drawing.Point(107, 12);
            this.cbo_jenisakun.Name = "cbo_jenisakun";
            this.cbo_jenisakun.Size = new System.Drawing.Size(241, 28);
            this.cbo_jenisakun.TabIndex = 144;
            this.cbo_jenisakun.SelectedIndexChanged += new System.EventHandler(this.cbo_jenisakun_SelectedIndexChanged);
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToResizeColumns = false;
            this.dgvData.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtTanggal,
            this.txtKeterangan,
            this.txtDebet,
            this.txtKredit,
            this.txtsaldodebet,
            this.txtsaldokredit,
            this.txtNoBukti});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvData.Location = new System.Drawing.Point(12, 46);
            this.dgvData.Name = "dgvData";
            this.dgvData.Size = new System.Drawing.Size(700, 367);
            this.dgvData.TabIndex = 146;
            // 
            // txtTanggal
            // 
            this.txtTanggal.HeaderText = "Tanggal";
            this.txtTanggal.Name = "txtTanggal";
            this.txtTanggal.Width = 80;
            // 
            // txtKeterangan
            // 
            this.txtKeterangan.HeaderText = "Keterangan";
            this.txtKeterangan.Name = "txtKeterangan";
            this.txtKeterangan.Width = 150;
            // 
            // txtDebet
            // 
            this.txtDebet.HeaderText = "Debet";
            this.txtDebet.Name = "txtDebet";
            this.txtDebet.ReadOnly = true;
            this.txtDebet.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtDebet.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.txtDebet.Width = 80;
            // 
            // txtKredit
            // 
            this.txtKredit.HeaderText = "Kredit";
            this.txtKredit.Name = "txtKredit";
            this.txtKredit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtKredit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.txtKredit.Width = 80;
            // 
            // txtsaldodebet
            // 
            this.txtsaldodebet.HeaderText = "Saldo Debet";
            this.txtsaldodebet.Name = "txtsaldodebet";
            // 
            // txtsaldokredit
            // 
            this.txtsaldokredit.HeaderText = "Saldo Kredit";
            this.txtsaldokredit.Name = "txtsaldokredit";
            // 
            // txtNoBukti
            // 
            this.txtNoBukti.HeaderText = "No. Bukti";
            this.txtNoBukti.Name = "txtNoBukti";
            this.txtNoBukti.Width = 65;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(253, 416);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(242, 21);
            this.label2.TabIndex = 147;
            this.label2.Text = "Saldo Akhir (Sebelum Closing) :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(258, 444);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 21);
            this.label1.TabIndex = 148;
            this.label1.Text = "Saldo Akhir (Setelah Closing) :";
            // 
            // lblSebelum
            // 
            this.lblSebelum.AutoSize = true;
            this.lblSebelum.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSebelum.Location = new System.Drawing.Point(501, 416);
            this.lblSebelum.Name = "lblSebelum";
            this.lblSebelum.Size = new System.Drawing.Size(19, 21);
            this.lblSebelum.TabIndex = 149;
            this.lblSebelum.Text = "-";
            // 
            // lblSetelah
            // 
            this.lblSetelah.AutoSize = true;
            this.lblSetelah.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSetelah.Location = new System.Drawing.Point(501, 444);
            this.lblSetelah.Name = "lblSetelah";
            this.lblSetelah.Size = new System.Drawing.Size(19, 21);
            this.lblSetelah.TabIndex = 150;
            this.lblSetelah.Text = "-";
            // 
            // btn_tutup
            // 
            this.btn_tutup.Location = new System.Drawing.Point(655, 13);
            this.btn_tutup.Name = "btn_tutup";
            this.btn_tutup.Size = new System.Drawing.Size(75, 23);
            this.btn_tutup.TabIndex = 151;
            this.btn_tutup.Text = "Tutup Buku";
            this.btn_tutup.UseVisualStyleBackColor = true;
            this.btn_tutup.Click += new System.EventHandler(this.btn_tutup_Click);
            // 
            // fmbukubesar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 467);
            this.Controls.Add(this.btn_tutup);
            this.Controls.Add(this.lblSetelah);
            this.Controls.Add(this.lblSebelum);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.lbl_jenisakun);
            this.Controls.Add(this.cbo_jenisakun);
            this.Name = "fmbukubesar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fmbukubesar";
            this.Load += new System.EventHandler(this.fmbukubesar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_jenisakun;
        private System.Windows.Forms.ComboBox cbo_jenisakun;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtTanggal;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKeterangan;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtDebet;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtsaldodebet;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtsaldokredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNoBukti;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSebelum;
        private System.Windows.Forms.Label lblSetelah;
        private System.Windows.Forms.Button btn_tutup;
    }
}