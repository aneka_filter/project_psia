﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmPembeliTambah : Form
    {
        MySqlConnection con = new MySqlConnection();
        public fmPembeliTambah()
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =aneka_filter; uid=root;";
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            TambahPembeli();
        }
        #region Tambah Pembeli
        public void TambahPembeli()
        {
            if (txtNama.Text != "" && txtAlamat.Text != "" && txtTelp.Text != "")
            {
                con.Open();

                MySqlCommand objData = new MySqlCommand();
                objData.Connection = con;
                objData.CommandType = CommandType.Text;
                objData.CommandText = "Select nama From pembeli";
                MySqlDataReader objReader;
                objReader = objData.ExecuteReader();

                string tampungTxtNama = txtNama.Text;
                List<string> namaTampung = new List<string>();
                int cek = 0;
                while (objReader.Read() == true)
                {
                    namaTampung.Add(objReader.GetValue(0).ToString());
                }

                for (int j = 0; j < namaTampung.Count; j++)
                {
                    if (namaTampung[j] == tampungTxtNama)
                    {
                        MessageBox.Show("Nama Sama!!!");
                        cek = 1;
                        j = namaTampung.Count;
                    }
                }
                objReader.Close();

                if (cek == 0)
                {
                    MySqlCommand objKomen = new MySqlCommand();
                    objKomen.Connection = con;
                    objKomen.CommandType = CommandType.Text;
                    objKomen.CommandText = "Insert Into pembeli Values ('' ,@nama, @alamat, @telp, @status)";
                    objKomen.Parameters.AddWithValue("@nama", txtNama.Text);
                    objKomen.Parameters.AddWithValue("@alamat", txtAlamat.Text);
                    objKomen.Parameters.AddWithValue("@telp", txtTelp.Text);
                    objKomen.Parameters.AddWithValue("@status", 0);

                    int hasil = objKomen.ExecuteNonQuery();

                    if (hasil > 0)
                    {
                        MessageBox.Show("Penambahan data yang Anda tambahkan berhasil!");
                        txtNama.Clear();
                        txtAlamat.Clear();
                        txtTelp.Clear();
                    }
                    else
                    {
                        MessageBox.Show("Mohon maaf, Anda gagal menambahkan data");
                    }
                }

                con.Close();
            }
            else
            {
                MessageBox.Show("Periksa Data!!!");
            }

        }

        #endregion

        private void fmPembeliTambah_Load(object sender, EventArgs e)
        {

        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            else
                return base.ProcessDialogKey(keyData);
        }
    }
}
