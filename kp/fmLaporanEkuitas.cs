﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace kp
{
    public partial class fmLaporanEkuitas : Form
    {
        PrintDocument pdoc = new PrintDocument();

        MySqlConnection con = new MySqlConnection();
        int saldoAwal = 0;
        int labarugi = 0;
        int ekuitasLabaRugi = 0;
        int prive = 0;
        int ekuitasPrive = 0;
        int ekuitasBaru = 0;
        public fmLaporanEkuitas(int lr)
        {
            InitializeComponent();
            con.ConnectionString = "server= localhost; database =psia_transaksi; uid=root;";
            pdoc.PrintPage += printDocument1_PrintPage;
            labarugi = lr;
        }

        private void fmLaporanEkuitas_Load(object sender, EventArgs e)
        {
            AmbilNilaiEkuitasAwal();
            if(labarugi>=0)
            {
                lblLabaRugiBerjalan.Text = labarugi.ToString();
            }
            else
            {
                lblLabaRugiBerjalan.Text = "-("+(labarugi*-1).ToString()+")";
            }
            ekuitasLabaRugi = saldoAwal + labarugi;
            if (ekuitasLabaRugi >= 0)
            {
                lblEkuitasSetelahLabaRugi.Text = ekuitasLabaRugi.ToString();
            }
            else
            {
                lblEkuitasSetelahLabaRugi.Text = "-(" + (ekuitasLabaRugi*-1).ToString() + ")";
            }
            lblPrive.Text = prive.ToString();
            ekuitasPrive = ekuitasLabaRugi + prive;
            if (ekuitasPrive>= 0)
            {
                lblEkuitasSetelahPrive.Text = ekuitasPrive.ToString();
            }
            else
            {
                lblEkuitasSetelahPrive.Text = "-(" + (ekuitasPrive* -1).ToString()+ ")";
            }
            
            lblEkuitasPemilikBaru.Text = lblEkuitasSetelahPrive.Text;
        }

        public void AmbilNilaiEkuitasAwal()
        {
            con.Open();

            MySqlCommand objData = new MySqlCommand();
            objData.Connection = con;
            objData.CommandType = CommandType.Text;
            objData.CommandText = "SELECT saldo_awal FROM akun Where no_akun=30010";
            MySqlDataReader objReader;
            objReader = objData.ExecuteReader();

            while (objReader.Read() == true)
            {
                saldoAwal = int.Parse(objReader.GetValue(0).ToString());
            }
            if(saldoAwal>=0)
            {
                lblEkuitasPemilik.Text = saldoAwal.ToString();
            }
            else
            {
                lblEkuitasPemilik.Text = "-("+(saldoAwal*-1).ToString()+")";
            }
            
            objReader.Close();
            con.Close();
        }

        public void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Font objFont = new Font("Microsoft Sans Serif", 10F);//sets the font type and size
            float fTopMargin = e.MarginBounds.Top;
            float fLeftMargin = 50;//sets left margin
            float fLeftMargins = 100;//sets left margin
            float fRightMargin = e.MarginBounds.Right - 150;//sets right margin   
            string judul = String.Concat("Laporan Perubahan Ekuitas");
            string modal = String.Concat("Ekuitas Pemilik :  ", lblEkuitasPemilik.Text);
            string labarugi = String.Concat("Laba Rugi Tahun Berjalan :  ", lblLabaRugiBerjalan.Text);
            string setelahLabarugi = String.Concat("Ekuitas Pemilik Setelah Laba / Rugi :  ", lblEkuitasSetelahLabaRugi.Text);
            string prive = string.Concat("Prive :  ", lblPrive.Text);
            string setelahPrive = string.Concat("Ekuitas Setelah Prive :  ", lblEkuitasSetelahPrive.Text);
            string baru = string.Concat("Ekuitas Pemilik Baru :  ", lblEkuitasPemilikBaru.Text);

            e.Graphics.DrawString(judul, objFont, Brushes.Black, fLeftMargins, fTopMargin);

            fTopMargin += objFont.GetHeight() * 2;//skip two lines

            e.Graphics.DrawString(modal, objFont, Brushes.Black, fLeftMargin, fTopMargin);

            fTopMargin += objFont.GetHeight() * 2;//skip two lines

            e.Graphics.DrawString(labarugi, objFont, Brushes.Black, fLeftMargin, fTopMargin);

            fTopMargin += objFont.GetHeight() * 2;//skip two lines

            e.Graphics.DrawString(setelahLabarugi, objFont, Brushes.Black, fLeftMargin, fTopMargin);

            fTopMargin += objFont.GetHeight() * 2;//skip two lines

            e.Graphics.DrawString(prive, objFont, Brushes.Black, fLeftMargin, fTopMargin);

            fTopMargin += objFont.GetHeight() * 2;//skip two lines

            e.Graphics.DrawString(setelahPrive, objFont, Brushes.Black, fLeftMargin, fTopMargin);

            fTopMargin += objFont.GetHeight() * 2;//skip two lines

            e.Graphics.DrawString(baru, objFont, Brushes.Black, fLeftMargin, fTopMargin);

            objFont.Dispose();

            e.HasMorePages = false;
        }

        private void btnCetak_Click(object sender, EventArgs e)
        {
            using (PrintDialog pd = new PrintDialog())
            {
                if (pd.ShowDialog() == DialogResult.OK)
                {
                    pdoc.PrinterSettings = pd.PrinterSettings;
                    pdoc.Print();

                }
            }
        }
    }
}
