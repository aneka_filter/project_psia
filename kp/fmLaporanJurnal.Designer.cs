﻿namespace kp
{
    partial class fmLaporanJurnal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.txtTanggal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtKeterangan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNamaAkun = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDebet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtKredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtNoBukti = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToResizeColumns = false;
            this.dgvData.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtTanggal,
            this.txtKeterangan,
            this.txtNamaAkun,
            this.txtDebet,
            this.txtKredit,
            this.txtNoBukti});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Comic Sans MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvData.Location = new System.Drawing.Point(4, 101);
            this.dgvData.Name = "dgvData";
            this.dgvData.Size = new System.Drawing.Size(617, 367);
            this.dgvData.TabIndex = 66;
            // 
            // txtTanggal
            // 
            this.txtTanggal.HeaderText = "Tanggal";
            this.txtTanggal.Name = "txtTanggal";
            this.txtTanggal.Width = 80;
            // 
            // txtKeterangan
            // 
            this.txtKeterangan.HeaderText = "Keterangan";
            this.txtKeterangan.Name = "txtKeterangan";
            this.txtKeterangan.Width = 150;
            // 
            // txtNamaAkun
            // 
            this.txtNamaAkun.HeaderText = "N. Akun";
            this.txtNamaAkun.Name = "txtNamaAkun";
            this.txtNamaAkun.Width = 120;
            // 
            // txtDebet
            // 
            this.txtDebet.HeaderText = "Debet";
            this.txtDebet.Name = "txtDebet";
            this.txtDebet.ReadOnly = true;
            this.txtDebet.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtDebet.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.txtDebet.Width = 80;
            // 
            // txtKredit
            // 
            this.txtKredit.HeaderText = "Kredit";
            this.txtKredit.Name = "txtKredit";
            this.txtKredit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.txtKredit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.txtKredit.Width = 80;
            // 
            // txtNoBukti
            // 
            this.txtNoBukti.HeaderText = "No. Bukti";
            this.txtNoBukti.Name = "txtNoBukti";
            this.txtNoBukti.Width = 65;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(232, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 30);
            this.label1.TabIndex = 67;
            this.label1.Text = "LAPORAN JURNAL";
            // 
            // fmLaporanJurnal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 485);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.label1);
            this.Name = "fmLaporanJurnal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "fmLaporanJurnal";
            this.Load += new System.EventHandler(this.fmLaporanJurnal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtTanggal;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKeterangan;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNamaAkun;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtDebet;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtKredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtNoBukti;
    }
}